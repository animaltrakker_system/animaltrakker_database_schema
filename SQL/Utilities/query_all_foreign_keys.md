### Query All Foreign Keys

```sql
SELECT schema.name, fk_list.*
FROM sqlite_master AS schema
JOIN pragma_foreign_key_list(schema.name) AS fk_list 
ON schema.name != fk_list."table"
WHERE schema.type = 'table'
ORDER BY schema.name