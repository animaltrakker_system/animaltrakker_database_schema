---
publish: false
created: 2024-12-29
modified: 2024-12-29
aliases: 
tags:
- 
---

#  [[animaltrakker_database_schema]]
```SQL
-- Version 6.0.0

CREATE TABLE "alert_type_table"
	("id_alerttypeid" TEXT PRIMARY KEY
	, "alert_type_name" TEXT NOT NULL 
	, "alert_type_definition" TEXT NOT NULL 
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);

CREATE TABLE "animal_alert_table"
	("id_animalalertid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "alert" TEXT NOT NULL 
	, "alert_date" TEXT NOT NULL CHECK (alert_date IS DATE (alert_date))
	, "alert_time" TEXT CHECK (alert_time IS NULL OR alert_time IS TIME (alert_time))
	, "id_alerttypeid" TEXT NOT NULL 
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_alerttypeid) REFERENCES alert_type_table(id_alerttypeid)
	);
	
CREATE TABLE "animal_at_stud_table"
	("id_animalatstudid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "live_cover" INTEGER  NOT NULL CHECK (live_cover = 0 OR live_cover = 1) -- Boolean 1 = TRUE indicates available live cover
	, "live_cover_price" REAL CHECK (live_cover_price IS NULL OR live_cover_price >= 0.0)
	, "live_cover_price_units" TEXT 
	, "fresh_cooled_semen" INTEGER  NOT NULL CHECK (fresh_cooled_semen = 0 OR fresh_cooled_semen = 1) -- Boolean 1 = TRUE indicates available live cover
	, "fresh_cooled_semen_price" REAL CHECK (fresh_cooled_semen_price IS NULL OR fresh_cooled_semen_price >= 0.0)
	, "fresh_cooled_semen_price_units" TEXT 
	, "frozen_semen" INTEGER NOT NULL CHECK (frozen_semen = 0 OR frozen_semen = 1) -- Boolean 1 = TRUE indicates frozen semen available
	, "frozen_semen_price" REAL CHECK (frozen_semen_price IS NULL OR frozen_semen_price >= 0.0)
	, "frozen_semen_price_units" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN Key (live_cover_price_units) REFERENCES units_table(id_unitsid)
	, FOREIGN Key (frozen_semen_price_units) REFERENCES units_table(id_unitsid)
	, CHECK ((live_cover = 0 AND live_cover_price IS NULL AND live_cover_price_units IS NULL ) OR (live_cover_price IS NOT NULL AND live_cover_price_units IS NOT NULL))
	, CHECK ((fresh_cooled_semen = 0 AND fresh_cooled_semen_price IS NULL AND fresh_cooled_semen_price_units IS NULL) OR (fresh_cooled_semen_price IS NOT NULL AND fresh_cooled_semen_price_units IS NOT NULL))
	, CHECK ((frozen_semen = 0 AND frozen_semen_price IS NULL AND frozen_semen_price_units IS NULL) OR (frozen_semen_price IS NOT NULL AND frozen_semen_price_units IS NOT NULL))
	);
	
CREATE TABLE "animal_breed_table" (
	"id_animalbreedid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "id_breedid" TEXT NOT NULL 
	, "breed_percentage" REAL NOT NULL CHECK (breed_percentage > 0.0 AND breed_percentage <= 100.0)
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_breedid) REFERENCES breed_table(id_breedid)
	);
	
CREATE TABLE "animal_cluster_table" (
	"id_animalclusterid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "cluster_date" TEXT NOT NULL CHECK(cluster_date IS DATE (cluster_date))
	, "id_clusternameid" TEXT  NOT NULL 
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_clusternameid) REFERENCES cluster_name_table(id_clusternameid)
	);

CREATE TABLE "animal_date_last_seen_table" 
	("id_animalid" TEXT PRIMARY KEY
	, "last_seen_date" TEXT NOT NULL CHECK (last_seen_date IS DATE (last_seen_date))
	, "last_seen_time" TEXT NOT NULL CHECK (last_seen_time IS TIME (last_seen_time))
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	);
	
CREATE TABLE "animal_drug_table"
	("id_animaldrugid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "id_druglotid" TEXT NOT NULL 
	, "drug_date_on" TEXT NOT NULL CHECK (drug_date_on IS DATE (drug_date_on))
	, "drug_time_on" TEXT CHECK (drug_time_on IS TIME (drug_time_on))
	, "drug_date_off" TEXT CHECK (drug_date_off IS DATE (drug_date_off))
	, "drug_time_off" TEXT CHECK (drug_time_off IS TIME (drug_time_off))
	, "id_druglocationid" TEXT NOT NULL 
	, "drug_dosage" TEXT NOT NULL 
	, "id_drugofflabelid" TEXT 
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_druglotid) REFERENCES drug_lot_table(id_druglotid)
	, FOREIGN KEY (id_druglocationid) REFERENCES drug_location_table(id_druglocationid)
	, FOREIGN KEY (id_drugofflabelid) REFERENCES drug_off_label_table(id_drugofflabelid) 
	, CHECK(drug_time_off IS NULL OR drug_date_off IS NOT NULL)
	);
	
CREATE TABLE "animal_ebv_table"
	("id_animalebvid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "animal_ebv_id_number" TEXT NOT NULL -- This is the animal individual ID used by a particular EBV calculation method. 
	, "id_ebvcalculationmethodid" TEXT NOT NULL -- This is the pointer into all the possible EBV tables
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_ebvcalculationmethodid) REFERENCES ebv_calculation_method_table(id_ebvcalculationmethodid)
	);
	
CREATE TABLE "animal_evaluation_table" 
-- 	trait_name01 through trait_name20 fields are links into the evaluation_trait_table
-- 	trait_score16 through trait_score20 are links into the custom_evaluation_traits_table
	("id_animalevaluationid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "trait_name01" TEXT
	, "trait_score01" INTEGER CHECK (trait_score01 IS NULL OR (trait_score01 > 0 AND trait_score01 <= 5))
	, "trait_name02" TEXT
	, "trait_score02" INTEGER CHECK (trait_score02 IS NULL OR (trait_score02 > 0 AND trait_score02 <= 5))
	, "trait_name03" TEXT
	, "trait_score03" INTEGER CHECK (trait_score03 IS NULL OR (trait_score03 > 0 AND trait_score03 <= 5))
	, "trait_name04" TEXT
	, "trait_score04" INTEGER CHECK (trait_score04 IS NULL OR (trait_score04 > 0 AND trait_score04 <= 5))
	, "trait_name05" TEXT
	, "trait_score05" INTEGER CHECK (trait_score05 IS NULL OR (trait_score05 > 0 AND trait_score05 <= 5))
	, "trait_name06" TEXT
	, "trait_score06" INTEGER CHECK (trait_score06 IS NULL OR (trait_score06 > 0 AND trait_score06 <= 5))
	, "trait_name07" TEXT
	, "trait_score07" INTEGER CHECK (trait_score07 IS NULL OR (trait_score07 > 0 AND trait_score07 <= 5))
	, "trait_name08" TEXT
	, "trait_score08" INTEGER CHECK (trait_score08 IS NULL OR (trait_score08 > 0 AND trait_score08 <= 5))
	, "trait_name09" TEXT
	, "trait_score09" INTEGER CHECK (trait_score09 IS NULL OR (trait_score09 > 0 AND trait_score09 <= 5))
	, "trait_name10" TEXT
	, "trait_score10" INTEGER CHECK (trait_score10 IS NULL OR (trait_score10 > 0 AND trait_score10 <= 5))
	, "trait_name11" TEXT
	, "trait_score11" REAL
	, "trait_name12" TEXT
	, "trait_score12" REAL
	, "trait_name13" TEXT 
	, "trait_score13" REAL
	, "trait_name14" TEXT
	, "trait_score14" REAL
	, "trait_name15" TEXT
	, "trait_score15" REAL
	, "trait_units11" TEXT
	, "trait_units12" TEXT
	, "trait_units13" TEXT
	, "trait_units14" TEXT
	, "trait_units15" TEXT
	, "trait_name16"TEXT
	, "trait_score16" TEXT
	, "trait_name17" TEXT
	, "trait_score17" TEXT
	, "trait_name18" TEXT
	, "trait_score18"TEXT
	, "trait_name19" TEXT
	, "trait_score19" TEXT
	, "trait_name20" TEXT
	, "trait_score20" TEXT
	, "animal_rank" INTEGER CHECK (animal_rank IS NULL OR (0 < animal_rank AND animal_rank <= number_animals_ranked))
	, "number_animals_ranked" INTEGER CHECK (number_animals_ranked IS NULL OR 0 < number_animals_ranked)
	, "eval_date" TEXT NOT NULL CHECK (eval_date IS DATE (eval_date))
	, "eval_time" TEXT NOT NULL CHECK (eval_time IS TIME (eval_time))
	, "age_in_days" INTEGER NOT NULL CHECK(0 < age_in_days)
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (trait_name01) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name02) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name03) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name04) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name05) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name06) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name07) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name08) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name09) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name10) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name11) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name12) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name13) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name14) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name15) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name16) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name17) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name18) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name19) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_name20) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, FOREIGN KEY (trait_units11) REFERENCES units_table (id_unitsid)
	, FOREIGN KEY (trait_units12) REFERENCES units_table (id_unitsid)
	, FOREIGN KEY (trait_units13) REFERENCES units_table (id_unitsid)
	, FOREIGN KEY (trait_units14) REFERENCES units_table (id_unitsid)
	, FOREIGN KEY (trait_units15) REFERENCES units_table (id_unitsid)
	, FOREIGN KEY (trait_score16) REFERENCES custom_evaluation_traits_table (id_customevaluationtraitsid)
	, FOREIGN KEY (trait_score17) REFERENCES custom_evaluation_traits_table (id_customevaluationtraitsid)
	, FOREIGN KEY (trait_score18) REFERENCES custom_evaluation_traits_table (id_customevaluationtraitsid)
	, FOREIGN KEY (trait_score19) REFERENCES custom_evaluation_traits_table (id_customevaluationtraitsid)
	, FOREIGN KEY (trait_score20) REFERENCES custom_evaluation_traits_table (id_customevaluationtraitsid)
	, CHECK((number_animals_ranked IS NULL AND animal_rank IS NULL) OR (number_animals_ranked IS NOT NULL AND animal_rank IS NOT NULL))
	, CHECK(trait_score01 IS NULL OR trait_name01 IS NOT NULL)
	, CHECK(trait_score02 IS NULL OR trait_name02 IS NOT NULL)
	, CHECK(trait_score03 IS NULL OR trait_name03 IS NOT NULL)
	, CHECK(trait_score04 IS NULL OR trait_name04 IS NOT NULL)
	, CHECK(trait_score05 IS NULL OR trait_name05 IS NOT NULL)
	, CHECK(trait_score06 IS NULL OR trait_name06 IS NOT NULL)
	, CHECK(trait_score07 IS NULL OR trait_name07 IS NOT NULL)
	, CHECK(trait_score08 IS NULL OR trait_name08 IS NOT NULL)
	, CHECK(trait_score09 IS NULL OR trait_name09 IS NOT NULL)
	, CHECK(trait_score10 IS NULL OR trait_name10 IS NOT NULL)
	, CHECK((trait_name11 IS NOT NULL AND trait_units11 IS NOT NULL) OR (trait_name11 IS NULL AND trait_units11 IS NULL AND trait_score11 IS NULL))
	, CHECK((trait_name12 IS NOT NULL AND trait_units12 IS NOT NULL) OR (trait_name12 IS NULL AND trait_units12 IS NULL AND trait_score12 IS NULL))
	, CHECK((trait_name13 IS NOT NULL AND trait_units13 IS NOT NULL) OR (trait_name13 IS NULL AND trait_units13 IS NULL AND trait_score13 IS NULL))
	, CHECK((trait_name14 IS NOT NULL AND trait_units14 IS NOT NULL) OR (trait_name14 IS NULL AND trait_units14 IS NULL AND trait_score14 IS NULL))
	, CHECK((trait_name15 IS NOT NULL AND trait_units15 IS NOT NULL) OR (trait_name15 IS NULL AND trait_units15 IS NULL AND trait_score15 IS NULL))
	, CHECK(trait_score16 IS NULL OR trait_name16 IS NOT NULL)
	, CHECK(trait_score17 IS NULL OR trait_name17 IS NOT NULL)
	, CHECK(trait_score18 IS NULL OR trait_name18 IS NOT NULL)
	, CHECK(trait_score19 IS NULL OR trait_name19 IS NOT NULL)
	, CHECK(trait_score20 IS NULL OR trait_name20 IS NOT NULL)
	);	
	
CREATE TABLE "animal_external_file_table" 
	( "id_animalexternalfileid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL
	, "animal_external_file_name" TEXT NOT NULL
	, "animalexternal_file_relative_path" TEXT NOT NULL -- UNIX style names
	, "animalexternal_file_date" TEXT NOT NULL CHECK (animalexternal_file_date IS DATE (animalexternal_file_date))
	, "animalexternal_file_time" TEXT NOT NULL CHECK (animalexternal_file_time IS TIME (animalexternal_file_time))
	, "id_externalfiletypeid" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_externalfiletypeid) REFERENCES external_file_type_table(id_externalfiletypeid)
	);
	
CREATE TABLE "animal_farm_location_history_table"
	("id_animalfarmlocationhistoryid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "id_farmlocationid" TEXT NOT NULL 
	, "farm_location_date_in" TEXT NOT NULL CHECK (farm_location_date_in IS DATE (farm_location_date_in))
	, "farm_location_date_out" TEXT CHECK (farm_location_date_out IS NULL OR farm_location_date_out IS DATE (farm_location_date_out))
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_farmlocationid) REFERENCES farm_location_table(id_farmlocationid)
	);
	
CREATE TABLE "animal_female_breeding_table" 
	("id_animalfemalebreedingid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL -- the mother
	, "birthing_date" TEXT CHECK (birthing_date IS NULL OR birthing_date IS DATE (birthing_date))-- is either date of parturition or date when she was declared barren for this year
	, "birthing_time" TEXT CHECK (birthing_time IS NULL OR birthing_time IS TIME (birthing_time))
	, "birthing_notes" TEXT -- related to her birth notes not the individual offspring notes
	, "number_animals_born" INTEGER NOT NULL CHECK (number_animals_born >= 0)-- total number born including stillborn animals
	, "number_animals_weaned" INTEGER NOT NULL CHECK (number_animals_weaned >= 0)-- total number of animals weaned
	, "gestation_length" INTEGER  CHECK (gestation_length IS NULL OR gestation_length > 0)-- Calculated from first mating date
	, "id_animalmalebreedingid" TEXT -- which mating corresponds to this pregnancy For group matings we cannot determine the sire except with DNA on offspring so no male breeding entry is possible. 
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (number_animals_born) REFERENCES birth_type_table(id_birthtypeid)
	, FOREIGN KEY (number_animals_weaned) REFERENCES birth_type_table(id_birthtypeid)
	, FOREIGN KEY (id_animalmalebreedingid) REFERENCES animal_male_breeding_table(id_animalmalebreedingid)
	);
	
CREATE TABLE "animal_flock_prefix_table"
	("id_animalflockprefixid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "id_flockprefixid" TEXT NOT NULL
	, "id_registry_id_companyid" TEXT NOT NULL --The particular Registry that this record is referring to. 
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_flockprefixid) REFERENCES flock_prefix_table(id_flockprefixid)
	, FOREIGN KEY (id_registry_id_companyid) REFERENCES company_table (id_companyid)
	);
	
CREATE TABLE "animal_for_sale_table"
	("id_animalforsaleid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "sale_price" REAL CHECK (sale_price IS NULL OR sale_price >= 0.0)
	, "sale_price_id_unitsid" TEXT 
	, "sale_notes" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (sale_price_id_unitsid) REFERENCES units_table(id_unitsid)
	, CHECK ((sale_price IS NULL AND sale_price_id_unitsid IS NULL) OR (sale_price IS NOT NULL AND sale_price_id_unitsid IS NOT NULL))
	);
	
CREATE TABLE "animal_genetic_characteristic_table"
-- id_geneticcharacteristicvalueid is a foreign key but what table it is references varies based on the data in id_geneticcharacteristictableid
	("id_animalgeneticcharacteristicid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL
	, "id_geneticcharacteristictableid" TEXT NOT NULL
	, "id_geneticcharacteristicvalueid" TEXT NOT NULL
	, "id_geneticcharacteristiccalculationmethodid" TEXT NOT NULL
	, "genetic_characteristic_date" TEXT NOT NULL CHECK (genetic_characteristic_date IS DATE (genetic_characteristic_date))
	, "genetic_characteristic_time" TEXT CHECK (genetic_characteristic_time IS NULL OR genetic_characteristic_time IS TIME (genetic_characteristic_time))
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_geneticcharacteristictableid) REFERENCES genetic_characteristic_table(id_geneticcharacteristicid)
	, FOREIGN KEY (id_geneticcharacteristiccalculationmethodid) REFERENCES genetic_characteristic_calculation_method_table(id_geneticcharacteristiccalculationmethodid)
	);
	
CREATE TABLE "animal_id_info_table" 
	("id_animalidinfoid" TEXT PRIMARY KEY 
	, "id_animalid" TEXT NOT NULL 
	, "id_idtypeid" TEXT NOT NULL
	, "id_male_id_idcolorid" TEXT NOT NULL -- Color of the male portion of a 2 part tag. Single part tags have the same color both both of these fields
	, "id_female_id_idcolorid" TEXT NOT NULL -- Color of the female portion of a 2 part tag. Single part tags have the same color both both of these fields
	, "id_idlocationid" TEXT NOT NULL
	, "id_date_on" TEXT NOT NULL CHECK (id_date_on IS DATE (id_date_on))
	, "id_time_on" TEXT NOT NULL CHECK (id_time_on IS TIME (id_time_on))
	, "id_date_off" TEXT CHECK (id_date_off IS DATE (id_date_off))
	, "id_time_off" TEXT CHECK (id_time_off IS TIME (id_time_off))
	, "id_number" TEXT NOT NULL
	, "id_scrapieflockid" TEXT
	, "official_id" INTEGER NOT NULL CHECK (official_id = 0 OR official_id = 1)  -- Boolean 1 = TRUE indicates ID is an official ID
	, "id_idremovereasonid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_idtypeid) REFERENCES id_type_table(id_idtypeid)
	, FOREIGN KEY (id_male_id_idcolorid) REFERENCES id_color_table(id_idcolorid)
	, FOREIGN KEY (id_female_id_idcolorid) REFERENCES id_color_table(id_idcolorid)
	, FOREIGN KEY (id_idlocationid) REFERENCES id_location_table(id_idlocationid)
	, FOREIGN KEY (id_scrapieflockid) REFERENCES scrapie_flock_number_table(id_scrapieflocknumberid)
	, FOREIGN KEY (id_idremovereasonid) REFERENCES id_remove_reason_table(id_idremovereasonid)
	, CHECK ((id_date_off IS NULL AND id_time_off IS NULL) OR (id_date_off IS NOT NULL AND id_time_off IS NOT NULL))
	);
	
CREATE TABLE "animal_inbreeding_table" 
	("id_animalinbreedingid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "inbreeding" REAL NOT NULL CHECK (inbreeding >=0 AND inbreeding <= 1.0)
	, "inbreeding_date" TEXT NOT NULL CHECK (inbreeding_date IS DATE (inbreeding_date))
	, "id_inbreedingcalculationtypeid" TEXT NOT NULL 
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_inbreedingcalculationtypeid) REFERENCES inbreeding_calculation_type_table(id_inbreedingcalculationtypeid)
	);
	
CREATE TABLE "animal_location_history_table" 
-- Animals go from location of null at birth to the location of the dam or surrogate dam when born. 
-- Then the chain continues as they are moved. 
-- When animals die they go from the last location to Null to indicate they are nowhere
	("id_animallocationhistoryid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "movement_date" TEXT NOT NULL CHECK (movement_date IS DATE (movement_date))
	, "from_id_premiseid" TEXT
	, "to_id_premiseid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (from_id_premiseid) REFERENCES premise_table(id_premiseid)
	, FOREIGN KEY (to_id_premiseid) REFERENCES premise_table(id_premiseid)
	, CHECK (from_id_premiseid IS NOT NULL OR to_id_premiseid IS NOT NULL)
	);
	
CREATE TABLE "animal_male_breeding_table" 
	("id_animalmalebreedingid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL
	, "date_male_in" TEXT NOT NULL CHECK (date_male_in IS DATE (date_male_in))
	, "time_male_in" TEXT NOT NULL CHECK (time_male_in IS TIME (time_male_in))
	, "date_male_out" TEXT CHECK (date_male_out IS DATE (date_male_out))
	, "time_male_out" TEXT CHECK (time_male_out IS TIME (time_male_out))
	, "id_servicetypeid" TEXT NOT NULL
	, "id_animalstoredsemenid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_servicetypeid) REFERENCES service_type_table(id_servicetypeid)
	, FOREIGN KEY (id_animalstoredsemenid) REFERENCES animal_stored_semen_table(id_animalstoredsemenid)
	);
	
CREATE TABLE "animal_note_table" 
	("id_animalnoteid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "note_text" TEXT 
	, "note_date" TEXT NOT NULL CHECK (note_date IS DATE (note_date))
	, "note_time" TEXT NOT NULL CHECK (note_time IS TIME (note_time))
	, "id_predefinednotesid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_predefinednotesid) REFERENCES predefined_notes_table(id_predefinednotesid)
	, CHECK (note_text IS NOT NULL OR id_predefinednotesid IS NOT NULL)
	);

CREATE TABLE "animal_on_lease_table"
-- Breeding leases also require an acompanying animal_location_history record
	("id_animalonleaseid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "from_id_contactid" TEXT
	, "to_id_contactid" TEXT 
	, "from_id_companyid" TEXT 
	, "to_id_companyid" TEXT
	, "start_lease_date" TEXT NOT NULL CHECK (start_lease_date IS DATE (start_lease_date))
	, "end_lease_date" TEXT CHECK (end_lease_date IS DATE (end_lease_date))
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (from_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (to_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (from_id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (to_id_companyid) REFERENCES company_table (id_companyid)
	, CHECK ((from_id_contactid IS NULL AND from_id_companyid IS NOT NULL) OR (from_id_contactid IS NOT NULL AND from_id_companyid IS NULL))
	, CHECK ((to_id_contactid IS NULL AND to_id_companyid IS NOT NULL) OR (to_id_contactid IS NOT NULL AND to_id_companyid IS NULL))
	);
	
CREATE TABLE "animal_ownership_history_table" 
-- Animals go from ownership of NULL at birth to the first owner and then continue with changes as they are transferred. 
-- The last owner is the one who owned the animal at death.
	("id_animalownershiphistoryid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "transfer_date" TEXT NOT NULL CHECK (transfer_date IS DATE (transfer_date))
	, "from_id_contactid" TEXT
	, "to_id_contactid" TEXT  
	, "from_id_companyid" TEXT 
	, "to_id_companyid" TEXT 
	, "id_transferreasonid" TEXT NOT NULL
	, "sale_price" REAL
	, "sale_price_id_unitsid" TEXT 
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table (id_animalid)
	, FOREIGN KEY (from_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (to_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (from_id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (to_id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (id_transferreasonid) REFERENCES transfer_reason_table (id_transferreasonid)
	, FOREIGN KEY (sale_price_id_unitsid) REFERENCES units_table (id_unitsid)
	, CHECK ((from_id_contactid IS NULL AND from_id_companyid IS NULL) OR (from_id_contactid IS NULL AND from_id_companyid IS NOT NULL) OR (from_id_contactid IS NOT NULL AND from_id_companyid IS NULL))
	, CHECK ((to_id_contactid IS NULL AND to_id_companyid IS NOT NULL) OR (to_id_contactid IS NOT NULL AND to_id_companyid IS NULL))
	, CHECK ((sale_price IS NULL AND sale_price_id_unitsid IS NULL) OR (sale_price IS NOT NULL AND sale_price_id_unitsid IS NOT NULL))
	);
	
CREATE TABLE "animal_registration_table" 
-- Note that unregistered animals will still have a record in this table so that the breeder can be identified. 
-- For unregistered animals the breeder is the owner of the dam at the time of birth. 
-- Registries may use different rules for who is the breeder of any given animal. 
	("id_animalregistrationid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "animal_name" TEXT NOT NULL -- Names can vary with the registry so need to be here. It might be a duplicate of the name in the animal table. 
	, "registration_number" TEXT 
	, "id_registry_id_companyid" TEXT -- NULL means the animal is unregistered 
	, "id_animalregistrationtypeid" TEXT 
	, "id_flockbookid" TEXT 
	, "registration_date" TEXT NOT NULL CHECK (registration_date IS DATE (registration_date)) -- birth date for unregistered animals
	, "registration_description" TEXT
	, "id_breeder_id_contactid" TEXT
	, "id_breeder_id_companyid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_registry_id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (id_animalregistrationtypeid) REFERENCES registration_type_table (id_registrationtypeid)
	, FOREIGN KEY (id_flockbookid) REFERENCES flock_book_table (id_flockbookid)
	, FOREIGN KEY (id_breeder_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (id_breeder_id_companyid) REFERENCES company_table (id_companyid)
	, CHECK ((id_breeder_id_contactid IS NULL AND id_breeder_id_companyid IS NOT NULL) OR (id_breeder_id_contactid IS NOT NULL AND id_breeder_id_companyid IS NULL))
	, CHECK((registration_number IS NULL AND id_registry_id_companyid IS NULL AND id_animalregistrationtypeid IS NULL AND id_flockbookid IS NULL) OR (registration_number IS NOT NULL AND id_registry_id_companyid IS NOT NULL AND id_animalregistrationtypeid IS NOT NULL AND id_flockbookid IS NOT NULL))
	, UNIQUE (id_registry_id_companyid, id_animalregistrationtypeid, registration_number, id_flockbookid)	
);

CREATE TABLE "animal_stored_embryo_table" 
	("id_animalstoredembryoid" TEXT PRIMARY KEY
	, "sire_id" TEXT NOT NULL
	, "dam_id" TEXT NOT NULL
	, "number_embryos_stored" INTEGER NOT NULL  CHECK (number_embryos_stored >= 0)
	, "number_embryos_used" INTEGER NOT NULL CHECK (number_embryos_used >= 0 AND number_embryos_used <= number_embryos_stored) DEFAULT 0
	, "number_embryos_per_straw" INTEGER NOT NULL CHECK (number_embryos_per_straw >= 1)
	, "goblet_identifier" TEXT NOT NULL
	, "id_locationcollectedid" TEXT NOT NULL
	, "id_locationstoredid" TEXT NOT NULL
	, "date_collected" TEXT NOT NULL CHECK (date_collected IS DATE (date_collected))
	, "time_collected" TEXT CHECK (time_collected IS TIME (time_collected))
	, "date_female_estrus" TEXT CHECK (date_female_estrus IS DATE (date_female_estrus))
	, "id_embryofreezingmethodid" TEXT NOT NULL
	, "id_embryogradeid" TEXT NOT NULL
	, "id_embryostageid" TEXT NOT NULL
	, "id_vetcollectedid" TEXT NOT NULL
	, "id_animalfemalebreedingid" TEXT NOT NULL -- Breeding that produced the embryos
	, "owner_at_collection_id_contactid" TEXT
	, "owner_at_collection_id_companyid" TEXT
	, "owner_id_contactid" TEXT
	, "owner_id_companyid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (sire_id) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (dam_id) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_locationcollectedid) REFERENCES premise_table (id_premiseid)
	, FOREIGN KEY (id_locationstoredid) REFERENCES premise_table (id_premiseid)
	, FOREIGN KEY (id_embryofreezingmethodid) REFERENCES embryo_freezing_method_table (id_embryofreezingmethodid) 
	, FOREIGN KEY (id_embryogradeid) REFERENCES embryo_grade_table (id_embryogradeid)
	, FOREIGN KEY (id_embryostageid) REFERENCES embryo_stage_table (id_embryostageid)
	, FOREIGN KEY (id_vetcollectedid) REFERENCES contact_veterinarian_table (id_contactveterinarianid)
	, FOREIGN KEY (id_animalfemalebreedingid) REFERENCES animal_female_breeding_table (id_animalfemalebreedingid)
	, FOREIGN KEY (owner_at_collection_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (owner_at_collection_id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (owner_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (owner_id_companyid) REFERENCES company_table (id_companyid)
	, CHECK ((owner_at_collection_id_contactid IS NULL AND owner_at_collection_id_companyid IS NOT NULL) OR (owner_at_collection_id_contactid IS NOT NULL AND owner_at_collection_id_companyid IS NULL))
	, CHECK ((owner_id_contactid IS NULL AND owner_id_companyid IS NOT NULL) OR (owner_id_contactid IS NOT NULL AND owner_id_companyid IS NULL))	
	);
	
CREATE TABLE "animal_stored_semen_table" 
	("id_animalstoredsemenid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "number_straws_stored" INTEGER NOT NULL CHECK (number_straws_stored >= 0)
	, "number_straws_used" INTEGER NOT NULL CHECK (number_straws_used >= 0 AND number_straws_used <= number_straws_stored) DEFAULT "0"
	, "straws_per_insemination" REAL NOT NULL CHECK (number_straws_stored >= 0.0)
	, "id_semenextenderid" TEXT NOT NULL
	, "post_thaw_motility" REAL CHECK (post_thaw_motility IS NULL OR post_thaw_motility >= 0.0)
	, "post_thaw_progressive_motility" REAL CHECK (post_thaw_progressive_motility IS NULL OR post_thaw_progressive_motility >= 0.0)
	, "motile_sperm_per_insemination" REAL CHECK (motile_sperm_per_insemination IS NULL OR motile_sperm_per_insemination >= 0.0)
	, "goblet_identifier" TEXT NOT NULL
	, "id_locationcollectedid" TEXT NOT NULL
	, "date_collected" TEXT NOT NULL CHECK (date_collected IS DATE (date_collected))
	, "time_collected" TEXT CHECK (time_collected IS TIME (time_collected))
	, "owner_at_collection_id_contactid" TEXT
	, "owner_at_collection_id_companyid" TEXT
	, "owner_id_contactid" TEXT
	, "owner_id_companyid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table (id_animalid)
	, FOREIGN KEY (id_semenextenderid) REFERENCES semen_extender_table (id_semenextenderid)
	, FOREIGN KEY (id_locationcollectedid) REFERENCES premise_table (id_premiseid)
	, FOREIGN KEY (owner_at_collection_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (owner_at_collection_id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (owner_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (owner_id_companyid) REFERENCES company_table (id_companyid)
	, CHECK ((owner_at_collection_id_contactid IS NULL AND owner_at_collection_id_companyid IS NOT NULL) OR (owner_at_collection_id_contactid IS NOT NULL AND owner_at_collection_id_companyid IS NULL))
	, CHECK ((owner_id_contactid IS NULL AND owner_id_companyid IS NOT NULL) OR (owner_id_contactid IS NOT NULL AND owner_id_companyid IS NULL))	
	);
	
CREATE TABLE "animal_table" 
	("id_animalid" TEXT PRIMARY KEY
	, "animal_name" TEXT NOT NULL -- This name is the call or farm name. 
	--Registered animals may have different registered names for each registry they are in.
	--This is the name field that can be queried to locate sheep records on the farm app. 
	--Other registered names may be searchable in the desktop but cannot be searched on the mobile app. 
	, "id_sexid" TEXT NOT NULL -- The current sex of the animal. This will change if the animal is spayed or castrated. 
	, "sex_change_date" TEXT CHECK (sex_change_date IS DATE (sex_change_date))
	, "birth_date" TEXT CHECK (birth_date IS DATE (birth_date)) -- NULL is an Unknown birthdate. If only a year is given we make it 1 January. If a year and month we make it the first of that month so that all dates are considered dates.
	, "birth_time" TEXT CHECK (birth_time IS TIME (birth_time))
	, "id_birthtypeid" TEXT NOT NULL DEFAULT "7585ea2e-dcdf-41cb-94c1-4d133d624c1e" -- Set to unknown as a default
	, "birth_weight" REAL CHECK (birth_weight IS NULL OR birth_weight > 0.0)
	, "birth_weight_id_unitsid" TEXT
	, "birth_order" INTEGER CHECK (birth_order IS NULL OR birth_order > 0)
	, "rear_type" TEXT
	, "weaned_date" TEXT CHECK (weaned_date IS DATE (weaned_date))
	, "death_date" TEXT CHECK (death_date IS DATE (death_date))
	, "id_deathreasonid" TEXT 
	, "sire_id" TEXT -- genetic sire
	, "dam_id" TEXT -- genetic dam
	, "foster_dam_id" TEXT -- after birth dam that raised the animal
	, "surrogate_dam_id" TEXT --embryo transfer recipient dam
	, "hand_reared" INTEGER  NOT NULL DEFAULT 0 CHECK (hand_reared = 0 OR hand_reared = 1) -- Boolean 1 = TRUE indicates the animal was hand reared
	, "id_managementgroupid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_sexid) REFERENCES sex_table(id_sexid)
	, FOREIGN KEY (id_birthtypeid) REFERENCES birth_type_table (id_birthtypeid)
	, FOREIGN KEY (birth_weight_id_unitsid) REFERENCES units_table (id_unitsid)
	, FOREIGN KEY (rear_type) REFERENCES birth_type_table (id_birthtypeid)
	, FOREIGN KEY (id_deathreasonid) REFERENCES death_reason_table (id_deathreasonid)
	, FOREIGN KEY (sire_id) REFERENCES animal_table (id_animalid)
	, FOREIGN KEY (dam_id) REFERENCES animal_table (id_animalid)
	, FOREIGN KEY (foster_dam_id) REFERENCES animal_table (id_animalid)
	, FOREIGN KEY (surrogate_dam_id) REFERENCES animal_table (id_animalid)
	, FOREIGN KEY (id_managementgroupid) REFERENCES management_group_table (id_managementgroupid)
	, CHECK ((birth_weight IS NULL AND birth_weight_id_unitsid IS NULL) OR (birth_weight IS NOT NULL AND birth_weight_id_unitsid IS NOT NULL))
	, CHECK ((death_date IS NULL AND id_deathreasonid IS NULL) OR (death_date IS NOT NULL AND id_deathreasonid IS NOT NULL))
	);
	
CREATE TABLE "animal_tissue_sample_taken_table"
	( "id_animaltissuesampletakenid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL 
	, "id_tissuesampletypeid" TEXT NOT NULL
	, "tissue_sample_date" TEXT NOT NULL CHECK (tissue_sample_date IS DATE (tissue_sample_date))
	, "tissue_sample_time" TEXT CHECK (tissue_sample_time IS TIME (tissue_sample_time))
	, "id_tissuesamplecontainertypeid" TEXT NOT NULL
	, "tissue_sample_container_id" TEXT
	, "tissue_sample_container_exp_date" TEXT CHECK (tissue_sample_container_exp_date IS DATE (tissue_sample_container_exp_date))
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table (id_animalid)
	, FOREIGN KEY (id_tissuesampletypeid) REFERENCES tissue_sample_type_table (id_tissuesampletypeid)
	, FOREIGN KEY (id_tissuesamplecontainertypeid) REFERENCES tissue_sample_container_type_table (id_tissuesamplecontainertypeid)
	);
	
CREATE TABLE "animal_tissue_test_request_table"
	( "id_animaltissuetestrequestid" TEXT PRIMARY KEY
	, "id_animaltissuesampletakenid" TEXT NOT NULL
	, "id_tissuetestid" TEXT NOT NULL
	, "id_companylaboratoryid" TEXT NOT NULL
	, "tissue_sample_lab_accession_id" TEXT
	, "tissue_test_results" TEXT
	, "tissue_test_results_date" TEXT CHECK (tissue_test_results_date IS DATE (tissue_test_results_date))
	, "tissue_test_results_time" TEXT CHECK (tissue_test_results_time IS TIME (tissue_test_results_time))
	, "id_tissue_test_results_id_animalexternalfileid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animaltissuesampletakenid) REFERENCES animal_tissue_sample_taken_table (id_animaltissuesampletakenid)
	, FOREIGN KEY (id_tissuetestid) REFERENCES tissue_test_table (id_tissuetestid)
	, FOREIGN KEY (id_companylaboratoryid) REFERENCES company_laboratory_table (id_companylaboratoryid)
	, FOREIGN KEY (id_tissue_test_results_id_animalexternalfileid) REFERENCES animal_external_file_table (id_animalexternalfileid)
	);
	
CREATE TABLE "animaltrakker_default_settings_table" 
--	Most of these are pointers into the referenced table
-- All the color fields are pointers into the id_color_table
--	All the location fields are pointers into the id_location_table
-- All the contact and company fields are pointers into the contact or company tables
	("id_animaltrakkerdefaultsettingsid" TEXT PRIMARY KEY
	, "default_settings_name" TEXT NOT NULL UNIQUE
	, "owner_id_contactid" TEXT
	, "owner_id_companyid" TEXT
	, "owner_id_premiseid" TEXT NOT NULL
	, "breeder_id_contactid" TEXT
	, "breeder_id_companyid" TEXT
	, "breeder_id_premiseid" TEXT NOT NULL
	, "vet_id_contactid"TEXT
	, "vet_id_premiseid" TEXT
	, "lab_id_companyid" TEXT
	, "lab_id_premiseid"TEXT
	, "id_registry_id_companyid" TEXT
	, "registry_id_premiseid" TEXT
	, "id_stateid" TEXT NOT NULL
	, "id_countyid" TEXT
	, "id_flockprefixid" TEXT
	, "id_breedid" TEXT NOT NULL 
	, "id_sexid" TEXT NOT NULL	
	, "id_idtypeid_primary" TEXT NOT NULL 
	, "id_idtypeid_secondary" TEXT
	, "id_idtypeid_tertiary" TEXT
	, "id_eid_tag_male_color_female_color_same" INTEGER NOT NULL CHECK (id_eid_tag_male_color_female_color_same = 0 OR id_eid_tag_male_color_female_color_same = 1) -- Boolean 1 = true
	, "eid_tag_color_male" TEXT
	, "eid_tag_color_female" TEXT
	, "eid_tag_location" TEXT
	, "id_farm_tag_male_color_female_color_same" INTEGER NOT NULL CHECK (id_farm_tag_male_color_female_color_same = 0 OR id_farm_tag_male_color_female_color_same = 1) -- Boolean 1 = true
	, "farm_tag_based_on_eid_tag" INTEGER NOT NULL CHECK (farm_tag_based_on_eid_tag = 0 OR farm_tag_based_on_eid_tag = 1) -- Boolean 1 = true
	, "farm_tag_number_digits_from_eid" INTEGER
	, "farm_tag_color_male" TEXT
	, "farm_tag_color_female" TEXT
	, "farm_tag_location" TEXT
	, "id_fed_tag_male_color_female_color_same" INTEGER NOT NULL CHECK (id_fed_tag_male_color_female_color_same = 0 OR id_fed_tag_male_color_female_color_same = 1) -- Boolean 1 = true
	, "fed_tag_color_male" TEXT
	, "fed_tag_color_female" TEXT
	, "fed_tag_location" TEXT
	, "id_nues_tag_male_color_female_color_same" INTEGER NOT NULL CHECK (id_nues_tag_male_color_female_color_same = 0 OR id_nues_tag_male_color_female_color_same = 1) -- Boolean 1 = true
	, "nues_tag_color_male" TEXT
	, "nues_tag_color_female" TEXT
	, "nues_tag_location" TEXT
	, "id_trich_tag_male_color_female_color_same" INTEGER NOT NULL CHECK (id_trich_tag_male_color_female_color_same = 0 OR id_trich_tag_male_color_female_color_same = 1) -- Boolean 1 = true
	, "trich_tag_color_male" TEXT
	, "trich_tag_color_female" TEXT
	, "trich_tag_location" TEXT
	, "trich_tag_auto_increment" INTEGER NOT NULL CHECK (trich_tag_auto_increment = 0 OR trich_tag_auto_increment = 1) -- Boolean 1 = true
	, "trich_tag_next_tag_number" INTEGER 
	, "id_bangs_tag_male_color_female_color_same" TEXT NOT NULL CHECK (id_bangs_tag_male_color_female_color_same = 0 OR id_bangs_tag_male_color_female_color_same = 1) -- Boolean 1 = true
	, "bangs_tag_color_male" TEXT
	, "bangs_tag_color_female" TEXT
	, "bangs_tag_location" TEXT
	, "id_sale_order_tag_male_color_female_color_same" INTEGER NOT NULL CHECK (id_sale_order_tag_male_color_female_color_same = 0 OR id_sale_order_tag_male_color_female_color_same = 1) -- Boolean 1 = true
	, "sale_order_tag_color_male" TEXT
	, "sale_order_tag_color_female" TEXT
	, "sale_order_tag_location" TEXT
	, "use_paint_marks" INTEGER NOT NULL CHECK (use_paint_marks = 0 OR use_paint_marks = 1) -- Boolean 1 = true
	, "paint_mark_color" TEXT
	, "paint_mark_location" TEXT
	, "tattoo_color" TEXT
	, "tattoo_location" TEXT
	, "freeze_brand_location" TEXT
	, "id_idremovereasonid" TEXT NOT NULL	
	, "id_tissuesampletypeid" TEXT
	, "id_tissuetestid" TEXT
	, "id_tissuesamplecontainertypeid" TEXT NOT NULL	
	, "birth_type" TEXT NOT NULL
	, "rear_type" INTEGER
	, "minimum_birth_weight" REAL NOT NULL
	, "maximum_birth_weight" REAL NOT NULL
	, "birth_weight_id_unitsid" TEXT NOT NULL
	, "weight_id_unitsid" TEXT NOT NULL
	, "sale_price_id_unitsid" TEXT
	, "death_reason_id_contactid"  TEXT -- Which contact to use if you are sharing a death reason table with someone else
	, "death_reason_id_companyid"  TEXT -- Which company to use if you are sharing a death reason table e.g. 700 to use the generic death reasons
	, "id_deathreasonid" TEXT NOT NULL
	, "id_transferreasonid" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (owner_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (owner_id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (owner_id_premiseid) REFERENCES premise_table (id_premiseid)
	, FOREIGN KEY (breeder_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (breeder_id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (breeder_id_premiseid) REFERENCES premise_table (id_premiseid)
	, FOREIGN KEY (vet_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (vet_id_premiseid) REFERENCES premise_table (id_premiseid)
	, FOREIGN KEY (lab_id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (lab_id_premiseid) REFERENCES premise_table (id_premiseid)
	, FOREIGN KEY (id_registry_id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (registry_id_premiseid) REFERENCES premise_table (id_premiseid)
	, FOREIGN KEY (id_stateid) REFERENCES state_table (id_stateid)
	, FOREIGN KEY (id_countyid) REFERENCES county_table (id_countyid)
	, FOREIGN KEY (id_flockprefixid) REFERENCES flock_prefix_table(id_flockprefixid)
	, FOREIGN KEY (id_breedid) REFERENCES breed_table (id_breedid)
	, FOREIGN KEY (id_sexid) REFERENCES sex_table (id_sexid)
	, FOREIGN KEY (id_idtypeid_primary) REFERENCES id_type_table (id_idtypeid)
	, FOREIGN KEY (id_idtypeid_secondary) REFERENCES id_type_table (id_idtypeid)
	, FOREIGN KEY (id_idtypeid_tertiary) REFERENCES id_type_table (id_idtypeid)
	, FOREIGN KEY (eid_tag_color_male) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (eid_tag_color_female) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (eid_tag_location) REFERENCES id_location_table (id_idlocationid)
	, FOREIGN KEY (farm_tag_color_male) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (farm_tag_color_female) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (farm_tag_location) REFERENCES id_location_table (id_idlocationid)
	, FOREIGN KEY (fed_tag_color_male) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (fed_tag_color_female) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (fed_tag_location) REFERENCES id_location_table (id_idlocationid)
	, FOREIGN KEY (nues_tag_color_male) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (nues_tag_color_female) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (nues_tag_location) REFERENCES id_location_table (id_idlocationid)
	, FOREIGN KEY (trich_tag_color_male) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (trich_tag_color_female) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (trich_tag_location) REFERENCES id_location_table (id_idlocationid)
	, FOREIGN KEY (bangs_tag_color_male) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (bangs_tag_color_female) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (bangs_tag_location) REFERENCES id_location_table (id_idlocationid)
	, FOREIGN KEY (sale_order_tag_color_male) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (sale_order_tag_color_female) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (sale_order_tag_location) REFERENCES id_location_table (id_idlocationid)
	, FOREIGN KEY (paint_mark_color) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (paint_mark_location) REFERENCES id_location_table (id_idlocationid)
	, FOREIGN KEY (tattoo_color) REFERENCES id_color_table (id_idcolorid)
	, FOREIGN KEY (tattoo_location) REFERENCES id_location_table (id_idlocationid)
	, FOREIGN KEY (freeze_brand_location) REFERENCES id_location_table (id_idlocationid)
	, FOREIGN KEY (id_idremovereasonid) REFERENCES id_remove_reason_table (id_idremovereasonid)
	, FOREIGN KEY (id_tissuesampletypeid) REFERENCES tissue_sample_type_table (id_tissuesampletypeid)
	, FOREIGN KEY (id_tissuetestid) REFERENCES tissue_test_table (id_tissuetestid)
	, FOREIGN KEY (id_tissuesamplecontainertypeid) REFERENCES tissue_sample_container_type_table (id_tissuesamplecontainertypeid)
	, FOREIGN KEY (birth_type) REFERENCES birth_type_table (id_birthtypeid)
	, FOREIGN KEY (rear_type) REFERENCES birth_type_table (id_birthtypeid)
	, FOREIGN KEY (birth_weight_id_unitsid) REFERENCES units_table (id_unitsid)
	, FOREIGN KEY (weight_id_unitsid) REFERENCES units_table (id_unitsid)
	, FOREIGN KEY (sale_price_id_unitsid) REFERENCES units_table (id_unitsid)
	, FOREIGN KEY (id_deathreasonid) REFERENCES death_reason_table (id_deathreasonid)
	, FOREIGN KEY (id_transferreasonid) REFERENCES transfer_reason_table (id_transferreasonid)
	, CHECK ((owner_id_contactid IS NULL AND owner_id_companyid IS NOT NULL) OR (owner_id_contactid IS NOT NULL AND owner_id_companyid IS NULL))
	, CHECK ((breeder_id_contactid IS NULL AND breeder_id_companyid IS NOT NULL) OR (breeder_id_contactid IS NOT NULL AND breeder_id_companyid IS NULL))
	, CHECK ((vet_id_contactid IS NULL AND vet_id_premiseid IS NULL) OR (vet_id_contactid IS NOT NULL AND vet_id_premiseid IS NOT NULL))
	, CHECK ((lab_id_companyid IS NULL AND lab_id_premiseid IS NULL) OR (lab_id_companyid IS NOT NULL AND lab_id_premiseid IS NOT NULL))
	, CHECK ((id_registry_id_companyid IS NULL AND registry_id_premiseid IS NULL) OR (id_registry_id_companyid IS NOT NULL AND registry_id_premiseid IS NOT NULL))
	, CHECK ((farm_tag_based_on_eid_tag = 0 AND farm_tag_number_digits_from_eid IS NULL) OR (farm_tag_based_on_eid_tag = 1 AND 0 < farm_tag_number_digits_from_eid))
	, CHECK (trich_tag_auto_increment = 0 OR (trich_tag_auto_increment = 1 AND trich_tag_next_tag_number IS NOT NULL))
	, CHECK (id_eid_tag_male_color_female_color_same = 0  OR eid_tag_color_male = eid_tag_color_female)
	, CHECK (id_farm_tag_male_color_female_color_same = 0  OR farm_tag_color_male = farm_tag_color_female)
	, CHECK (id_fed_tag_male_color_female_color_same = 0  OR fed_tag_color_male = fed_tag_color_female)
	, CHECK (id_nues_tag_male_color_female_color_same = 0  OR nues_tag_color_male = nues_tag_color_female)
	, CHECK (id_trich_tag_male_color_female_color_same = 0  OR trich_tag_color_male = trich_tag_color_female)
	, CHECK (id_bangs_tag_male_color_female_color_same = 0  OR bangs_tag_color_male = bangs_tag_color_female)
	, CHECK (id_sale_order_tag_male_color_female_color_same = 0  OR sale_order_tag_color_male = sale_order_tag_color_female)
	);
	
CREATE TABLE "animaltrakker_metadata_table"
	( "id_animaltrakkermetadataid" TEXT PRIMARY KEY
	, "database_version" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "birth_type_table" 
	( "id_birthtypeid" TEXT PRIMARY KEY
	, "birth_type" TEXT NOT NULL UNIQUE
	, "birth_type_abbrev" TEXT NOT NULL UNIQUE
	, "birth_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "breed_table" 
	("id_breedid" TEXT PRIMARY KEY
	, "breed_name" TEXT NOT NULL
	, "breed_abbrev" TEXT NOT NULL
	, "breed_display_order" INTEGER NOT NULL
	, "id_speciesid" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, UNIQUE(id_speciesid, breed_name)
	, UNIQUE(id_speciesid, breed_abbrev)
	);
	
CREATE TABLE "cluster_calculation_type_table" 
	( "id_clustercalculationtypeid" TEXT PRIMARY KEY
	, "cluster_calculation_type" TEXT NOT NULL UNIQUE
	, "cluster_calculation_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);

CREATE TABLE "cluster_evaluation_table" 
	( "id_clusterevaluationid" TEXT PRIMARY KEY
	, "description" TEXT NOT NULL
	, "description_abbrev" TEXT NOT NULL
	, "id_clustercalculationtypeid" TEXT NOT NULL
	, "number_cluster_options"  INTEGER NOT NULL
	, "cluster_calculation_id_contactid" TEXT
	, "cluster_calculation_id_companyid" TEXT
	, "date_cluster_calculation"  TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_clustercalculationtypeid) REFERENCES cluster_calculation_type_table (id_clustercalculationtypeid)
	, FOREIGN KEY (cluster_calculation_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (cluster_calculation_id_companyid) REFERENCES company_table (id_companyid)
	, CHECK ((cluster_calculation_id_contactid IS NULL AND cluster_calculation_id_companyid IS NOT NULL) OR (cluster_calculation_id_contactid IS NOT NULL AND cluster_calculation_id_companyid IS NULL))
	);

CREATE TABLE "cluster_name_table"
	("id_clusternameid" TEXT PRIMARY KEY
	, "cluster_name" TEXT NOT NULL
	, "id_clusterevaluationid" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_clusterevaluationid) REFERENCES cluster_evaluation_table(id_clusterevaluationid)
	);
	
CREATE TABLE "company_email_table" 
	("id_companyemailid" TEXT PRIMARY KEY
	, "id_companyid" TEXT NOT NULL
	, "id_emailtypeid" TEXT NOT NULL
	, "company_email" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (id_emailtypeid) REFERENCES email_type_table (id_emailtypeid)
	);
	
CREATE TABLE "company_laboratory_table" 
	("id_companylaboratoryid" TEXT PRIMARY KEY
	, "id_companyid" TEXT NOT NULL
	, "laboratory_display_order" INTEGER NOT NULL
	, "lab_license_number" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_companyid) REFERENCES company_table (id_companyid)
	);
	
CREATE TABLE "company_note_table" 
	("id_companynoteid" TEXT PRIMARY KEY
	, "id_companyid" TEXT NOT NULL
	, "company_note_date" TEXT NOT NULL CHECK (company_note_date IS DATE (company_note_date))
	, "company_note_time" TEXT NOT NULL CHECK (company_note_time IS TIME (company_note_time))
	, "company_note" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_companyid) REFERENCES company_table (id_companyid)
	);
	
CREATE TABLE "company_phone_table" 
	("id_companyphoneid" TEXT PRIMARY KEY
	, "id_companyid" TEXT NOT NULL
	, "id_phonetypeid"TEXT NOT NULL
	, "company_phone" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_companyid) REFERENCES company_table(id_companyid)
	, FOREIGN KEY (id_phonetypeid) REFERENCES phone_type_table(id_phonetypeid)
	);
	
CREATE TABLE "company_premise_table" 
	("id_companypremiseid" TEXT PRIMARY KEY
	, "id_companyid" TEXT NOT NULL
	, "id_premiseid" TEXT NOT NULL
	, "start_premise_use" TEXT NOT NULL CHECK (start_premise_use IS DATE (start_premise_use))
	, "end_premise_use" TEXT CHECK (end_premise_use IS DATE (end_premise_use))
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (id_premiseid) REFERENCES premise_table (id_premiseid)
	);
	
CREATE TABLE "company_table" 
	( "id_companyid" TEXT PRIMARY KEY
	, "company" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "company_website_table" 
	("id_companywebsiteid" TEXT PRIMARY KEY
	, "id_companyid" TEXT NOT NULL
	, "company_website" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_companyid) REFERENCES company_table( id_companyid)
	);
	
CREATE TABLE "contact_company_table" 
	("id_contactcompanyid" TEXT PRIMARY KEY
	, "id_contactid" TEXT NOT NULL
	, "id_companyid" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (id_companyid) REFERENCES company_table (id_companyid)
	);
	
CREATE TABLE "contact_email_table" 
	("id_contactemailid" TEXT PRIMARY KEY
	, "id_contactid" TEXT NOT NULL
	, "id_emailtypeid"TEXT NOT NULL
	, "contact_email" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (id_emailtypeid) REFERENCES email_type_table (id_emailtypeid)
	);
	
CREATE TABLE "contact_note_table" 
	("id_contactnoteid" TEXT PRIMARY KEY
	, "id_contactid" TEXT NOT NULL
	, "contact_note_date" TEXT NOT NULL CHECK (contact_note_date IS DATE (contact_note_date))
	, "contact_note_time" TEXT NOT NULL CHECK (contact_note_time IS TIME (contact_note_time))
	, "contact_note" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contactid) REFERENCES contact_table (id_contactid)
	);
	
CREATE TABLE "contact_phone_table" 
	("id_contactphoneid" TEXT PRIMARY KEY
	, "id_contactid" TEXT NOT NULL
	, "id_phonetypeid" TEXT NOT NULL
	, "contact_phone" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contactid) REFERENCES contact_table(id_contactid)
	, FOREIGN KEY (id_phonetypeid) REFERENCES phone_type_table(id_phonetypeid)
	);
	
CREATE TABLE "contact_premise_table" 
	("id_contactpremiseid" TEXT PRIMARY KEY
	, "id_contactid" TEXT NOT NULL
	, "id_premiseid" TEXT NOT NULL
	, "start_premise_use" TEXT NOT NULL CHECK (start_premise_use IS DATE (start_premise_use))
	, "end_premise_use" TEXT CHECK (end_premise_use IS DATE (end_premise_use))
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (id_premiseid) REFERENCES premise_table (id_premiseid)
	);
	
CREATE TABLE "contact_table" 
	("id_contactid" TEXT PRIMARY KEY
	, "contact_last_name" TEXT NOT NULL
	, "contact_first_name" TEXT NOT NULL
	, "contact_middle_name" TEXT
	, "id_contacttitleid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contacttitleid) REFERENCES contact_title_table (id_contacttitleid)
	);
	
CREATE TABLE "contact_title_table" 
	("id_contacttitleid" TEXT PRIMARY KEY
	, "contact_title" TEXT NOT NULL UNIQUE
	, "contact_title_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "contact_veterinarian_license_table" 
	("id_contactveterinarianlicenseid" TEXT PRIMARY KEY
	, "id_contactid" TEXT NOT NULL
	, "id_contactveterinarianlicensetypeid" TEXT NOT NULL
	, "vet_license_number" TEXT NOT NULL
	, "vet_license_state_submission_status" TEXT NOT NULL
	, "vet_license_state" TEXT NOT NULL
	, "vet_license_expiration_date" TEXT NOT NULL CHECK (vet_license_expiration_date IS DATE (vet_license_expiration_date))
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contactid) REFERENCES contact_table (id_contactid)	
	, FOREIGN KEY (id_contactveterinarianlicensetypeid) REFERENCES contact_veterinarian_license_type_table (id_contactveterinarianlicensetypeid)	
	, FOREIGN KEY (vet_license_state) REFERENCES state_table (id_stateid)
	);
	
CREATE TABLE "contact_veterinarian_license_type_table" 
	("id_contactveterinarianlicensetypeid" TEXT PRIMARY KEY
	, "vet_license_type" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "contact_veterinarian_table" 
	("id_contactveterinarianid" TEXT PRIMARY KEY
	, "id_contactid" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contactid) REFERENCES contact_table (id_contactid)
	);
	
CREATE TABLE "contact_website_table" 
	("id_contactwebsiteid" TEXT PRIMARY KEY
	, "id_contactid" TEXT NOT NULL
	, "contact_website" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contactid) REFERENCES contact_table( id_contactid)
	);
	
CREATE TABLE "country_table" 
	("id_countryid" TEXT PRIMARY KEY
	, "country_name" TEXT NOT NULL UNIQUE
	, "country_abbrev" TEXT NOT NULL UNIQUE
	, "country_eid_prefix" TEXT NOT NULL CHECK (LENGTH (country_eid_prefix) = 3) -- Future consider how to limit to digits only.
	, "country_name_display_order" INTEGER NOT NULL 
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "county_table" 
	("id_countyid" TEXT PRIMARY KEY
	, "county_name" TEXT NOT NULL
	, "county_abbrev" TEXT NOT NULL
	, "id_stateid"  TEXT NOT NULL
	, "county_name_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_stateid) REFERENCES state_table (id_stateid)
	, UNIQUE(id_stateid, county_name)
	, UNIQUE(id_stateid, county_abbrev)
	);
	
CREATE TABLE "custom_evaluation_traits_table" 
	("id_customevaluationtraitsid" TEXT PRIMARY KEY
	, "id_evaluationtraitid" TEXT NOT NULL
	, "custom_evaluation_item" TEXT NOT NULL
	, "custom_evaluation_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_evaluationtraitid) REFERENCES evaluation_trait_table (id_evaluationtraitid)
	, UNIQUE(id_evaluationtraitid, custom_evaluation_item)
	);
	
CREATE TABLE "death_reason_table" 
	("id_deathreasonid" TEXT PRIMARY KEY
	, "death_reason" TEXT NOT NULL
	, "id_contactid" TEXT 
	, "id_companyid" TEXT 
	, "death_reason_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (id_companyid) REFERENCES company_table (id_companyid)
	, CHECK ((id_contactid IS NULL AND id_companyid IS NOT NULL) OR (id_contactid IS NOT NULL AND id_companyid IS NULL))
	, UNIQUE(id_contactid, death_reason)
	, UNIQUE(id_companyid, death_reason)
	);
	
CREATE TABLE "drug_location_table" 
	("id_druglocationid" TEXT PRIMARY KEY
	, "drug_location_name" TEXT NOT NULL UNIQUE
	, "drug_location_abbrev" TEXT NOT NULL UNIQUE
	, "drug_location_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "drug_lot_table" 
	("id_druglotid" TEXT PRIMARY KEY
	, "id_drugid" TEXT NOT NULL
	-- Drug lots with no lot number but we know there should have been one will have Unknown. 
	-- Drugs with no lot number at all will have NULL in the lot number field.
	, "drug_lot" TEXT
	-- Drugs that never expire will have NULL in the drug_expire_date
	-- Drugs where we never got the info but we know they do expire will have Unknown.
	, "drug_expire_date" TEXT
	, "drug_purchase_date" TEXT CHECK(drug_purchase_date IS DATE(drug_purchase_date))
	, "drug_amount_purchased" TEXT
	, "drug_cost" REAL CHECK(0.0 <= drug_cost)
	, "id_drug_cost_id_unitsid" TEXT 
	, "drug_dispose_date" TEXT CHECK(drug_dispose_date IS DATE(drug_dispose_date))
	, "drug_gone" INTEGER NOT NULL DEFAULT 0 CHECK (drug_gone = 0 OR drug_gone = 1) --BOOLEAN TRUE = 1 for all gone
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_drug_cost_id_unitsid) REFERENCES units_table (id_unitsid)
	, CHECK ((drug_cost IS NULL AND id_drug_cost_id_unitsid IS NULL) OR (drug_cost IS NOT NULL AND id_drug_cost_id_unitsid IS NOT NULL))
	);

CREATE TABLE "drug_off_label_table"
	("id_drugofflabelid" TEXT PRIMARY KEY
	, "id_drugid" TEXT NOT NULL
	, "id_speciesid" TEXT NOT NULL
	, "off_label_id_contactveterinarianid" TEXT NOT NULL
	, "off_label_drug_dosage" TEXT NOT NULL
	, "start_off_label_use" TEXT NOT NULL CHECK(start_off_label_use IS DATE(start_off_label_use))
	, "end_off_label_use" TEXT CHECK(end_off_label_use IS DATE(end_off_label_use))
	, "off_label_note" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_drugid) REFERENCES drug_table (id_drugid)
	, FOREIGN KEY (id_speciesid) REFERENCES species_table (id_speciesid)
	, FOREIGN KEY (off_label_id_contactveterinarianid) REFERENCES contact_veterinarian_table (id_contactveterinarianid)		
	);
	
CREATE TABLE "drug_table" 
	("id_drugid" TEXT PRIMARY KEY
	, "id_drugtypeid" TEXT NOT NULL
	, "trade_drug_name" TEXT NOT NULL
	, "generic_drug_name" TEXT NOT NULL
	, "drug_removable" INTEGER  NOT NULL DEFAULT 0 CHECK (drug_removable = 0 OR drug_removable = 1) -- 1 for removable, 0 for not removable
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_drugtypeid) REFERENCES drug_type_table (id_drugtypeid)
	);
	
CREATE TABLE "drug_type_table" 
	("id_drugtypeid" TEXT PRIMARY KEY
	, "drug_type" TEXT NOT NULL UNIQUE
	, "drug_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "drug_withdrawal_table"
	("id_drugwithdrawalid" TEXT PRIMARY KEY
	, "id_drugid" TEXT NOT NULL
	, "id_speciesid" TEXT NOT NULL
	, "drug_meat_withdrawal" INTEGER 
	, "id_meat_withdrawal_id_unitsid" TEXT 
	, "user_meat_withdrawal" INTEGER
	, "drug_milk_withdrawal" INTEGER 
	, "id_milk_withdrawal_id_unitsid" TEXT 
	, "user_milk_withdrawal" INTEGER 
	, "official_drug_dosage" TEXT NOT NULL
	, "user_drug_dosage" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_drugid) REFERENCES drug_table (id_drugid)
	, FOREIGN KEY (id_speciesid) REFERENCES species_table (id_speciesid)
	, FOREIGN KEY (id_meat_withdrawal_id_unitsid) REFERENCES units_table (id_unitsid)
	, FOREIGN KEY (id_milk_withdrawal_id_unitsid) REFERENCES units_table (id_unitsid)
	, CHECK((drug_meat_withdrawal IS NULL AND user_meat_withdrawal IS NULL AND id_meat_withdrawal_id_unitsid IS NULL) OR (drug_meat_withdrawal IS NOT NULL AND user_meat_withdrawal IS NOT NULL AND id_meat_withdrawal_id_unitsid IS NOT NULL AND 0 < drug_meat_withdrawal AND drug_meat_withdrawal <= user_meat_withdrawal))
	, CHECK((drug_milk_withdrawal IS NULL AND user_milk_withdrawal IS NULL AND id_milk_withdrawal_id_unitsid IS NULL) OR (drug_milk_withdrawal IS NOT NULL AND user_milk_withdrawal IS NOT NULL AND id_milk_withdrawal_id_unitsid IS NOT NULL AND 0 < drug_milk_withdrawal AND drug_milk_withdrawal <= user_milk_withdrawal))
	, UNIQUE(id_drugid, id_speciesid)
);

CREATE TABLE "ebv_calculation_method_table"
	("id_ebvcalculationmethodid" TEXT PRIMARY KEY
	, "ebv_table_name" TEXT NOT NULL
	, "ebv_calculation_method_abbreviation" TEXT NOT NULL
	, "ebv_calculation_method_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);

CREATE TABLE "ebv_date_ranges_table" 
	("id_ebvdaterangesid" TEXT PRIMARY KEY
	, "id_ebvcalculationmethodid" TEXT NOT NULL
	, "age_name" TEXT NOT NULL 
	, "ebv_minimum_days" INTEGER NOT NULL CHECK(0 <= ebv_minimum_days)
	, "ebv_maximum_days" INTEGER NOT NULL CHECK(ebv_minimum_days <= ebv_maximum_days)
	, "ebv_optimal_days" INTEGER 
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_ebvcalculationmethodid) REFERENCES ebv_calculation_method_table (id_ebvcalculationmethodid)
	, UNIQUE (id_ebvcalculationmethodid, age_name)
	);
		
CREATE TABLE "email_type_table" 
	("id_emailtypeid" TEXT PRIMARY KEY
	, "email_type" TEXT NOT NULL UNIQUE
	, "email_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "embryo_freezing_method_table" 
	("id_embryofreezingmethodid" TEXT PRIMARY KEY
	, "embryo_freezing_method_name" TEXT NOT NULL UNIQUE
	, "embryo_freezing_method_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "embryo_grade_table" 
	("id_embryogradeid" TEXT PRIMARY KEY
	, "embryo_grade_name" TEXT NOT NULL UNIQUE 
	, "embryo_grade_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "embryo_stage_table" 
	("id_embryostageid" TEXT PRIMARY KEY
	, "embryo_stage_name" TEXT NOT NULL UNIQUE
	, "embryo_stage_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "evaluation_trait_table" 
	("id_evaluationtraitid" TEXT PRIMARY KEY
	, "trait_name" TEXT NOT NULL UNIQUE
	, "id_evaluationtraittypeid" TEXT NOT NULL
	, "evaluation_trait_display_order" INTEGER NOT NULL
	, "id_unitstypeid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_evaluationtraittypeid) REFERENCES evaluation_trait_type_table (id_evaluationtraittypeid)
	, FOREIGN KEY (id_unitstypeid) REFERENCES units_type_table (id_unitstypeid)
	--Consider adding CHECK to constrain id_unitstypeid to NOT NULL if of type units trait type.
	);
	
CREATE TABLE "evaluation_trait_type_table" 
	("id_evaluationtraittypeid" TEXT PRIMARY KEY
	, "trait_type" TEXT NOT NULL UNIQUE
	, "trait_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "external_file_root_folder_table"
	( "id_externalfilerootfolderid" TEXT PRIMARY KEY
	, "id_contactid" TEXT
	, "id_companyid" TEXT
	, "absolute_path_root" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (id_companyid) REFERENCES company_table (id_companyid)
	, CHECK((id_contactid IS NULL AND id_companyid IS NOT NULL) OR (id_contactid IS NOT NULL AND id_companyid IS NULL))
	);
	
CREATE TABLE "external_file_type_table" 
	("id_externalfiletypeid" TEXT PRIMARY KEY
	, "external_file_type" TEXT NOT NULL
	, "external_file_type_suffix" TEXT NOT NULL
	, "external_file_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "farm_location_table" 
	("id_farmlocationid" TEXT PRIMARY KEY
	, "farm_location_name" TEXT NOT NULL
	, "farm_location_abbreviation" TEXT NOT NULL
	, "id_premiseid" TEXT NOT NULL
	, "farm_location_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_premiseid) REFERENCES premise_table (id_premiseid)
	, UNIQUE (id_premiseid, farm_location_name)
	, UNIQUE (id_premiseid, farm_location_abbreviation)
	);
	
CREATE TABLE "flock_book_table" 
	("id_flockbookid" TEXT PRIMARY KEY
	, "registry_id_companyid" TEXT NOT NULL
	, "flock_book_volume_name" TEXT NOT NULL
	, "flock_book_publication_date" TEXT CHECK (flock_book_publication_date IS DATE (flock_book_publication_date))
	, "flock_book_volume_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (registry_id_companyid) REFERENCES company_table (id_companyid)
	, UNIQUE(registry_id_companyid, flock_book_volume_name)
	);
	
CREATE TABLE "flock_prefix_table" 
	("id_flockprefixid" TEXT PRIMARY KEY
	, "flock_prefix" TEXT NOT NULL 
	, "id_prefixowner_id_contactid" TEXT
	, "id_prefixowner_id_companyid" TEXT
	, "id_registry_id_companyid" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_prefixowner_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (id_prefixowner_id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (id_registry_id_companyid) REFERENCES company_table (id_companyid)	
	, CHECK ((id_prefixowner_id_contactid IS NULL AND id_prefixowner_id_companyid IS NOT NULL) OR (id_prefixowner_id_contactid IS NOT NULL AND id_prefixowner_id_companyid IS NULL))
	);
	
CREATE TABLE "genetic_characteristic_calculation_method_table"
	("id_geneticcharacteristiccalculationmethodid" TEXT PRIMARY KEY
	, "genetic_characteristic_calculation_method" TEXT NOT NULL UNIQUE
	, "genetic_characteristic_calculation_method_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "genetic_characteristic_table"
	("id_geneticcharacteristicid" TEXT PRIMARY KEY
	, "genetic_characteristic_table_name" TEXT NOT NULL UNIQUE
	, "genetic_characteristic_table_display_name" TEXT NOT NULL UNIQUE
	, "genetic_characteristic_table_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "genetic_coat_color_table" 
	("id_geneticcoatcolorid" TEXT PRIMARY KEY
	, "id_registry_id_companyid" TEXT NOT NULL 
	, "coat_color" TEXT NOT NULL
	, "coat_color_abbrev" TEXT NOT NULL
	, "coat_color_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_registry_id_companyid) REFERENCES company_table (id_companyid)
	, UNIQUE(id_registry_id_companyid, coat_color)
	, UNIQUE(id_registry_id_companyid, coat_color_abbrev)
	);
	
CREATE TABLE "genetic_codon112_table" 
	("id_geneticcodon112id" TEXT PRIMARY KEY
	, "codon112_alleles" TEXT NOT NULL UNIQUE
	, "codon112_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "genetic_codon136_table" 
	("id_geneticcodon136id" TEXT PRIMARY KEY
	, "codon136_alleles" TEXT NOT NULL UNIQUE
	, "codon136_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "genetic_codon141_table" 
	("id_geneticcodon141id" TEXT PRIMARY KEY
	, "codon141_alleles" TEXT NOT NULL UNIQUE
	, "codon141_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "genetic_codon154_table" 
	("id_geneticcodon154id" TEXT PRIMARY KEY
	, "codon154_alleles" TEXT NOT NULL UNIQUE
	, "codon154_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "genetic_codon171_table" 
	("id_geneticcodon171id" TEXT PRIMARY KEY
	, "codon171_alleles" TEXT NOT NULL UNIQUE
	, "codon171_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "genetic_horn_type_table" 
	("id_genetichorntypeid" TEXT PRIMARY KEY
	, "id_registry_id_companyid" TEXT NOT NULL 
	, "horn_type" TEXT NOT NULL
	, "horn_type_abbrev" TEXT NOT NULL
	, "horn_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_registry_id_companyid) REFERENCES company_table (id_companyid)
	, UNIQUE(id_registry_id_companyid, horn_type)
	, UNIQUE(id_registry_id_companyid, horn_type_abbrev)
	);
	
CREATE TABLE "id_color_table" 
	( "id_idcolorid" TEXT PRIMARY KEY
	, "id_color_name" TEXT NOT NULL UNIQUE
	, "id_color_abbrev" TEXT NOT NULL UNIQUE
	, "id_color_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "id_location_table" 
	("id_idlocationid" TEXT PRIMARY KEY
	, "id_location_name" TEXT NOT NULL UNIQUE
	, "id_location_abbrev" TEXT (2) NOT NULL UNIQUE
	, "id_location_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "id_remove_reason_table" 
	("id_idremovereasonid" TEXT PRIMARY KEY
	, "id_remove_reason" TEXT NOT NULL UNIQUE
	, "id_remove_reason_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "id_type_table" 
	("id_idtypeid" TEXT PRIMARY KEY
	, "id_type_name" TEXT NOT NULL UNIQUE
	, "id_type_abbrev" TEXT NOT NULL UNIQUE
	, "id_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "inbreeding_calculation_type_table"
	("id_inbreedingcalculationtypeid" TEXT PRIMARY KEY
	, "inbreeding_calculation_type" TEXT NOT NULL UNIQUE
	, "inbreeding_calculation_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "lab_test_external_submission_form_table" 
	("id_labtestexternalsubmissionformid" TEXT PRIMARY KEY
	, "id_externalfiletypeid" TEXT NOT NULL
	, "id_tissuetestid" TEXT NOT NULL
	, "id_companyid" TEXT NOT NULL -- The lab that uses this specific form
	, "lab_submission_form_filename" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_externalfiletypeid) REFERENCES external_file_type_table(id_externalfiletypeid)
	, FOREIGN KEY (id_tissuetestid) REFERENCES tissue_test_table(id_tissuetestid)
	, FOREIGN KEY (id_companyid) REFERENCES company_table(id_companyid)
	);
	
CREATE TABLE "management_group_table" 
	("id_managementgroupid" TEXT PRIMARY KEY 
	, "management_group" TEXT NOT NULL
	, "management_group_display_order" INTEGER NOT NULL
	, "id_contactid" TEXT
	, "id_companyid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (id_companyid) REFERENCES company_table (id_companyid)
	, CHECK ((id_contactid IS NULL AND id_companyid IS NOT NULL) OR (id_contactid IS NOT NULL AND id_companyid IS NULL))
	, UNIQUE(id_contactid, management_group)
	, UNIQUE(id_companyid, management_group)
	);
	
CREATE TABLE "owner_note_table" 
-- Similar to the contacts_note_table but this one is for the registrar
--	to enter notes on individual owners
	("id_ownernoteid" TEXT PRIMARY KEY 
	, "id_owner_id_contactid" TEXT
	, "id_owner_id_companyid" TEXT
	, "owner_note_date" TEXT NOT NULL CHECK (owner_note_date IS DATE (owner_note_date))
	, "owner_note_text" TEXT NOT NULL
	, "id_registry_id_companyid" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_owner_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (id_owner_id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (id_registry_id_companyid) REFERENCES company_table (id_companyid)
	, CHECK ((id_owner_id_contactid IS NULL AND id_owner_id_companyid IS NOT NULL) OR (id_owner_id_contactid IS NOT NULL AND id_owner_id_companyid IS NULL))
	);
	
CREATE TABLE "owner_registration_table" 
	("id_ownerregistrationid" INTEGER PRIMARY KEY
	, "membership_number" TEXT NOT NULL
	, "id_contactid" TEXT
	, "id_companyid" TEXT
	, "id_registry_id_companyid" TEXT NOT NULL 
	, "date_joined" TEXT CHECK (date_joined IS DATE (date_joined))
	, "dues_paid_until" TEXT CHECK (dues_paid_until IS DATE (dues_paid_until))
	, "date_resigned" TEXT CHECK (date_resigned IS TIME (date_resigned))
	, "online_password" TEXT --Note that access to on-line cannot make changes to the real database
	, "id_registrymembershipregionid" TEXT
	, "id_flockprefixid" TEXT
	, "id_registrymembershipstatusid" TEXT
	, "id_registrymembershiptypeid" TEXT
	, "board_member" INTEGER NOT NULL DEFAULT 0 CHECK (board_member = 0 OR board_member = 1) --BOOLEAN TRUE = 1 for members of the Board of Directors of this registry
	, "last_census" TEXT CHECK (last_census IS DATE (last_census))
	, "id_registryprivacyid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (id_registry_id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (id_registrymembershipregionid) REFERENCES registry_membership_region_table (id_registrymembershipregionid)
	, FOREIGN KEY (id_flockprefixid) REFERENCES flock_prefix_table (id_flockprefixid)
	, FOREIGN KEY (id_registrymembershipstatusid) REFERENCES registry_membership_status_table (id_registrymembershipstatusid)
	, FOREIGN KEY (id_registrymembershiptypeid) REFERENCES registry_membership_type_table (id_registrymembershiptypeid)
	, FOREIGN KEY (id_registryprivacyid) REFERENCES registry_privacy_table (id_registryprivacyid)
	, CHECK ((id_contactid IS NULL AND id_companyid IS NOT NULL) OR (id_contactid IS NOT NULL AND id_companyid IS NULL))
	);
	
CREATE TABLE "phone_type_table" 
	("id_phonetypeid" TEXT PRIMARY KEY
	, "phone_type" TEXT NOT NULL UNIQUE
	, "phone_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "predefined_notes_table" 
	("id_predefinednotesid" TEXT PRIMARY KEY 
	, "predefined_note_text" TEXT NOT NULL
	, "id_contactid" TEXT
	, "id_companyid" TEXT
	, "predefined_note_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (id_companyid) REFERENCES company_table (id_companyid)
	, CHECK ((id_contactid IS NULL AND id_companyid IS NOT NULL) OR (id_contactid IS NOT NULL AND id_companyid IS NULL))
	, UNIQUE(id_contactid, predefined_note_text)
	, UNIQUE(id_companyid, predefined_note_text)
	);
	
CREATE TABLE "premise_jurisdiction_table"
	("id_premisejurisdictionid" TEXT PRIMARY KEY
	, "premise_jurisdiction" TEXT NOT NULL UNIQUE
	, "premise_jurisdiction_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "premise_table" 
	("id_premiseid" TEXT PRIMARY KEY
	, "id_premisetypeid" TEXT NOT NULL 
	, "id_premisejurisdictionid" TEXT
	, "premise_number" TEXT
	, "premise_lat_long_id_unitsid" TEXT
	, "premise_latitude" REAL
	, "premise_longitude" REAL
	, "premise_address1" TEXT NOT NULL
	, "premise_address2" TEXT
	, "premise_city" TEXT NOT NULL
	, "premise_id_stateid" TEXT NOT NULL 
	, "premise_postcode" TEXT NOT NULL
	, "premise_id_countyid" TEXT
	, "premise_id_countryid" TEXT NOT NULL 
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_premisetypeid) REFERENCES premise_type_table (id_premisetypeid)
	, FOREIGN KEY (id_premisejurisdictionid) REFERENCES premise_jurisdiction_table(id_premisejurisdictionid)
	, FOREIGN KEY (premise_lat_long_id_unitsid) REFERENCES units_table (id_unitsid)
	, FOREIGN KEY (premise_id_stateid) REFERENCES state_table (id_stateid)
	, FOREIGN KEY (premise_id_countyid) REFERENCES county_table (id_countyid)
	, FOREIGN KEY (premise_id_countryid) REFERENCES country_table (id_countryid)
	, CHECK ((premise_latitude IS NULL AND premise_longitude IS NULL AND premise_lat_long_id_unitsid IS NULL) OR (premise_latitude IS NOT NULL AND premise_longitude IS NOT NULL AND premise_lat_long_id_unitsid IS NOT NULL))
	);
	
CREATE TABLE "premise_type_table"
	("id_premisetypeid" TEXT PRIMARY KEY
	, "premise_type" TEXT NOT NULL UNIQUE
	, "premise_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "registration_type_table" 
	("id_registrationtypeid" TEXT PRIMARY KEY
	, "id_registry_id_companyid" TEXT NOT NULL
	, "registration_type" TEXT NOT NULL
	, "registration_type_abbrev" TEXT NOT NULL
	, "registration_number_formula" TEXT
	, "registration_number_calculation"  TEXT
	, "last_registration_number"  TEXT
	, "registration_certificate_file_name" TEXT
	, "registration_certificate_file_path_from_root" TEXT
	, "registration_certificate_file_absolute_path" TEXT
	, "registration_certificate_file_hash" TEXT
	, "registration_certificate_file_date" TEXT
	, "registration_certificate_id_externalfiletypeid" TEXT
	, "registration_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY ("id_registry_id_companyid") REFERENCES company_table (id_companyid)
	, FOREIGN KEY ("registration_certificate_id_externalfiletypeid") REFERENCES external_file_type_table(id_externalfiletypeid)
	, UNIQUE (id_registry_id_companyid, registration_type)
	, UNIQUE (id_registry_id_companyid, registration_type_abbrev)
	);
	
CREATE TABLE "registry_certificate_print_table" 
	("id_registrycertificateprintid" TEXT PRIMARY KEY
	, "id_companyid" TEXT NOT NULL
	, "id_animalid" TEXT NOT NULL
	, "id_registrationtypeid" TEXT NOT NULL
	, "printed" INTEGER NOT NULL DEFAULT 0 CHECK (printed = 0 OR printed = 1) --Boolean 1 means registration certificate has been printed 0 means it's ready to print
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (id_animalid) REFERENCES animal_table (id_animalid)
	, FOREIGN KEY (id_registrationtypeid) REFERENCES registration_type_table (id_registrationtypeid)
	);

CREATE TABLE "registry_default_settings_table" 
	("id_registrydefaultsettingsid" TEXT PRIMARY KEY
	, "id_companyid" TEXT NOT NULL
	, "current_id_flockbookid" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (current_id_flockbookid) REFERENCES flock_book_table (id_flockbookid)
	);
	
CREATE TABLE "registry_info_table" 
	("id_registryinfoid" TEXT PRIMARY KEY
	, "id_companyid" TEXT NOT NULL
	, "registry_founded_date" TEXT NOT NULL CHECK (registry_founded_date IS DATE (registry_founded_date))
	, "registry_closed_date" TEXT CHECK (registry_closed_date IS DATE (registry_closed_date))
	, "registry_abbrev" TEXT NOT NULL
	, "id_breedid" TEXT
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_companyid) REFERENCES company_table (id_companyid)
	, FOREIGN KEY (id_breedid) REFERENCES breed_table (id_breedid)
	, UNIQUE(id_companyid, registry_abbrev)
	);
	
CREATE TABLE "registry_membership_region_table" 
	("id_registrymembershipregionid" INTEGER PRIMARY KEY
	, "registry_id_companyid" TEXT NOT NULL
	, "membership_region" TEXT NOT NULL
	, "membership_region_number" INTEGER
	, "membership_region_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (registry_id_companyid) REFERENCES company_table (id_companyid)
	, UNIQUE(registry_id_companyid, membership_region)
	, UNIQUE(registry_id_companyid, membership_region_number)
	);
	
CREATE TABLE "registry_membership_status_table" 
	("id_registrymembershipstatusid" TEXT PRIMARY KEY
	, "registry_id_companyid" TEXT NOT NULL
	, "membership_status" TEXT NOT NULL
	, "membership_status_abbrev" TEXT NOT NULL
	, "membership_status_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (registry_id_companyid) REFERENCES company_table (id_companyid)
	, UNIQUE(registry_id_companyid, membership_status)
	, UNIQUE(registry_id_companyid, membership_status_abbrev)
	);
	
CREATE TABLE "registry_membership_type_table"
	("id_registrymembershiptypeid" TEXT PRIMARY KEY
	, "registry_id_companyid" TEXT NOT NULL
	, "membership_type" TEXT NOT NULL
	, "membership_type_abbrev" TEXT NOT NULL
	, "membership_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (registry_id_companyid) REFERENCES company_table (id_companyid)
	, UNIQUE(registry_id_companyid, membership_type)
	, UNIQUE(registry_id_companyid, membership_type_abbrev)
	);
	
CREATE TABLE "registry_privacy_table" 
	("id_registryprivacyid" TEXT PRIMARY KEY
	, "registry_id_companyid" TEXT NOT NULL
	, "registry_privacy" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (registry_id_companyid) REFERENCES company_table (id_companyid)
	);
	
CREATE TABLE "saved_evaluations_table" (
	"id_savedevaluationsid" TEXT PRIMARY KEY
	, "evaluation_name"	TEXT NOT NULL
	, "saved_evaluation_id_contactid"	TEXT
	, "saved_evaluation_id_companyid"	TEXT
	, "trait_name01"	TEXT
	, "trait_name02"	TEXT
	, "trait_name03"	TEXT
	, "trait_name04"	TEXT
	, "trait_name05"	TEXT
	, "trait_name06"	TEXT
	, "trait_name07"	TEXT
	, "trait_name08"	TEXT
	, "trait_name09"	TEXT
	, "trait_name10"	TEXT
	, "trait_name11"	TEXT
	, "trait_name12"	TEXT
	, "trait_name13"	TEXT
	, "trait_name14"	TEXT
	, "trait_name15"	TEXT
	, "trait_units11"	TEXT
	, "trait_units12"	TEXT
	, "trait_units13"	TEXT
	, "trait_units14"	TEXT
	, "trait_units15"	TEXT
	, "trait_name16"	TEXT
	, "trait_name17"	TEXT
	, "trait_name18"	TEXT
	, "trait_name19"	TEXT
	, "trait_name20"	TEXT
	, "trait_name01_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name01_deferred = 0 OR trait_name01_deferred = 1)
	, "trait_name02_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name02_deferred = 0 OR trait_name02_deferred = 1)
	, "trait_name03_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name03_deferred = 0 OR trait_name03_deferred = 1)
	, "trait_name04_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name04_deferred = 0 OR trait_name04_deferred = 1)
	, "trait_name05_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name05_deferred = 0 OR trait_name05_deferred = 1)
	, "trait_name06_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name06_deferred = 0 OR trait_name06_deferred = 1)
	, "trait_name07_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name07_deferred = 0 OR trait_name07_deferred = 1)
	, "trait_name08_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name08_deferred = 0 OR trait_name08_deferred = 1)
	, "trait_name09_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name09_deferred = 0 OR trait_name09_deferred = 1)
	, "trait_name10_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name10_deferred = 0 OR trait_name10_deferred = 1)
	, "trait_name11_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name11_deferred = 0 OR trait_name11_deferred = 1)
	, "trait_name12_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name12_deferred = 0 OR trait_name12_deferred = 1)
	, "trait_name13_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name13_deferred = 0 OR trait_name13_deferred = 1)
	, "trait_name14_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name14_deferred = 0 OR trait_name14_deferred = 1)
	, "trait_name15_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name15_deferred = 0 OR trait_name15_deferred = 1)
	, "trait_name16_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name16_deferred = 0 OR trait_name16_deferred = 1)
	, "trait_name17_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name17_deferred = 0 OR trait_name17_deferred = 1)
	, "trait_name18_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name18_deferred = 0 OR trait_name18_deferred = 1)
	, "trait_name19_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name19_deferred = 0 OR trait_name19_deferred = 1)
	, "trait_name20_deferred"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name20_deferred = 0 OR trait_name20_deferred = 1)
	, "trait_name01_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name01_optional = 0 OR trait_name01_optional = 1)
	, "trait_name02_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name02_optional = 0 OR trait_name02_optional = 1)
	, "trait_name03_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name03_optional = 0 OR trait_name03_optional = 1)
	, "trait_name04_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name04_optional = 0 OR trait_name04_optional = 1)
	, "trait_name05_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name05_optional = 0 OR trait_name05_optional = 1)
	, "trait_name06_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name06_optional = 0 OR trait_name06_optional = 1)
	, "trait_name07_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name07_optional = 0 OR trait_name07_optional = 1)
	, "trait_name08_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name08_optional = 0 OR trait_name08_optional = 1)
	, "trait_name09_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name09_optional = 0 OR trait_name09_optional = 1)
	, "trait_name10_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name10_optional = 0 OR trait_name10_optional = 1)
	, "trait_name11_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name11_optional = 0 OR trait_name11_optional = 1)
	, "trait_name12_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name12_optional = 0 OR trait_name12_optional = 1)
	, "trait_name13_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name13_optional = 0 OR trait_name13_optional = 1)
	, "trait_name14_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name14_optional = 0 OR trait_name14_optional = 1)
	, "trait_name15_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name15_optional = 0 OR trait_name15_optional = 1)
	, "trait_name16_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name16_optional = 0 OR trait_name16_optional = 1)
	, "trait_name17_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name17_optional = 0 OR trait_name17_optional = 1)
	, "trait_name18_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name18_optional = 0 OR trait_name18_optional = 1)
	, "trait_name19_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name19_optional = 0 OR trait_name19_optional = 1)
	, "trait_name20_optional"	INTEGER NOT NULL DEFAULT 0 CHECK (trait_name20_optional = 0 OR trait_name20_optional = 1)
	, "add_alert_summary"	INTEGER NOT NULL DEFAULT 0 CHECK (add_alert_summary = 0 OR add_alert_summary = 1) -- Boolean 1 = TRUE Add an alert with the data form the evaluation
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, "is_system_only"	INTEGER NOT NULL DEFAULT 0 CHECK (is_system_only = 0 OR is_system_only = 1)
	, FOREIGN KEY("saved_evaluation_id_companyid") REFERENCES "company_table"("id_companyid")
	, FOREIGN KEY("saved_evaluation_id_contactid") REFERENCES "contact_table"("id_contactid")
	, FOREIGN KEY("trait_name01") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name02") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name03") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name04") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name05") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name06") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name07") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name08") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name09") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name10") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name11") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name12") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name13") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name14") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name15") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name16") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name17") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name18") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name19") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_name20") REFERENCES "evaluation_trait_table"("id_evaluationtraitid")
	, FOREIGN KEY("trait_units11") REFERENCES "units_table"("id_unitsid")
	, FOREIGN KEY("trait_units12") REFERENCES "units_table"("id_unitsid")
	, FOREIGN KEY("trait_units13") REFERENCES "units_table"("id_unitsid")
	, FOREIGN KEY("trait_units14") REFERENCES "units_table"("id_unitsid")
	, FOREIGN KEY("trait_units15") REFERENCES "units_table"("id_unitsid")
	, CHECK ((saved_evaluation_id_contactid IS NULL AND saved_evaluation_id_companyid IS NOT NULL) OR (saved_evaluation_id_contactid IS NOT NULL AND saved_evaluation_id_companyid IS NULL))
	, CHECK((trait_name11 IS NULL AND trait_units11 IS NULL) OR (trait_name11 IS NOT NULL AND trait_units11 IS NOT NULL))
	, CHECK((trait_name12 IS NULL AND trait_units12 IS NULL) OR (trait_name12 IS NOT NULL AND trait_units12 IS NOT NULL))
	, CHECK((trait_name13 IS NULL AND trait_units13 IS NULL) OR (trait_name13 IS NOT NULL AND trait_units13 IS NOT NULL))
	, CHECK((trait_name14 IS NULL AND trait_units14 IS NULL) OR (trait_name14 IS NOT NULL AND trait_units14 IS NOT NULL))
	, CHECK((trait_name15 IS NULL AND trait_units15 IS NULL) OR (trait_name15 IS NOT NULL AND trait_units15 IS NOT NULL))
	, UNIQUE(saved_evaluation_id_contactid, saved_evaluation_id_companyid, evaluation_name)
);

CREATE TABLE "scrapie_flock_number_table" 
	("id_scrapieflocknumberid" TEXT PRIMARY KEY
	, "scrapie_flockid" TEXT NOT NULL UNIQUE
	, "scrapie_flock_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
		);

CREATE TABLE "scrapie_flock_owner_table" 
	("id_scrapieflockownerid" TEXT PRIMARY KEY
	, "owner_id_contactid" TEXT
	, "owner_id_companyid" TEXT
	, "owner_scrapie_flock_note" TEXT -- Used for underage owners who use their parents scrapie flock ID.
	, "id_scrapieflocknumberid" TEXT NOT NULL
	, "start_scrapie_flock_use" TEXT NOT NULL CHECK (start_scrapie_flock_use IS DATE (start_scrapie_flock_use))
	, "end_scrapie_flock_use" TEXT CHECK (end_scrapie_flock_use IS DATE (end_scrapie_flock_use))
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (owner_id_contactid) REFERENCES contact_table (id_contactid)
	, FOREIGN KEY (owner_id_companyid) REFERENCES company_table (id_companyid)
	, CHECK ((owner_id_contactid IS NULL AND owner_id_companyid IS NOT NULL) OR (owner_id_contactid IS NOT NULL AND owner_id_companyid IS NULL))
	);

CREATE TABLE "semen_extender_table" (
	"id_semenextenderid" TEXT PRIMARY KEY
	, "semen_extender_name" TEXT NOT NULL UNIQUE
	, "semen_extender_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "service_type_table" 
	("id_servicetypeid" TEXT PRIMARY KEY
	, "service_type" TEXT NOT NULL UNIQUE
	, "service_abbrev" TEXT NOT NULL UNIQUE
	, "service_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "sex_table" 
	("id_sexid" TEXT PRIMARY KEY
	, "sex_name" TEXT NOT NULL 
	, "sex_abbrev" TEXT NOT NULL
	, "sex_standard" TEXT NOT NULL
	, "sex_abbrev_standard" TEXT NOT NULL
	, "sex_display_order" INTEGER NOT NULL
	, "id_speciesid" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_speciesid) REFERENCES species_table (id_speciesid)
	);
	
CREATE TABLE "sheep_ebv_nsip_lambplan_cross_reference_table" 
	("id_sheepebvnsiplambplancrossreferenceid" TEXT PRIMARY KEY
	, "animaltrakker_table_name" TEXT NOT NULL
	, "animaltrakker_field_name" TEXT NOT NULL
	, "lambplan_field_name" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "sheep_fleece_weight_adjustment_table" 
	("id_sheepfleeceweightadjustmentid" TEXT PRIMARY KEY
	, "adjustment_name" TEXT NOT NULL UNIQUE
	, "ewe_age_2yr" REAL NOT NULL CHECK(0.0 < ewe_age_2yr)
	, "ewe_age_3yr" REAL NOT NULL CHECK(0.0 < ewe_age_3yr)
	, "ewe_age_4_5yr" REAL NOT NULL CHECK(0.0 < ewe_age_4_5yr)
	, "ewe_age_6_7yr" REAL NOT NULL CHECK(0.0 < ewe_age_6_7yr)
	, "ewe_age_8yr_up" REAL NOT NULL CHECK(0.0 < ewe_age_8yr_up)
	, "source_reference" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "sheep_lambs_born_adjustment_table" 
	("id_sheeplambsbornadjustmentid" TEXT PRIMARY KEY
	, "adjustment_name" TEXT NOT NULL UNIQUE
	, "ewe_age_1yr" REAL NOT NULL CHECK(0.0 < ewe_age_1yr)
	, "ewe_age_2yr" REAL NOT NULL CHECK(0.0 < ewe_age_2yr)
	, "ewe_age_3yr" REAL NOT NULL CHECK(0.0 < ewe_age_3yr)
	, "ewe_age_4yr" REAL NOT NULL CHECK(0.0 < ewe_age_4yr)
	, "ewe_age_5yr" REAL NOT NULL CHECK(0.0 < ewe_age_5yr)
	, "ewe_age_6yr" REAL NOT NULL CHECK(0.0 < ewe_age_6yr)
	, "ewe_age_7yr" REAL NOT NULL CHECK(0.0 < ewe_age_7yr)
	, "ewe_age_8yr" REAL NOT NULL CHECK(0.0 < ewe_age_8yr)
	, "ewe_age_9yr_up" REAL NOT NULL CHECK(0.0 < ewe_age_9yr_up)
	, "source_reference" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "sheep_nsip_ebv_table" 
	("id_sheepnsipebvid" TEXT PRIMARY KEY
	, "id_animalid" TEXT NOT NULL
	, "ebv_date" TEXT NOT NULL CHECK (ebv_date IS DATE (ebv_date))
	, "usa_maternal_index" REAL
	, "maternal_dollar_index" REAL
	, "maternal_dollar_acc" REAL
	, "ebv_birth_weight" REAL
	, "ebv_birth_weight_acc" REAL
	, "ebv_wean_weight" REAL
	, "ebv_wean_weight_acc" REAL
	, "ebv_maternal_birth_weight" REAL
	, "ebv_maternal_birth_weight_acc" REAL
	, "ebv_maternal_wean_weight" REAL
	, "ebv_maternal_wean_weight_acc" REAL
	, "ebv_post_wean_weight" REAL
	, "ebv_post_wean_weight_acc" REAL
	, "ebv_yearling_weight" REAL
	, "ebv_yearling_weight_acc" REAL
	, "ebv_hogget_weight" REAL
	, "ebv_hogget_weight_acc" REAL
	, "ebv_adult_weight" REAL 
	, "ebv_adult_weight_acc" REAL
	, "ebv_wean_fat" REAL
	, "ebv_wean_fat_acc" REAL 
	, "ebv_post_wean_fat" REAL
	, "ebv_post_wean_fat_acc" REAL
	, "ebv_yearling_fat" REAL
	, "ebv_yearling_fat_acc" REAL
	, "ebv_hogget_fat" REAL
	, "ebv_hogget_fat_acc" REAL
	, "ebv_wean_emd" REAL
	, "ebv_wean_emd_acc" REAL
	, "ebv_post_wean_emd" REAL
	, "ebv_post_wean_emd_acc" REAL
	, "ebv_yearling_emd" REAL
	, "ebv_yearling_emd_acc" REAL
	, "ebv_hogget_emd" REAL
	, "ebv_hogget_emd_acc" REAL
	, "ebv_wean_fec" REAL
	, "ebv_wean_fec_acc" REAL
	, "ebv_post_wean_fec" REAL
	, "ebv_post_wean_fec_acc" REAL
	, "ebv_yearling_fec" REAL
	, "ebv_yearling_fec_acc" REAL
	, "ebv_hogget_fec" REAL
	, "ebv_hogget_fec_acc" REAL
	, "ebv_post_wean_scrotal" REAL
	, "ebv_post_wean_scrotal_acc" REAL
	, "ebv_yearling_scrotal" REAL
	, "ebv_yearling_scrotal_acc" REAL
	, "ebv_hogget_scrotal" REAL
	, "ebv_hogget_scrotal_acc" REAL
	, "ebv_number_lambs_born" REAL
	, "ebv_number_lambs_born_acc" REAL
	, "ebv_number_lambs_weaned" REAL
	, "ebv_number_lambs_weaned_acc" REAL
	, "ebv_yearling_fleece_diameter" REAL
	, "ebv_yearling_fleece_diameter_acc" REAL
	, "ebv_hogget_fleece_diameter" REAL
	, "ebv_hogget_fleece_diameter_acc" REAL
	, "ebv_adult_fleece_diameter" REAL
	, "ebv_adult_fleece_diameter_acc" REAL
	, "ebv_yearling_greasy_fleece_weight" REAL
	, "ebv_yearling_greasy_fleece_weight_acc" REAL
	, "ebv_hogget_greasy_fleece_weight" REAL
	, "ebv_hogget_greasy_fleece_weight_acc" REAL
	, "ebv_adult_greasy_fleece_weight" REAL
	, "ebv_adult_greasy_fleece_weight_acc" REAL
	, "ebv_yearling_clean_fleece_weight" REAL
	, "ebv_yearling_clean_fleece_weight_acc" REAL
	, "ebv_hogget_clean_fleece_weight" REAL
	, "ebv_hogget_clean_fleece_weight_acc" REAL
	, "ebv_adult_clean_fleece_weight" REAL
	, "ebv_adult_clean_fleece_weight_acc" REAL
	, "ebv_yearling_fleece_yield" REAL
	, "ebv_yearling_fleece_yield_acc" REAL
	, "ebv_hogget_fleece_yield" REAL
	, "ebv_hogget_fleece_yield_acc" REAL
	, "ebv_adult_fleece_yield" REAL
	, "ebv_adult_fleece_yield_acc" REAL
	, "ebv_yearling_fiber_diameter_variation" REAL
	, "ebv_yearling_fiber_diameter_variation_acc" REAL
	, "ebv_hogget_fiber_diameter_variation" REAL
	, "ebv_hogget_fiber_diameter_variation_acc" REAL
	, "ebv_adult_fiber_diameter_variation" REAL
	, "ebv_adult_fiber_diameter_variation_acc" REAL
	, "ebv_yearling_staple_strength" REAL
	, "ebv_yearling_staple_strength_acc" REAL
	, "ebv_hogget_staple_strength" REAL
	, "ebv_hogget_staple_strength_acc" REAL
	, "ebv_adult_staple_strength" REAL
	, "ebv_adult_staple_strength_acc" REAL
	, "ebv_yearling_staple_length" REAL
	, "ebv_yearling_staple_length_acc" REAL
	, "ebv_hogget_staple_length" REAL
	, "ebv_hogget_staple_length_acc" REAL
	, "ebv_adult_staple_length" REAL
	, "ebv_adult_staple_length_acc" REAL
	, "ebv_yearling_fleece_curvature" REAL
	, "ebv_yearling_fleece_curvature_acc" REAL
	, "ebv_hogget_fleece_curvature" REAL
	, "ebv_hogget_fleece_curvature_acc" REAL
	, "ebv_adult_fleece_curvature" REAL
	, "ebv_adult_fleece_curvature_acc" REAL
	, "ebv_lambease_direct" REAL
	, "ebv_lambease_direct_acc" REAL
	, "ebv_lambease_daughter" REAL
	, "ebv_lambease_daughter_acc" REAL
	, "ebv_gestation_length" REAL
	, "ebv_gestation_length_acc" REAL
	, "ebv_gestation_length_daughter" REAL
	, "ebv_gestation_length_daughter_acc" REAL
	, "self_replacing_carcass_index" REAL
	, "self_replacing_carcass_acc" REAL
	, "self_replacing_carcass_no_repro_index" REAL
	, "self_replacing_carcass_no_repro_acc" REAL
	, "carcass_plus_index" REAL
	, "carcass_plus_acc" REAL
	, "lamb_2020_index" REAL
	, "lamb_2020_acc" REAL
	, "maternal_dollar_no_repro_index" REAL
	, "maternal_dollar_no_repro_acc" REAL
	, "dual_purpose_dollar_index" REAL
	, "dual_purpose_dollar_acc" REAL
	, "dual_purpose_dollar_no_repro_index" REAL
	, "dual_purpose_dollar_no_repro_acc" REAL
	, "which_run" TEXT NOT NULL
	, "ebv_yearling_number_lambs_born" REAL
	, "ebv_yearling_number_lambs_born_acc" REAL
	, "ebv_yearling_number_lambs_weaned" REAL
	, "ebv_yearling_number_lambs_weaned_acc" REAL
	, "border_dollar_index" REAL
	, "border_dollar_acc" REAL
	, "spare_index" REAL
	, "spare_index_acc" REAL
	, "coopworth_dollar_index" REAL
	, "coopworth_dollar_acc" REAL
	, "spare_2_index" REAL
	, "spare_2_index_acc" REAL
	, "export_index" REAL
	, "trade_index" REAL
	, "merino_dp_index" REAL
	, "merino_dp_acc" REAL
	, "merino_dp_plus_index" REAL
	, "merino_dp_plus_acc" REAL
	, "fine_medium_index" REAL
	, "fine_medium_acc" REAL
	, "fine_medium_plus_index" REAL
	, "fine_medium_plus_acc" REAL
	, "samm_index" REAL
	, "samm_acc" REAL
	, "dohne_no_repro_index" REAL
	, "dohne_no_repro_acc" REAL
	, "superfine_index" REAL
	, "superfine_acc" REAL
	, "superfine_plus_index" REAL
	, "superfine_plus_acc" REAL
	, "maternal_greasy_fleece_weight" REAL
	, "maternal_greasy_fleece_weight_acc" REAL
	, "maternal_clean_fleece_weight" REAL
	, "maternal_clean_fleece_weight_acc" REAL
	, "early_breech_wrinkle" REAL
	, "early_breech_wrinkle_acc" REAL
	, "late_breech_wrinkle" REAL
	, "late_breech_wrinkle_acc" REAL
	, "early_body_wrinkle" REAL
	, "early_body_wrinkle_acc" REAL
	, "late_body_wrinkle" REAL
	, "late_body_wrinkle_acc" REAL
	, "late_dag" REAL
	, "late_dag_acc" REAL
	, "id_ebvcalculationmethodid" TEXT NOT NULL
	, "intramuscular_fat" REAL
	, "intramuscular_fat_acc" REAL
	, "carcass_shear_force" REAL
	, "carcass_shear_force_acc" REAL
	, "dressing_percentage" REAL
	, "dressing_percentage_acc" REAL
	, "lean_meat_yield" REAL
	, "lean_meat_yield_acc" REAL
	, "hot_carcass_weight" REAL
	, "hot_carcass_weight_acc" REAL
	, "carcass_fat" REAL
	, "carcass_fat_acc" REAL
	, "carcass_eye_muscle_depth" REAL
	, "carcass_eye_muscle_depth_acc" REAL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_animalid) REFERENCES animal_table(id_animalid)
	, FOREIGN KEY (id_ebvcalculationmethodid) REFERENCES ebv_calculation_method_table(id_ebvcalculationmethodid)
	);
	
CREATE TABLE "sheep_weaning_adjustment_table" 
	("id_sheepweaningadjustmentid" TEXT PRIMARY KEY
	, "adjustment_name" TEXT NOT NULL UNIQUE
	, "ewe_age_1yr" REAL NOT NULL CHECK(0.0 < ewe_age_1yr)
	, "ewe_age_2yr" REAL NOT NULL CHECK(0.0 < ewe_age_2yr)
	, "ewe_age_3_6yr" REAL NOT NULL CHECK(0.0 < ewe_age_3_6yr)
	, "ewe_age_over_6yr" REAL NOT NULL CHECK(0.0 < ewe_age_over_6yr)
	, "lamb_sex_ram" REAL NOT NULL CHECK(0.0 < lamb_sex_ram)
	, "lamb_sex_ewe" REAL NOT NULL CHECK(0.0 < lamb_sex_ewe)
	, "lamb_sex_wether" REAL NOT NULL CHECK(0.0 < lamb_sex_wether)
	, "birth_and_rear_11" REAL NOT NULL CHECK(0.0 < birth_and_rear_11)
	, "birth_and_rear_12" REAL NOT NULL CHECK(0.0 < birth_and_rear_12)
	, "birth_and_rear_21" REAL NOT NULL CHECK(0.0 < birth_and_rear_21)
	, "birth_and_rear_22" REAL NOT NULL CHECK(0.0 < birth_and_rear_22)
	, "birth_and_rear_31" REAL NOT NULL CHECK(0.0 < birth_and_rear_31)
	, "birth_and_rear_32" REAL NOT NULL CHECK(0.0 < birth_and_rear_32)
	, "birth_and_rear_33" REAL NOT NULL CHECK(0.0 < birth_and_rear_33)
	, "source_reference" TEXT NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, UNIQUE (adjustment_name, source_reference)
	);
	
CREATE TABLE "species_table" 
	("id_speciesid" TEXT PRIMARY KEY
	, "species_common_name" TEXT NOT NULL
	, "species_generic_name" TEXT NOT NULL
	, "species_scientific_family" TEXT NOT NULL
	, "species_scientific_sub_family" TEXT
	, "species_scientific_name" TEXT NOT NULL
	, "early_male_breeding_age_days" INTEGER NOT NULL CHECK(0 < early_male_breeding_age_days)
	, "early_female_breeding_age_days" INTEGER NOT NULL CHECK(0 < early_female_breeding_age_days)
	, "early_gestation_length_days" INTEGER NOT NULL CHECK(0 < early_gestation_length_days)
	, "late_gestation_length_days" INTEGER NOT NULL CHECK(0 < late_gestation_length_days)
	, "typical_gestation_length_days" INTEGER NOT NULL CHECK(0 < typical_gestation_length_days)
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, CHECK(early_gestation_length_days < late_gestation_length_days)
	, CHECK(early_gestation_length_days <= typical_gestation_length_days AND typical_gestation_length_days <= late_gestation_length_days)
	);	
	
CREATE TABLE "state_table" 
	("id_stateid" TEXT PRIMARY KEY
	, "state_name" TEXT NOT NULL
	, "state_abbrev" TEXT NOT NULL
	, "id_countryid" TEXT NOT NULL
	, "state_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_countryid) REFERENCES country_table (id_countryid)
	, UNIQUE(id_countryid, state_name)
	, UNIQUE(id_countryid, state_abbrev)
	);
	
CREATE TABLE "tissue_sample_container_type_table"
	( "id_tissuesamplecontainertypeid" TEXT PRIMARY KEY
	, "tissue_sample_container_name" TEXT NOT NULL UNIQUE
	, "tissue_sample_container_abbrev" TEXT NOT NULL UNIQUE
	, "tissue_sample_container_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "tissue_sample_type_table"
	( "id_tissuesampletypeid" TEXT PRIMARY KEY
	, "tissue_sample_type_name" TEXT NOT NULL UNIQUE
	, "tissue_sample_type_abbrev" TEXT NOT NULL UNIQUE
	, "tissue_sample_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "tissue_test_table"
	( "id_tissuetestid" TEXT PRIMARY KEY
	, "tissue_test_name" TEXT NOT NULL UNIQUE
	, "tissue_test_abbrev" TEXT NOT NULL UNIQUE
	, "tissue_test_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);
	
CREATE TABLE "transfer_reason_table" 
	("id_transferreasonid" TEXT PRIMARY KEY
	, "transfer_reason" TEXT NOT NULL
	, "id_registry_id_companyid" TEXT NOT NULL
	, "transfer_reason_display_order" INTEGER NOT NULL 
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	, FOREIGN KEY (id_registry_id_companyid) REFERENCES company_table (id_companyid)
	, UNIQUE(id_registry_id_companyid, transfer_reason)
	);
	
CREATE TABLE "units_table" 
	("id_unitsid" TEXT PRIMARY KEY
	, "units_name" TEXT NOT NULL
	, "units_abbrev" TEXT NOT NULL
	, "id_unitstypeid" TEXT NOT NULL
	, "units_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK(created IS DATETIME(created))
	, "modified" TEXT NOT NULL CHECK(modified IS DATETIME(modified))
	, FOREIGN KEY (id_unitstypeid) REFERENCES units_type_table (id_unitstypeid)
	);
	
CREATE TABLE "units_type_table" 
	("id_unitstypeid" TEXT PRIMARY KEY
	, "unit_type_name" TEXT NOT NULL UNIQUE
	, "units_type_display_order" INTEGER NOT NULL
	, "created" TEXT NOT NULL CHECK (created IS DATETIME (created))
	, "modified" TEXT NOT NULL CHECK (modified IS DATETIME (modified))
	);

-- Insert Statements to fill the seed database with required data. 

INSERT INTO alert_type_table (id_alerttypeid, alert_type_name, alert_type_definition, created, modified) VALUES ('4d264192-63bc-4b81-8a1a-e5378f9c6649', 'User Defined', 'Put definition here', '2024-10-29 13:02:40', '2024-10-29 13:02:40');
INSERT INTO alert_type_table (id_alerttypeid, alert_type_name, alert_type_definition, created, modified) VALUES ('ab9b9fa3-fc41-4ec1-a1e7-a228fa35b042', 'Drug Withdrawal', '{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "DrugWithdrawal",
  "type": "object",
  "properties": {
    "type": {
      "type": "string",
      "enum": ["meat","milk"]
    },
    "drugId": {
      "type": "string",
      "description": "UUID of the drug that was applied."
    },
    "drugName": {
      "type": "string",
      "description": "Name of the drug that was applied."
    },
    "drugLot": {
      "type": "string",
      "description": "The lot from which the drug that was applied was taken."
    },
    "withdrawalUnitsId": {
      "type": "string",
      "description": "UUID of the temporal units of the withdrawal period."
    },
    "withdrawalDate": {
      "type": "string",
      "description": "The date on which the withdrawal period ends in yyyy-MM-dd format."
    }
  }
}', '2024-10-29 13:02:40', '2024-10-29 13:02:40');
INSERT INTO alert_type_table (id_alerttypeid, alert_type_name, alert_type_definition, created, modified) VALUES ('386c2c81-503a-43d7-b319-b24824864776', 'Evaluation', '{
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "EvaluationSummary",
  "type": "object",
  "properties": {
    "animalEvaluationId": {
      "type": "string",
      "description": "UUID of the results of an evaluation performed on an animal."
    },
    "evaluationId": {
      "type": "string",
      "description": "UUID of the evaluation that was performed on the animal."
    },
    "evaluationName": {
      "type": "string",
      "description": "Name of the evaluation that was performed on the animal."
    },
    "evaluationTraits": {
      "type": "array",
      "items": {
        "type": "object",
        "properties": {
          "id": {
            "type": "string",
            "description": "UUID of the evaluation trait."
          },
          "typeId": {
            "type": "string",
            "description": "UUID of the type of evaluation trait."
          },
          "name": {
            "type": "string",
            "description": "Name of the evaluation trait."
          },
          "value": {
            "type": "string",
            "description": "Value captured for the evaluation trait, including units if applicable."
          }
        }
      }
    }
  }
}
', '2024-10-29 13:02:40', '2024-10-29 13:02:40');

INSERT INTO animal_breed_table (id_animalbreedid, id_animalid, id_breedid, breed_percentage, created, modified) VALUES ('0d326d9f-68e3-41a9-a67f-423a5d1cc5df', '1a97246f-9539-423e-9a63-8cfefa2cbf14', '159efdfe-d990-472b-867e-df4493431aef', '100', '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_breed_table (id_animalbreedid, id_animalid, id_breedid, breed_percentage, created, modified) VALUES ('df07d553-fb4d-43b8-8b4b-c76b06150b40', 'bc63874d-e98a-4d19-8411-2282a9db42cd', '159efdfe-d990-472b-867e-df4493431aef', '100', '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_breed_table (id_animalbreedid, id_animalid, id_breedid, breed_percentage, created, modified) VALUES ('54e15727-a9e8-4fc6-b933-96fa9a5d9c4b', 'a1541ba6-34da-46f3-9d8c-e5370fb9fbcb', '80a6d312-fb6e-4ede-9950-8ebc7c776f6e', '100', '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_breed_table (id_animalbreedid, id_animalid, id_breedid, breed_percentage, created, modified) VALUES ('a1fa31e2-a501-49ce-97d9-f9eda80f7544', '55bdcb61-bec1-43a2-8f9c-3161614043b3', '80a6d312-fb6e-4ede-9950-8ebc7c776f6e', '100', '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_breed_table (id_animalbreedid, id_animalid, id_breedid, breed_percentage, created, modified) VALUES ('f2133ce4-0bb6-4619-bdc1-1d1e67e91570', 'b201b137-3c68-4d3e-a6da-82d3d6b5bd1d', '60ce071e-2e31-4d9c-bc27-ed9e8bd6c599', '100', '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_breed_table (id_animalbreedid, id_animalid, id_breedid, breed_percentage, created, modified) VALUES ('798a4d3f-cfab-4dd8-b96c-254ee9c61051', 'f2be46bd-4d0d-46c3-8207-91fdaa3030f5', '60ce071e-2e31-4d9c-bc27-ed9e8bd6c599', '100', '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_breed_table (id_animalbreedid, id_animalid, id_breedid, breed_percentage, created, modified) VALUES ('3b35d4c6-4603-46ff-923a-3708fc0e64d6', '1ae1135a-1faa-4a6e-bcf5-fa17cbffb9a0', '42da8979-eed2-45c5-b15a-e25f5248857a', '100', '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_breed_table (id_animalbreedid, id_animalid, id_breedid, breed_percentage, created, modified) VALUES ('84c50c5b-9865-4c9a-aea6-2aa8703169d4', '4f24e784-bc54-42ea-8fc2-af1cbd8b179a', '42da8979-eed2-45c5-b15a-e25f5248857a', '100', '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_breed_table (id_animalbreedid, id_animalid, id_breedid, breed_percentage, created, modified) VALUES ('f85f6dff-f62a-477b-86b6-62680811cd2f', 'a2287ebc-8289-4d23-8065-20f7a82609ec', 'da041fc5-ddee-4fb5-a3d9-669cb55bb8f6', '100', '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_breed_table (id_animalbreedid, id_animalid, id_breedid, breed_percentage, created, modified) VALUES ('f437917b-38ed-44ef-96b4-76ee6b53a2cc', '49c8e135-0424-483e-9f62-9ce65b9b80ed', 'da041fc5-ddee-4fb5-a3d9-669cb55bb8f6', '100', '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_breed_table (id_animalbreedid, id_animalid, id_breedid, breed_percentage, created, modified) VALUES ('36118fa6-fa6f-4345-a7f7-812e65eea435', '78d28f7d-a22e-46fc-bba3-c96043d250c5', 'e69d5da3-0866-45ae-9667-fb27ee3d7512', '100', '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_breed_table (id_animalbreedid, id_animalid, id_breedid, breed_percentage, created, modified) VALUES ('d9e4c6fe-3636-4d7f-b976-6cbdeb2b4feb', '87923aac-7259-435c-8e96-c8dced8483b7', 'e69d5da3-0866-45ae-9667-fb27ee3d7512', '100', '2024-05-14 10:09:13', '2024-05-14 10:09:13');

INSERT INTO animal_table (id_animalid, animal_name, id_sexid, sex_change_date, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, created, modified) VALUES ('1a97246f-9539-423e-9a63-8cfefa2cbf14', 'Unknown-Sheep Ram', '23c415a7-b60f-4231-8cfc-c205a79faef3',NULL,NULL,NULL, '7585ea2e-dcdf-41cb-94c1-4d133d624c1e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0',NULL, '2024-03-23 13:01:54', '2024-03-23 13:01:54');
INSERT INTO animal_table (id_animalid, animal_name, id_sexid, sex_change_date, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, created, modified) VALUES ('bc63874d-e98a-4d19-8411-2282a9db42cd', 'Unknown-Sheep Ewe', 'ef27e059-b9db-4fe5-8017-8b1e10acf59f',NULL,NULL,NULL, '7585ea2e-dcdf-41cb-94c1-4d133d624c1e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0',NULL, '2024-03-23 13:01:54', '2024-03-23 13:01:54');
INSERT INTO animal_table (id_animalid, animal_name, id_sexid, sex_change_date, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, created, modified) VALUES ('a1541ba6-34da-46f3-9d8c-e5370fb9fbcb', 'Unknown-Goat Buck', '125d0f5e-05bf-4b91-b5f2-d0bb2ea4b32e',NULL,NULL,NULL, '7585ea2e-dcdf-41cb-94c1-4d133d624c1e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0',NULL, '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_table (id_animalid, animal_name, id_sexid, sex_change_date, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, created, modified) VALUES ('55bdcb61-bec1-43a2-8f9c-3161614043b3', 'Unknown-Goat Doe', '9a725f0a-2979-4063-9602-136a49fec454',NULL,NULL,NULL, '7585ea2e-dcdf-41cb-94c1-4d133d624c1e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0',NULL, '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_table (id_animalid, animal_name, id_sexid, sex_change_date, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, created, modified) VALUES ('b201b137-3c68-4d3e-a6da-82d3d6b5bd1d', 'Unknown-Cattle Bull', '9363482e-341c-453c-977b-a4f2898fbaf1',NULL,NULL,NULL, '7585ea2e-dcdf-41cb-94c1-4d133d624c1e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0',NULL, '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_table (id_animalid, animal_name, id_sexid, sex_change_date, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, created, modified) VALUES ('f2be46bd-4d0d-46c3-8207-91fdaa3030f5', 'Unknown-Cattle Cow', 'd89896da-5f58-4e3f-a8ce-23e5a104508b',NULL,NULL,NULL, '7585ea2e-dcdf-41cb-94c1-4d133d624c1e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0',NULL, '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_table (id_animalid, animal_name, id_sexid, sex_change_date, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, created, modified) VALUES ('1ae1135a-1faa-4a6e-bcf5-fa17cbffb9a0', 'Unknown-Horse Stallion', 'f8482618-57e7-4dc2-beac-a1974338713f',NULL,NULL,NULL, '7585ea2e-dcdf-41cb-94c1-4d133d624c1e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0',NULL, '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_table (id_animalid, animal_name, id_sexid, sex_change_date, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, created, modified) VALUES ('4f24e784-bc54-42ea-8fc2-af1cbd8b179a', 'Unknown-Horse Mare', '3a53ad48-05bd-4e99-8261-70e412772000',NULL,NULL,NULL, '7585ea2e-dcdf-41cb-94c1-4d133d624c1e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0',NULL, '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_table (id_animalid, animal_name, id_sexid, sex_change_date, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, created, modified) VALUES ('a2287ebc-8289-4d23-8065-20f7a82609ec', 'Unknown-Donkey Jack', 'f7d2d691-e880-4907-bdcb-033f9fa55a3f',NULL,NULL,NULL, '7585ea2e-dcdf-41cb-94c1-4d133d624c1e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0',NULL, '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_table (id_animalid, animal_name, id_sexid, sex_change_date, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, created, modified) VALUES ('49c8e135-0424-483e-9f62-9ce65b9b80ed', 'Unknown-Donkey Jenny', 'eb885c80-59ea-4656-aa2d-9a7c27de7a75',NULL,NULL,NULL, '7585ea2e-dcdf-41cb-94c1-4d133d624c1e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0',NULL, '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_table (id_animalid, animal_name, id_sexid, sex_change_date, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, created, modified) VALUES ('78d28f7d-a22e-46fc-bba3-c96043d250c5', 'Unknown-Pig Boar', '8ff2b1e9-82a4-4a5d-b005-2e2ea406e22d',NULL,NULL,NULL, '7585ea2e-dcdf-41cb-94c1-4d133d624c1e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0',NULL, '2024-05-14 10:09:13', '2024-05-14 10:09:13');
INSERT INTO animal_table (id_animalid, animal_name, id_sexid, sex_change_date, birth_date, birth_time, id_birthtypeid, birth_weight, birth_weight_id_unitsid, birth_order, rear_type, weaned_date, death_date, id_deathreasonid, sire_id, dam_id, foster_dam_id, surrogate_dam_id, hand_reared, id_managementgroupid, created, modified) VALUES ('87923aac-7259-435c-8e96-c8dced8483b7', 'Unknown-Pig Sow', '24f10675-75e5-45da-83c9-37b72656c67a',NULL,NULL,NULL, '7585ea2e-dcdf-41cb-94c1-4d133d624c1e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0',NULL, '2024-05-14 10:09:13', '2024-05-14 10:09:13');

INSERT INTO animaltrakker_default_settings_table (id_animaltrakkerdefaultsettingsid, default_settings_name, owner_id_contactid, owner_id_companyid, owner_id_premiseid, breeder_id_contactid, breeder_id_companyid, breeder_id_premiseid, vet_id_contactid, vet_id_premiseid, lab_id_companyid, lab_id_premiseid, id_registry_id_companyid, registry_id_premiseid, id_stateid, id_countyid, id_flockprefixid, id_breedid, id_sexid, id_idtypeid_primary, id_idtypeid_secondary, id_idtypeid_tertiary, id_eid_tag_male_color_female_color_same, eid_tag_color_male, eid_tag_color_female, eid_tag_location, id_farm_tag_male_color_female_color_same, farm_tag_based_on_eid_tag, farm_tag_number_digits_from_eid, farm_tag_color_male, farm_tag_color_female, farm_tag_location, id_fed_tag_male_color_female_color_same, fed_tag_color_male, fed_tag_color_female, fed_tag_location, id_nues_tag_male_color_female_color_same, nues_tag_color_male, nues_tag_color_female, nues_tag_location, id_trich_tag_male_color_female_color_same, trich_tag_color_male, trich_tag_color_female, trich_tag_location, trich_tag_auto_increment, trich_tag_next_tag_number, id_bangs_tag_male_color_female_color_same, bangs_tag_color_male, bangs_tag_color_female, bangs_tag_location, id_sale_order_tag_male_color_female_color_same, sale_order_tag_color_male, sale_order_tag_color_female, sale_order_tag_location, use_paint_marks, paint_mark_color, paint_mark_location, tattoo_color, tattoo_location, freeze_brand_location, id_idremovereasonid, id_tissuesampletypeid, id_tissuetestid, id_tissuesamplecontainertypeid, birth_type, rear_type, minimum_birth_weight, maximum_birth_weight, birth_weight_id_unitsid, weight_id_unitsid, sale_price_id_unitsid, death_reason_id_contactid, death_reason_id_companyid, id_deathreasonid, id_transferreasonid, created, modified) VALUES ('29753af4-2b46-49b3-854c-4644d8919db6', 'Standard Settings', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '06a2f695-5c34-41d3-b20c-49386749119b', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '4a04fcfe-889f-4cd9-8abe-bdd59807ad07', '7b8bfd81-3021-401a-a174-9ec109a36e2e', '4a04fcfe-889f-4cd9-8abe-bdd59807ad07', 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '4a04fcfe-889f-4cd9-8abe-bdd59807ad07', 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '4a04fcfe-889f-4cd9-8abe-bdd59807ad07', '3f432960-e734-4f4e-8fe5-382383233aa2', NULL, NULL, '62e70bd7-13b2-4cd4-b155-aa8b13b96c15', 'ef27e059-b9db-4fe5-8017-8b1e10acf59f', '50f1c64f-e56e-420e-8150-9347fe51c0c1', '6af3845e-0abc-4afa-bcb4-4eea96f2ecc2', NULL, 1, '709c3963-bbe6-4524-bc9d-a0fc3b754e68', '709c3963-bbe6-4524-bc9d-a0fc3b754e68', '107da4a3-fec3-4ba3-97ae-898b387bfc7f', 1, 1, 5, 'd7228b86-cb15-4e16-bec7-5ee956de9641', 'd7228b86-cb15-4e16-bec7-5ee956de9641', '7969cccf-0c6f-49ab-ba1d-9a9b7f2012b1', 1, '8b382ec8-863f-4888-a8fe-c3abbd9f214f', '8b382ec8-863f-4888-a8fe-c3abbd9f214f', '7969cccf-0c6f-49ab-ba1d-9a9b7f2012b1', 0, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, NULL, 1,'d7228b86-cb15-4e16-bec7-5ee956de9641', 'd7228b86-cb15-4e16-bec7-5ee956de9641', '7969cccf-0c6f-49ab-ba1d-9a9b7f2012b1', 0,NULL, NULL, NULL, 1,'d7228b86-cb15-4e16-bec7-5ee956de9641', '2e4170b9-eacc-433c-bb58-897db91051cc', NULL, NULL, NULL, 'be98c69d-e7c5-4b0b-b2c3-d2bed29a9362', 'ae1e540e-4b62-4a15-9e9b-d1ff1bdf6675', 'c83904b7-fcbb-4437-9a16-14a78bbd61f5', 'cd94c4de-0bf6-4577-8908-00f5ed66cc47', '867bb971-68ae-4539-9b02-b27a22815305', NULL, 0.0, 15.0, 'fe4678ef-c2a0-470e-950b-b25088867ed6', 'fe4678ef-c2a0-470e-950b-b25088867ed6', '9b023940-1cc1-455a-996e-166700750650', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', 'da1178b9-e4b9-4b25-8578-987aa5a8ecd2', '2d3e0aed-84c9-48e8-b11f-00fad83288c6', '2024-05-07 10:48:49', '2024-05-07 10:48:49');

INSERT INTO animaltrakker_metadata_table (id_animaltrakkermetadataid, database_version, created, modified) VALUES ('dd7ce5d0-4aa5-44cd-bfe4-97e35259430f', '6.0.0', '2024-07-23 10:15:53', '2024-09-29 07:29:35');

INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('3323b31f-c478-4d2b-abfe-1fa12e73e32a', 'Single', 'S', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('867bb971-68ae-4539-9b02-b27a22815305', 'Twin', 'T', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('4ddf6aaa-009e-4a15-8c8b-f5eec5f8ff6e', 'Triplet', 'Tr', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('eee07d45-86a8-4488-8bb4-7a6846e69e39', 'Quadruplet', '4', '4', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('330e60fe-7e2b-4a3c-8e5c-8e3e6daeac2d', 'Quintuplet', '5', '5', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('0ba99855-f888-4504-ac8f-6a54ffea0187', 'Sextuplet', '6', '6', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('335714b4-c190-438f-9c25-7c75cbac391a', 'Seven', '7', '7', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('40674fc7-693a-4e21-a3bc-256c04902d96', 'Eight', '8', '8', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('12e577ea-33c6-4e30-b44b-bb7233786881', 'Nine', '9', '9', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('50dd4e31-3b06-4720-9caa-af3ff2f84dde', 'Ten', '10', '10', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('502d34c6-3163-49ad-95a8-daaf2aa63cc3', 'Eleven', '11', '11', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('cd090c16-55e3-4b03-b81b-cd0fc06ae41a', 'Twelve', '12', '12', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('2331d919-2965-4def-8672-8fc38258471d', 'Thirteen', '13', '13', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('471eeca3-6c09-42c0-8c9e-a0ec71654be5', 'Fourteen', '14', '14', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('e1737870-90b7-43f4-b1aa-ac6d2d1d299e', 'Fifteen', '15', '15', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('71946994-2faf-4b73-9122-966e22893e9e', 'Sixteen', '16', '16', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO birth_type_table (id_birthtypeid, birth_type, birth_type_abbrev, birth_type_display_order, created, modified) VALUES ('7585ea2e-dcdf-41cb-94c1-4d133d624c1e', 'Unknown', 'U', '42', '2024-02-18 14:37:57', '2024-02-18 14:37:57');

INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e8d5cda1-92ac-4173-b97b-880beb987371', 'Black Welsh Mountain', 'BW', '12', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('a29be365-3056-4fe4-9113-6808473ed539', 'Crossbred', 'X', '8', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('ca26a140-a91f-42fc-ab09-9d12f6846f84', 'Shetland', 'SL', '53', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('a9d3f92e-4a82-4355-91a4-ce4599865e65', 'Chocolate Welsh Mountain', 'CH', '13', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('004e8c9c-584a-405c-91d4-64f1e968a67a', 'White Welsh Mountain', 'WW', '14', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('159efdfe-d990-472b-867e-df4493431aef', 'Unknown', 'UNK', '10', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('62e70bd7-13b2-4cd4-b155-aa8b13b96c15', 'Black Welsh Mountain-UK', 'BW-UK', '15', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('d7d40699-502a-462b-b01d-dab3e4259899', 'Black Angus', 'AN', '1', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e93ce945-ccf9-403b-97e1-94a9de01ea48', 'Red Angus', 'AR', '2', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('12043966-0219-48af-b99b-0c3373a30877', 'Charolais', 'CH', '3', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c109430e-c46b-42a0-9e41-86cedb8f8b79', 'Gelbvieh', 'GV', '4', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('39e22ccb-2ea0-4196-839e-c22c64d14feb', 'Hereford Polled', 'HP', '5', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('65c7523b-a05b-42ca-ab98-cd89ccd9ec83', 'Holstein', 'HO', '6', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('7ce8150f-1ded-45fa-8b5d-03286bd6c077', 'Simmental', 'SM', '7', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c8412c79-9e31-4128-99b9-f603d8d3b827', 'Limousin', 'LM', '8', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('42540f6e-d9ca-42b3-9576-a73e25823e96', 'Black Baldy', 'Bk Baldy', '9', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('5275043f-dc6d-4bf5-97aa-1fcec7f48c12', 'Composite', 'Comp', '10', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('b0488f67-98b5-4d3a-ba33-71bab81db80c', 'Balancer', 'Bal', '11', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('60ce071e-2e31-4d9c-bc27-ed9e8bd6c599', 'Unknown', 'UNK', '12', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('d79c5acc-97f3-43df-a2a1-deb1a18b7b8f', 'Pinzgauer', 'PZ', '13', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('bd3fb275-368c-415e-afbf-35813deef129', 'Crossbred', 'XX', '14', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('def92fea-5ffb-4f5f-9abb-99e364435a12', 'Canadienne', 'Can', '15', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('b69bb08d-651d-4855-b35d-e9a6385eae33', 'Milking Devon', 'DE', '16', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c848347b-26eb-4ddf-8944-55c4cb331c4c', 'Randall Lineback', 'RL', '17', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('2262cc48-c4bf-4aaa-b7f7-b288737cf63d', 'Texas Longhorn', 'TL', '18', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c9fd30bb-d26e-4283-9533-350cd54b54b5', 'Florida Cracker', 'FC', '19', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('5ce34a52-c43e-4108-aab7-b3b2e3a9aee1', 'Pineywoods', 'PN', '20', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('cff7bcc1-3c6b-4d75-a3a7-f9f8a7f66eac', 'Dutch Belted', 'DL', '21', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('dbc43ba8-b53e-4b1b-8323-da8f7c12e32b', 'Shorthorn Beef', 'SS', '22', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('1b143f62-45f0-4f1f-b93c-058d20b6665e', 'Red Poll', 'RP', '23', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('931b85d0-70a4-411e-8425-57d0c59df50f', 'Guernsey', 'GU', '24', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('078a328f-c415-4347-ac66-77244e2f46fd', 'Dexter', 'DR', '25', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('4a28a794-5f5a-417a-b904-7cf865034db7', 'Red Devon', 'RV', '26', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('f7b571a4-0f7a-42fa-ad24-1563eaf7d0ea', 'Kerry', 'KR', '27', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('291216fe-6855-41f2-af72-fda75b805a1f', 'Lincoln Red', 'LR', '28', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('40c677cd-70ca-44df-8459-d7170262bcd5', 'Ancient White Park', 'WP', '29', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('52777f19-c8f6-41de-8c75-6360075f3ec0', 'Belted Galloway', 'BG', '30', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('536459da-cb7b-4258-bb0f-b3c6f3bc6e35', 'Ankole-Watusi', 'AW', '31', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('bba5a652-b46a-4d1c-bf62-30298ca60dbb', 'Florida Cracker', 'FC', '30', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('3b066470-3b7a-4102-87c6-de842e0c32eb', 'Gulf Coast', 'GC', '31', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('d79a72e2-bd33-40d6-ac04-c7cde03577e5', 'Hog Island', 'HG', '32', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c1fc1a44-b707-452b-8fe1-3372c0402b0d', 'Navajo Churro', 'NV', '43', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('53c6c8c8-4cc4-4649-9041-d162e91c6843', 'Santa Cruz', 'SZ', '51', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('80e2cda3-8dba-4e75-b777-e64d71647447', 'Jacob', 'JA', '35', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('398fca17-6801-491f-a84f-e3b456cd55f7', 'Karakul', 'KK', '36', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('8a0f2359-c6e9-4252-97a4-b8a281789d7c', 'Romedale/CVM', 'RD', '48', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('812942c9-f57d-41e7-8d86-04285ca227ef', 'Barbados Blackbelly', 'LY', '16', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e97d98fc-d6c3-4a7a-aeeb-c4bd84419918', 'St. Croix', 'SX', '57', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('edacfb77-72e5-4307-ba8f-3d43578ec474', 'Tunis', 'TU', '61', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c9fb974b-d340-43a0-a6c8-f637bad08608', 'Cotswold', 'CW', '24', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('85d7d91b-0828-4234-8c2f-c9a6aab3026c', 'Dorset Horn', 'DH', '26', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('db3061d4-45ac-49b2-b6f6-af8fc1f8181a', 'Lincoln', 'LI', '41', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('44fbd8ae-4c65-49d9-8884-05c655813dd6', 'Oxford', 'OX', '45', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('2f2ee631-194e-4d1e-9c0b-f89df3fb0bc3', 'Shropshire', 'SR', '54', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('8c6b05b2-22aa-47e5-a282-4cb679398c64', 'Southdown', 'ST', '56', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e446f86a-e1d4-41fe-a3ea-75323417ecb3', 'Teeswater', 'TW', '59', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('9c3aa940-bd44-4820-9473-3b73e9e20e14', 'Soay', 'SY', '55', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('665460c5-8c3f-4428-9aed-07f5c21aab45', 'Clun Forest', 'CF', '21', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('05cc7965-f4ec-4471-b86b-5ab40f53fbf5', 'Wiltshire Horn', 'WH', '62', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('d006deda-37fc-4e65-87eb-91b35e0c474f', 'Blue Faced Leister', 'BF', '17', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('dcdeb75b-d2ef-44c0-8fd7-294059a7febc', 'Border Cheviot', 'BC', '18', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('452c392c-1c24-4694-bb45-db1ee3408fef', 'Charollais', 'CO', '20', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c382c5c8-7ca9-4cc9-af8a-ea849b99a698', 'Columbia', 'CL', '1', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('f290d4cc-4bea-4579-beec-87502e4a7b17', 'Coopworth', 'CP', '22', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('b3165ede-34bb-48b5-aac6-71cab8ec15bb', 'Corriedale', 'CR', '23', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('09a2ebfe-7a25-40eb-a0a5-8cbf4c5dd14d', 'Dorper', 'DO', '25', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e493c81f-7a3e-4c55-a97a-bc5682d97c99', 'Drysdale', 'DY', '27', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('b4056ad8-e612-4e9b-8bbd-9e27c9f8cace', 'East Friesian', 'EF', '28', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('983d11b1-ff6b-4548-9e76-524362004d68', 'Finnish Landrace', 'FN', '29', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('7af37dd7-6dea-42e5-a638-f96fecf61367', 'Hampshire', 'HS', '6', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('b3292d42-ce28-4b39-aea2-616088239354', 'Icelandic', 'IL', '33', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('4b06b22b-e7e6-4d74-94ba-c0fba4fd1c8f', 'Ile de France', 'IF', '34', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('ef32f2c1-a9a3-4828-86be-39e83a92915e', 'Leicester Longwool', 'LL', '40', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('3a604e01-bd6f-453a-a92e-6b90ce0cdee0', 'Katahdin', 'KA', '37', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('5d6dc505-db31-4c4b-b8c6-f755db1c57c6', 'Kerry Hill', 'KH', '38', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('bfbfff3e-603c-4ce4-9f60-1e07d0b7059a', 'Lacaune Dairy Sheep', 'CU', '39', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('a1f66d2a-b577-46b9-a6c1-4c4185d9911d', 'Border Leicester', 'BL', '19', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('167eab60-646f-4e3f-81ed-4f22110859c8', 'Merino', 'MM', '3', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('85181c20-3b3a-42e9-af72-00f0e9de8efe', 'Merino Polled', 'MP', '4', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('4f41b332-36e5-4333-844b-5730d4f4ea41', 'Montadale', 'MT', '42', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('910160d9-6a3a-4c83-94b8-c43cb7438ecb', 'North Country Cheviot', 'NC', '44', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('a1c21889-468f-4b10-88a1-a1b518c3ca10', 'Perendale', 'PE', '46', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e759adf6-7ae0-4fc1-8f3b-d53b277f9b1c', 'Polypay', 'PP', '11', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('d0aff59b-e4fa-4e02-ab70-f79069ce2977', 'Rambouillet', 'RG', '2', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('7071a7b1-2a03-4eac-8b92-bc00278c8d86', 'Romanov', 'RV', '47', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('a5e9b3f9-21ac-4ee5-a433-778c18fb64ac', 'Romney', 'RY', '49', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('695be8ea-cab3-4b6b-b68c-8fddb36f724b', 'Ryeland', 'RL', '50', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('3e281f5b-ab3f-40aa-8de2-dd6e335f1c2b', 'Scottish Blackface', 'SC', '52', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('99feadc3-450f-4711-8c57-5906a5fc1c9a', 'Suffolk', 'SU', '7', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('aab3e305-2792-45cd-8acb-09ca09df5c9b', 'Targhee', 'TA', '58', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('a22c5fb6-9258-41b9-a058-f734bb43c537', 'Texel', 'TX', '60', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('d68b8c24-4465-4e28-a02f-a44ba50cb2ee', 'Ayrshire', 'AY', '32', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c194e8e8-96df-400f-8d97-0a4e7770dc4e', 'Beefalo', 'BE', '33', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('1d36ee3f-2b44-4d7f-aa07-9c88e1cbb0f2', 'Beefmaster', 'BM', '34', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('5259ca23-59e6-49ce-9f30-7ad9bf60f888', 'Belgian Blue', 'BB', '35', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('b7684b4a-3d83-4dd8-8dd2-f8f262c3d160', 'Brahman', 'BR', '36', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('23885cc1-ea9c-401b-b8e3-49402bc9ab85', 'Brangus', 'BN', '37', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('cae36cc5-7b48-420a-acc2-bf211a0015e5', 'Brown Swiss', 'BS', '38', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('40b98794-70e5-48e3-a353-b6ac4bbb6fdd', 'Chianina', 'CA', '39', '3', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('0940d56d-63b5-4099-b9ab-60123793d78b', 'Hereford Horned', 'HH', '40', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('745d39a5-6e70-4b85-b8d9-f0870288d31e', 'Highland Scotch', 'SH', '41', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('6a248b19-13bb-47ef-92de-0fc712c932ac', 'Jersey', 'JE', '42', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('2acd0010-074d-405b-a1a2-def2c8b17191', 'Mexican Corriente', 'MC', '43', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e698d965-5b3d-4b7a-bee2-d0e2d3f98da8', 'Murray Grey', 'MG', '44', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e4d9e8c6-089b-47fb-8001-a88820ec77a2', 'Normande', 'NM', '45', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('2f251cc0-55a7-45f8-8dcf-d20e5a97d92e', 'Norwegian Red', 'NR', '46', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('a1c4a138-a0f6-4530-80f7-bb47cd9e7b85', 'Piedmontese', 'PI', '47', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('ef2d7225-11e2-4e29-ac4a-4889335eade1', 'Red Holstein', 'WW', '48', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('f123a171-ca15-4efc-8e0f-52e4f625a708', 'Salers', 'SA', '49', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('6df63c34-d405-4696-ae3c-0d40725c462f', 'Santa Gertrudis', 'SG', '50', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('cc8e0da4-5980-432e-ac2e-f82f399ca19c', 'Alpine', 'AI', '1', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('65112cc5-1cde-45c5-87e7-60c90cdeac71', 'Angora', 'AG', '2', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('fbb16aba-6cdb-462c-b4b7-5ee249ee3308', 'Arapawa', 'AR', '3', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('8f42d588-204c-49f0-b80b-6c03218ed445', 'Boer', 'BZ', '4', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('ea409ebf-4886-4f35-a727-f15f304c1ae4', 'Cashmere', 'CS', '5', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('0a5c3c43-904a-495f-b54d-07608e1bc3e3', 'La Mancha', 'LN', '6', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('f66e48ff-fb24-4a8b-afbc-55d2b5ae5aa3', 'Myotonic', 'MY', '7', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('54f34d39-4a5f-4515-97f8-179b1cfe07f4', 'Nigerian Dwarf', 'ND', '8', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('350be125-78f2-4511-8631-d04324aae292', 'Nubian', 'NU', '9', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('4848819a-557d-4922-a936-1a97dac149b3', 'Oberhasli', 'OH', '10', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('aa88349f-6af6-4308-8680-a735ac2d7bde', 'Pygmy', 'PY', '11', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e070741f-d152-4430-b7e5-d1ae4d717c13', 'Saanen', 'EN', '12', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('d94f4f8c-e05f-46db-be91-14263268fad2', 'San Clemente Island', 'SC', '13', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('21f2b641-cb4c-4711-8811-f7202a8f0759', 'Spanish', 'SP', '14', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('52ef90cc-4af7-4c2b-af04-24a8242ac369', 'Toggenburg', 'TO', '15', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c6678f69-cb3d-4913-ab74-8bdca142024d', 'Berkshire', 'BK', '1', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('adb80cdd-8817-44a9-8559-9d2e573d8fa8', 'Chester White', 'CW', '2', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('f56538ad-c04f-40a6-82fe-3d34a9274bb6', 'Choctaw', 'CH', '3', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('dd3f8bf6-81d3-4d21-ba2d-5c15eb7f65e4', 'Duroc', 'DU', '4', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('67d63d24-2894-4a35-960d-7e67abc0ca97', 'Gloucestershire Old Spots', 'GS', '5', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('ae850b50-9277-463e-87b7-c75605911607', 'Guinea Hog', 'GH', '6', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('0cf522aa-fe80-4568-9f7d-a81a130e1f5b', 'Hampshire', 'HA', '7', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e03870c9-d813-43dc-a895-92da856baf86', 'Hereford', 'HE', '8', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('45a852e2-addf-4140-b9ba-acf0c99aedcc', 'Lacombe', 'LC', '9', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('aba9bd43-6c84-4c9f-ad3c-f5a8d3152c8e', 'Landrace', 'LA', '10', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e48d029f-197f-45d5-8518-d6e8d81b5f8b', 'Large Black', 'LB', '11', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('740e2994-c05b-4616-893a-a5cd9d2bff68', 'Large White', 'LW', '12', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('1efd0dca-a50a-48a9-b92b-71252777ebf3', 'Meishan', 'MS', '13', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('f9ab5408-d0fa-4276-9033-5070f85780e4', 'Mulefoot', 'MF', '14', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('26f1ecc7-b955-4780-9c0d-97db40b88d39', 'Ossabaw Island', 'OI', '15', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('0c225f2e-ead1-4238-ac7b-f49825668783', 'Pietrain', 'PE', '16', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('b582baa2-dec2-44bb-9011-903b870d4fca', 'Poland China', 'PC', '17', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('4e80b60c-21b2-4239-8d97-00832ee12f1e', 'Red Wattle', 'RW', '18', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('0313e1e4-bd39-47d5-bc0f-e88eb27bdae9', 'Saddleback', 'SB', '19', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('cbda60fe-5582-433a-81dd-873005e5a7d0', 'Spotted', 'SO', '20', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('f6b10420-7efd-4e48-b664-8cdf54868ada', 'Tamworth', 'TM', '21', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('5b9f61ad-4633-497c-a6f8-d2a1a4b150ed', 'Wessex Saddleback', 'WS', '22', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('a7e5e090-042a-4024-a785-3657f01cbbd2', 'Yorkshire', 'YO', '23', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('051a0ebc-5723-4808-82ab-1697ae89ce09', 'American Mammoth', 'AMD', '1', '67966e17-2e3d-44e3-9401-a2c6112fdbb4', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('3b0f96f8-833b-4450-8427-3a8cdbef35c1', 'Poitou', 'POD', '2', '67966e17-2e3d-44e3-9401-a2c6112fdbb4', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('f613a894-1491-4fa6-b58c-e2082b4f39b1', 'Miniature', 'MND', '3', '67966e17-2e3d-44e3-9401-a2c6112fdbb4', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('43d96b31-5020-40cf-be29-e3f0b4233dca', 'Standard', 'DOK', '4', '67966e17-2e3d-44e3-9401-a2c6112fdbb4', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('55276d3e-df59-44cd-899d-26814da256f6', 'Wild Burro', 'WDB', '5', '67966e17-2e3d-44e3-9401-a2c6112fdbb4', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('9b1e48b2-b04b-4b5d-ab3d-5b5f88f2fc26', 'Mule', 'MUL', '6', '67966e17-2e3d-44e3-9401-a2c6112fdbb4', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('4bbc7b65-2fa4-4e44-9a3f-4caf1992bfc5', 'Akhal-Teke', 'AKT', '1', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('75ed8284-2619-4752-850c-9f3c47200a53', 'American Bashkir Curley', 'ABC', '2', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c83b1b00-c57a-4a60-9adf-c410705e6376', 'American Cream', 'CD', '3', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('4a02a643-5e01-4466-a732-7d03239246fb', 'American Miniature', 'AMH', '4', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('735d22a9-ffab-4fb9-a63a-9175f3967695', 'American Saddlebred', 'AS', '5', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('09ebecd0-698d-4c75-8342-45bc995b5b77', 'Andalusian', 'AA', '6', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('14ff29fc-6264-439e-a36b-bb8db83e88d4', 'Appaloosa', 'AP', '7', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e0e4c868-d839-428c-8c58-bd34cae3cb09', 'Arabian', 'AD', '8', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('ad54adff-f9d7-4ea9-a05c-85283a342999', 'Banker', 'BAK', '9', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('4387b50e-849e-43ac-9af6-4bf15c898818', 'Barb', 'BAR', '10', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('66034c7c-4d41-4185-ab26-7fa494489d58', 'Bashkir Curly', 'BAC', '11', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('bdc4658d-abc4-4b98-98ff-4205a778a1bd', 'Bavarian Warmblood', 'BY', '12', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('6067e51f-b0c1-4e00-af92-93e41b9b50eb', 'Belgian', 'GI', '13', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('b67421b6-7ccb-452c-ba6d-d962b3470f6c', 'Brabant', 'BB', '14', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('bb9ec0f9-a025-4677-b8f7-cfeaa1b6111c', 'Canadian', 'CI', '15', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('a4a4019d-d31b-46ab-bb8b-6be907cc54c5', 'Caspian', 'CAS', '16', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('9bbafa04-6082-4f0d-b395-bc07c468d253', 'Chincoteague Pony', 'CHP', '17', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('8bbdebd3-1de2-4bee-9272-1dad911b8e7d', 'Cleveland Bay', 'CV', '18', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('5e0b9f2a-8283-4bbd-998a-b273576ec05a', 'Clydesdale', 'CY', '19', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('28540bf6-f1f1-45de-b8bc-90afc9ce2fd3', 'Colonial Spanish', 'CS', '20', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('b0ba895d-d6bb-4423-92e3-c5d435c8bf31', 'Colorado Ranger Horse', 'CRH', '21', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('ddc5221e-432d-4609-9349-e07a0252025b', 'Connemara Pony', 'CM', '22', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('8680f3f8-6c8c-4459-83c6-27547f482254', 'Dales Pony', 'DAP', '23', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('61b081e7-a5fd-414c-86af-be1ba6ba1aa8', 'Danish Warmblood', 'DW', '24', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('dd326436-c4a4-4f77-8928-403542cbcfb4', 'Dartmoor', 'DT', '25', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('bd43a566-c063-416f-8975-eada6cc7d09d', 'Dutch Warmblood', 'DWB', '26', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('dfb71225-d8f6-44f6-965a-98825eb9166e', 'Exmoor', 'EX', '27', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('dc1ed4dd-cc6e-42d1-b209-a4d14fe52314', 'Fell Pony', 'FE', '28', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('cb6efb34-b781-400a-9f94-07197a8b977b', 'Finnhhorse', 'FIN', '29', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('0945d0c8-7207-4e4f-86dd-9c0c966771f0', 'Fjord', 'FJ', '30', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('3a36685c-8dae-4fe1-94df-3716fe8d1eb6', 'Florida Cracker', 'FLC', '31', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('0c302eab-430c-4bf8-b440-2a4f6c59c8fd', 'Friesan', 'FR', '32', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('a3943f67-f9b0-4b76-bfc3-92945027b2ee', 'Galiceno', 'GAL', '33', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('0c61e470-3d7b-48c6-bce8-0567794e11a3', 'German Warmblood', 'GER', '34', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('63114ed9-2f16-4c03-b54b-f54b9e845140', 'Gotland', 'GOT', '35', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('5582da03-7986-4202-be60-5d180905ae3f', 'Hackney Horse', 'HN', '36', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('2f863da5-25d0-47b5-945e-40965d8c177b', 'Hackney Pony', 'HK', '37', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('be8159b6-bbce-468d-95ee-c4b81b378c1c', 'Haflinger', 'HF', '38', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e267accf-4c7e-433a-87ce-df91cbf3bc93', 'Hanoverian', 'HV', '39', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('f7105733-0dc7-4788-b938-393c98a270fb', 'Highland Pony', 'HG', '40', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('d39454d1-d9c3-4191-aa2f-7a52f4a247fb', 'Icelandic', 'IC', '41', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('a477dc8e-dd83-443c-9b7f-226b33a6e274', 'Irish Draught', 'IRD', '42', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('a20222b9-c8cc-4686-8049-2ded79ef60d7', 'Jutland', 'JUT', '43', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('d0d03a03-adb9-4504-ae1a-44772dc11214', 'Lipizzan', 'LZ', '44', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('ccbd494f-a6bf-49e1-a9a8-bf7319bf699e', 'Lusitano', 'LUS', '45', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('5d243f23-f366-4b7d-8bea-deef07426173', 'Marsh Tacky', 'MT', '46', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('d5139806-e638-425a-b5cc-7a76f23e1954', 'Miniature Horse', 'MNH', '47', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('54199547-70a9-4011-be55-ddc7d4b46fdd', 'Morab', 'MOB', '48', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('5e596b85-9369-4262-8f17-d4a3982e8440', 'Morgan', 'MND', '49', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('005729ea-64c5-4fc7-90f0-600e8a64377f', 'National Show Horse', 'NAT', '50', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('80dfa283-805b-48cf-adb7-217aa943e17d', 'New Forest Pony', 'NF', '51', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('aae584c0-9984-45ee-ae5e-a787baa67673', 'Newfoundland Pony', 'NFP', '52', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('92cf8c65-896c-4b84-98e2-3e3cf19b9d14', 'Oldenburg', 'OB', '53', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('befdac0f-e29e-4df4-a37e-40dcefbcbbe3', 'Orlov Trotter', 'ORT', '54', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('257a554f-20b8-40cf-b000-4afd8184b8e5', 'Paint', 'PAI', '55', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c969361b-8e6a-42be-908e-93af121ac0d9', 'Palomino', 'PL', '56', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('81cbb955-d7f5-4d77-bf13-25506b39d8c2', 'Paso Fino', 'PF', '57', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('ed6e8975-ee46-4cfb-992c-e1a5875c4da4', 'Percheron', 'PH', '58', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('6b38cc9b-cb5f-4d01-92ce-6813f405ca5d', 'Peruvian Paso', 'PV', '59', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('616c8e4a-8ed4-42b8-b533-c7e6a397f3f5', 'Pinto', 'PN', '60', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('96f2278a-39a4-4db1-bf01-5c6ad5c38f9d', 'Polish Warmblood', 'PW', '61', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('03131864-5537-4414-821e-94c5d07c2a2b', 'Pony', 'PO', '62', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('cfb4126e-eea4-46ca-97c5-5408f85db20b', 'Pony of the Americas', 'POA', '63', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c0f1f15c-7077-443f-abce-c0ecd7c13714', 'Quarter Horse', 'QH', '64', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('b10dc696-9c05-40ba-aa3d-f06d69c9af3c', 'Quarter Pony', 'QHP', '65', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('32024962-054b-4b7d-b668-9fbacab1a66d', 'Racking Horse', 'RAK', '66', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c5ae9005-ac8e-4b87-8446-e5fb7ea5160f', 'Rocky Mountain', 'RM', '67', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('a9caaff2-d4f1-44e8-8d42-f018783b849e', 'Shetland', 'SE', '68', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('92aaefa9-58c0-4747-b380-deaec8f477b7', 'Shire', 'SY', '69', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('c369900c-50d1-450d-bc36-bbabc263ac95', 'Spanish Baca Chica', 'SBC', '70', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e316d1b0-7732-4504-9634-f31728fbea8d', 'Spanish Choctaw', 'SCH', '71', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('dbd11ff8-d24b-42f7-9153-043d0fd66d98', 'Spanish Santa Cruz', 'SSC', '72', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('2abbac54-9b73-4e51-b391-f702937fbb00', 'Spanish Sulphur', 'SS', '73', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('cf8e6b4d-5f94-4766-bc6d-f39e8c2070ed', 'Spanish Wilbur-Cruce', 'SWC', '74', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('48e264d4-e9f1-41da-bb69-a6e0128104f8', 'Standardbred', 'SN', '75', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('1d5db65f-998e-4941-b54b-98510be28340', 'Suffolk', 'SF', '76', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('74c4e6d1-a64b-4f4e-8d63-cce836f24a8c', 'Swedish Warmblood', 'IW', '77', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('cf51298c-b6c0-40fa-9079-26a165ac11ee', 'Tennessee Walker', 'TW', '78', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('19bf7865-2501-482a-8640-115066c88a80', 'Thoroughbred', 'TH', '79', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('63bc34c0-478f-407d-8225-89b7c4d5d044', 'Trakehner', 'TR', '80', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('4ffaa921-c266-41be-85e5-3d8c127d4280', 'Welsh Pony', 'WP', '81', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('17d6edc8-749f-4060-bd93-2c231405e533', 'Black Face Cross', 'BFXB', '9', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-02-26 16:36:39', '2023-02-26 16:36:39');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('ddba1a19-f2d9-4b0a-b888-2a2cfd6de462', 'Dohne Merino', 'DM', '5', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-09-16 14:33:15', '2023-09-16 14:33:15');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('7d19f275-21f0-4321-a338-52bab5e2ac74', 'White Face Cross', 'WFXB', '8', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-09-20 11:00:00', '2023-09-20 11:00:00');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('80a6d312-fb6e-4ede-9950-8ebc7c776f6e', 'Unknown', 'UNK', '16', '3ea774ba-9103-410e-a904-d91a22b38276', '2024-03-23 13:01:54', '2024-03-23 13:01:54');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('42da8979-eed2-45c5-b15a-e25f5248857a', 'Unknown', 'UNK', '82', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2024-03-23 13:01:54', '2024-03-23 13:01:54');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('da041fc5-ddee-4fb5-a3d9-669cb55bb8f6', 'Unknown', 'UNK', '7', '67966e17-2e3d-44e3-9401-a2c6112fdbb4', '2024-03-23 13:01:54', '2024-03-23 13:01:54');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('e69d5da3-0866-45ae-9667-fb27ee3d7512', 'Unknown', 'UNK', '24', 'ce4876de-69c7-4e11-b123-c333d557eece', '2024-03-23 13:01:54', '2024-03-23 13:01:54');
INSERT INTO breed_table (id_breedid, breed_name, breed_abbrev, breed_display_order, id_speciesid, created, modified) VALUES ('87b425d5-1e6a-4824-be88-4dcc03a6b80a', 'Hair Sheep Cross', 'HSC', '243', '3ca0b500-3f96-4342-8620-bfda6e900222', '2024-12-22 09:15:24', '2024-12-22 09:15:24');

INSERT INTO cluster_calculation_type_table (id_clustercalculationtypeid, cluster_calculation_type, cluster_calculation_type_display_order, created, modified) VALUES ('5000a3e2-27be-47d4-bba3-f88feff0ec42', 'Wards Analysis', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO cluster_calculation_type_table (id_clustercalculationtypeid, cluster_calculation_type, cluster_calculation_type_display_order, created, modified) VALUES ('e756f9f5-0842-4050-9732-e58173574687', 'Pedigree Deduction', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO cluster_calculation_type_table (id_clustercalculationtypeid, cluster_calculation_type, cluster_calculation_type_display_order, created, modified) VALUES ('ff54d256-692e-4d94-8fbd-0f4d80bc3ed9', 'Founder Percentage', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO cluster_calculation_type_table (id_clustercalculationtypeid, cluster_calculation_type, cluster_calculation_type_display_order, created, modified) VALUES ('6862b231-2964-4a8b-8fdb-94795a6cec40', 'Owner Defined', '4', '2024-02-18 14:37:57', '2024-02-18 14:37:57');

INSERT INTO company_email_table (id_companyemailid, id_companyid, id_emailtypeid, company_email, created, modified) VALUES ('a1c6a059-77ea-405f-8be1-c8a2b715705e', 'e76333d0-4787-4aa0-b816-0ddbf273a634', '351dfa5c-be92-4d8f-97e2-ec0fbebfdd4b', 'geri.parsons@optimalag.com', '2024-02-25 12:39:36', '2024-02-25 12:39:36');
INSERT INTO company_email_table (id_companyemailid, id_companyid, id_emailtypeid, company_email, created, modified) VALUES ('9f467a45-79b8-4f7d-acbb-9262cc216b0a', '690885da-41bd-47bb-9daa-2671bcef08a8', '351dfa5c-be92-4d8f-97e2-ec0fbebfdd4b', 'support@animaltrakker.com', '2024-02-25 12:39:36', '2024-02-25 12:39:36');
INSERT INTO company_email_table (id_companyemailid, id_companyid, id_emailtypeid, company_email, created, modified) VALUES ('58d8d46e-5252-409f-a509-66056962b656', '665016c8-5ca8-4d32-8033-d7ba96c92871', '351dfa5c-be92-4d8f-97e2-ec0fbebfdd4b', 'dlab@colostate.edu', '2024-02-25 12:39:36', '2024-02-25 12:39:36');
INSERT INTO company_email_table (id_companyemailid, id_companyid, id_emailtypeid, company_email, created, modified) VALUES ('62b382ea-8ef3-4439-ac11-84d8487afab4', '6fe592a6-06db-45b9-aa4f-262fc0131568', '351dfa5c-be92-4d8f-97e2-ec0fbebfdd4b', 'cvmbs-vdl_rockyford@colostate.edu', '2024-02-25 12:39:36', '2024-02-25 12:39:36');
INSERT INTO company_email_table (id_companyemailid, id_companyid, id_emailtypeid, company_email, created, modified) VALUES ('d37a0977-554c-4b35-b4b2-f6c09eb737af', 'f8680404-08d0-4b5d-8454-b3f1f88916b8', '351dfa5c-be92-4d8f-97e2-ec0fbebfdd4b', 'vdl_western_slope@colostate.edu', '2024-02-25 12:39:36', '2024-02-25 12:39:36');
INSERT INTO company_email_table (id_companyemailid, id_companyid, id_emailtypeid, company_email, created, modified) VALUES ('56585b49-8117-4b74-bc2d-62d65fd5352c', '9917e913-4c44-432a-b946-bce5d9d5575f', '351dfa5c-be92-4d8f-97e2-ec0fbebfdd4b', 'genecheck@genecheck.com', '2024-02-25 12:39:36', '2024-02-25 12:39:36');
INSERT INTO company_email_table (id_companyemailid, id_companyid, id_emailtypeid, company_email, created, modified) VALUES ('bb02133a-0ad8-4890-9a34-f133f3a7ea08', 'c46a6034-0d65-490f-a7db-ba7cdb5ba22f', '351dfa5c-be92-4d8f-97e2-ec0fbebfdd4b', 'Karissa.Isaacs@SuperiorFarms.com', '2024-02-25 12:39:36', '2024-02-25 12:39:36');
INSERT INTO company_email_table (id_companyemailid, id_companyid, id_emailtypeid, company_email, created, modified) VALUES ('8e898735-cc06-48d2-b793-81cc57e48276', '77070f4b-decc-4b9d-a705-0c561c7b6c6c', '351dfa5c-be92-4d8f-97e2-ec0fbebfdd4b', 'info@nsip.org', '2024-09-30 14:08:10', '2024-09-30 14:08:10');

INSERT INTO company_laboratory_table (id_companylaboratoryid, id_companyid, laboratory_display_order, lab_license_number, created, modified) VALUES ('ed9d3404-316b-48bf-94ae-faa48b223acb', '322eb922-5a7a-44e0-9c53-159b9ea63878', '1',NULL, '2023-04-02 21:24:16', '2023-04-02 21:24:16');
INSERT INTO company_laboratory_table (id_companylaboratoryid, id_companyid, laboratory_display_order, lab_license_number, created, modified) VALUES ('f6779d1a-d3de-441e-99ae-694fc1ceaab2', '665016c8-5ca8-4d32-8033-d7ba96c92871', '2',NULL, '2023-04-02 21:24:16', '2023-04-02 21:24:16');
INSERT INTO company_laboratory_table (id_companylaboratoryid, id_companyid, laboratory_display_order, lab_license_number, created, modified) VALUES ('3cd6572e-6f95-4fb6-adc9-f5b4b1dade56', '6fe592a6-06db-45b9-aa4f-262fc0131568', '3',NULL, '2023-04-02 21:24:16', '2023-04-02 21:24:16');
INSERT INTO company_laboratory_table (id_companylaboratoryid, id_companyid, laboratory_display_order, lab_license_number, created, modified) VALUES ('b3870961-8f6a-46de-88bb-dfd1ee8bdc13', 'f8680404-08d0-4b5d-8454-b3f1f88916b8', '4',NULL, '2023-04-02 21:24:16', '2023-04-02 21:24:16');
INSERT INTO company_laboratory_table (id_companylaboratoryid, id_companyid, laboratory_display_order, lab_license_number, created, modified) VALUES ('c1b6c80a-60e1-4644-bbb9-0b0fa5a4f667', '9917e913-4c44-432a-b946-bce5d9d5575f', '6',NULL, '2023-04-02 21:24:16', '2023-04-02 21:24:16');
INSERT INTO company_laboratory_table (id_companylaboratoryid, id_companyid, laboratory_display_order, lab_license_number, created, modified) VALUES ('659adeb2-efde-4453-9bba-f3e5546b5e6e', 'c46a6034-0d65-490f-a7db-ba7cdb5ba22f', '7',NULL, '2023-04-02 21:24:16', '2023-04-02 21:24:16');
INSERT INTO company_laboratory_table (id_companylaboratoryid, id_companyid, laboratory_display_order, lab_license_number, created, modified) VALUES ('8dec1883-7af7-45e2-9788-07c99721ec5b', 'e76333d0-4787-4aa0-b816-0ddbf273a634', '5',NULL, '2023-04-02 21:24:16', '2023-04-02 21:24:16');
INSERT INTO company_laboratory_table (id_companylaboratoryid, id_companyid, laboratory_display_order, lab_license_number, created, modified) VALUES ('667ec358-697c-426e-91ed-7dcbcb01fdc9', '7d3e2fa2-c8da-4dba-910d-e0d2d29ca3e7', '8',NULL, '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO company_laboratory_table (id_companylaboratoryid, id_companyid, laboratory_display_order, lab_license_number, created, modified) VALUES ('a7543b81-fc42-4fbb-8d03-7d2f4b5ffe55', 'd2eb2274-865e-44f7-b2a0-9c85ac24664c', '9',NULL, '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO company_laboratory_table (id_companylaboratoryid, id_companyid, laboratory_display_order, lab_license_number, created, modified) VALUES ('30bfd0ff-6797-4de3-b9e5-5ee7fdc460b3', '54cd4831-14b4-4cdd-81a5-c816a6da1503', '10',NULL, '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO company_laboratory_table (id_companylaboratoryid, id_companyid, laboratory_display_order, lab_license_number, created, modified) VALUES ('14d79f9b-d5d7-44f4-90b5-31a8047d1a61', '169d93f1-7725-4904-be97-fcc4b417c1bd', '11',NULL, '2024-09-30 14:08:10', '2024-09-30 14:08:10');
INSERT INTO company_laboratory_table (id_companylaboratoryid, id_companyid, laboratory_display_order, lab_license_number, created, modified) VALUES ('ab7aead8-2093-404e-96f8-aca52177c3f1', '77070f4b-decc-4b9d-a705-0c561c7b6c6c', '12',NULL, '2024-09-30 14:08:10', '2024-09-30 14:08:10');

INSERT INTO company_phone_table (id_companyphoneid, id_companyid, id_phonetypeid, company_phone, created, modified) VALUES ('65b8f724-cf8a-4e41-9d8b-b7270daec4eb', '690885da-41bd-47bb-9daa-2671bcef08a8', '691ecec8-75dc-426a-af38-8666efe22409', '970-527-3573', '2023-04-14 21:02:27', '2023-04-14 21:02:27');
INSERT INTO company_phone_table (id_companyphoneid, id_companyid, id_phonetypeid, company_phone, created, modified) VALUES ('a37d9acf-d5c8-4444-9b28-29b521a8872b', '7d3e2fa2-c8da-4dba-910d-e0d2d29ca3e7', '691ecec8-75dc-426a-af38-8666efe22409', '515-337-7514', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO company_phone_table (id_companyphoneid, id_companyid, id_phonetypeid, company_phone, created, modified) VALUES ('bc4ebeaa-e472-418c-8fd9-0b24daf4a20f', 'd2eb2274-865e-44f7-b2a0-9c85ac24664c', '691ecec8-75dc-426a-af38-8666efe22409', '515-294-2038', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO company_phone_table (id_companyphoneid, id_companyid, id_phonetypeid, company_phone, created, modified) VALUES ('a4d6790d-cb2c-40e4-bd86-f218143b1fc6', '54cd4831-14b4-4cdd-81a5-c816a6da1503', '691ecec8-75dc-426a-af38-8666efe22409', '706-224-9445', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO company_phone_table (id_companyphoneid, id_companyid, id_phonetypeid, company_phone, created, modified) VALUES ('0298da36-2a79-4bca-a293-ff4b898af7d7', '77070f4b-decc-4b9d-a705-0c561c7b6c6c', '691ecec8-75dc-426a-af38-8666efe22409', '734-301-6931', '2024-09-30 14:08:10', '2024-09-30 14:08:10');

INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('c59d5594-769b-42cf-ad32-7c6768632d37', 'e76333d0-4787-4aa0-b816-0ddbf273a634', 'ea386787-b020-4b62-bb82-d5606d953834', '2000-01-01', '2023-12-21', '2023-02-25 14:29:36', '2023-02-25 14:29:36');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('88082956-c985-4437-a63f-7e833f1a9c17', '322eb922-5a7a-44e0-9c53-159b9ea63878', '7f69ed4d-dd08-42db-b9e2-5bfd904ac4ab', '2000-01-01',NULL, '2024-02-22 13:21:35', '2024-02-22 13:21:35');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('832f34ca-671b-4d6a-afe9-62a94aeccf07', '665016c8-5ca8-4d32-8033-d7ba96c92871', '06a2f695-5c34-41d3-b20c-49386749119b', '2000-01-01',NULL, '2024-02-22 13:21:35', '2024-02-22 13:21:35');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('d1c0c334-5b43-4911-86d6-e9bf4efbf0b8', '6fe592a6-06db-45b9-aa4f-262fc0131568', '1784dc32-b1c7-47a4-a3cf-15f41bd5cb13', '2000-01-01',NULL, '2024-02-22 13:21:35', '2024-02-22 13:21:35');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('a2337cde-d48a-4f36-a2d0-867f7f1184d4', 'f8680404-08d0-4b5d-8454-b3f1f88916b8', '2e2c2107-87ca-4287-9c97-a38a53eb7a78', '2000-01-01',NULL, '2024-02-22 13:21:35', '2024-02-22 13:21:35');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('d26ca84a-315a-46df-89dc-4d7c2502b57b', '9917e913-4c44-432a-b946-bce5d9d5575f', 'e50d7be2-79af-41ea-b88a-17ced6a87087', '2000-01-01',NULL, '2024-02-22 13:21:35', '2024-02-22 13:21:35');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('146a3461-666a-4616-b42b-f4558e234eda', 'c46a6034-0d65-490f-a7db-ba7cdb5ba22f', '6cffd0ba-642c-47d0-8d55-2dbc2a2fcc67', '2000-01-01',NULL, '2024-02-22 13:21:35', '2024-02-22 13:21:35');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('059b78ce-40db-4147-bd84-28c33117e07e', '690885da-41bd-47bb-9daa-2671bcef08a8', '5212bfa2-2a75-47c7-9a04-975dc5d0cec2', '2000-10-19',NULL, '2024-02-22 13:21:35', '2024-02-22 13:21:35');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('51341943-a880-456e-9177-8269595909a0', '7d3e2fa2-c8da-4dba-910d-e0d2d29ca3e7', 'f85f9438-02c5-4845-9c7a-37f2d53c0e75', '2000-01-01',NULL, '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('3bdf6ab1-70e4-42ca-b195-249bcbff7d72', 'd2eb2274-865e-44f7-b2a0-9c85ac24664c', '80a5f0ac-de36-4eae-8b6c-a5bbb3cf01c2', '2000-01-01',NULL, '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('83eeb47f-0e78-439e-a7aa-f584c82793ce', '54cd4831-14b4-4cdd-81a5-c816a6da1503', '1687e6c7-83bd-4465-bf1b-79019f709807', '2000-01-01',NULL, '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('2301e4e8-82bb-46d2-8b96-8a019200ab11', '77070f4b-decc-4b9d-a705-0c561c7b6c6c', 'f55e0fc8-26af-45db-8078-92d0256e846c', '2024-08-01',NULL, '2024-09-30 14:08:10', '2024-09-30 14:08:10');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('f4bef4ae-2a2b-4d68-9d02-314264be9ef1', 'e76333d0-4787-4aa0-b816-0ddbf273a634', 'a03d6eb9-8dc0-4de6-81ea-f12593d1f1a0', '2023-12-21',NULL, '2024-12-15 08:25:24', '2024-12-15 08:25:24');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('b48893f3-a86a-4a8a-b66a-f6d14b9aa6e1', '169d93f1-7725-4904-be97-fcc4b417c1bd', '06a2f695-5c34-41d3-b20c-49386749119b', '1999-10-01',NULL, '2023-02-25 14:29:36', '2023-02-25 14:29:36');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('be22758b-ee80-4954-967c-32db2bb64fa7', '3a7e2399-17fd-4a8f-af43-d66fde9e0539', '8f6adf49-cf66-4cb5-affd-135c4dd67189', '1998-01-01', '2000-10-18', '2023-02-25 14:29:36', '2023-02-25 14:29:36');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('c233e98b-bf77-4a20-979d-279ac56e71c8', 'dc9ffa44-049c-4b34-8430-61a442bbe025', '8f6adf49-cf66-4cb5-affd-135c4dd67189', '1998-01-01', '2000-10-18', '2023-02-25 14:29:36', '2023-02-25 14:29:36');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('b9dee056-c700-4917-abb3-b678d31c3785', 'd2122a99-b1c1-419f-8e18-28e1112dc7a8', '8f6adf49-cf66-4cb5-affd-135c4dd67189', '1998-01-01', '2000-10-18', '2023-02-25 14:29:36', '2023-02-25 14:29:36');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('a28e4661-04cf-4616-b678-7d9d29e65ae0', '3a7e2399-17fd-4a8f-af43-d66fde9e0539', 'eb932824-53a9-4d07-b24d-3900b66cde70', '2000-10-19',NULL, '2023-02-25 14:29:36', '2023-02-25 14:29:36');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('612fdeda-6477-48f0-8d41-f52edafa4b50', 'dc9ffa44-049c-4b34-8430-61a442bbe025', 'eb932824-53a9-4d07-b24d-3900b66cde70', '2000-10-19',NULL, '2023-02-25 14:29:36', '2023-02-25 14:29:36');
INSERT INTO company_premise_table (id_companypremiseid, id_companyid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('e401cef4-818d-418d-a250-e8c3ea8376c3', 'd2122a99-b1c1-419f-8e18-28e1112dc7a8', 'eb932824-53a9-4d07-b24d-3900b66cde70', '2000-10-19',NULL, '2023-02-25 14:29:36', '2023-02-25 14:29:36');

INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('e76333d0-4787-4aa0-b816-0ddbf273a634','Optimal Livestock','2023-02-24 21:42:29','2023-02-24 21:42:29');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('ed382247-5cef-48ea-b7fe-b09491dc9ad6','Generic','2023-05-13 16:52:03','2023-05-13 16:52:03');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('690885da-41bd-47bb-9daa-2671bcef08a8','Weyr Associates LLC','2024-02-20 10:53:12','2024-02-20 10:53:12');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('322eb922-5a7a-44e0-9c53-159b9ea63878','CO Animal Health Laboratory','2023-04-02 21:24:16','2023-04-02 21:24:16');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('665016c8-5ca8-4d32-8033-d7ba96c92871','CSU Fort Collins Laboratory','2023-04-02 21:24:16','2023-04-02 21:24:16');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('6fe592a6-06db-45b9-aa4f-262fc0131568','Rocky Ford Vet Diag. Lab','2023-04-02 21:24:16','2023-04-02 21:24:16');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('f8680404-08d0-4b5d-8454-b3f1f88916b8','CSU Western Slope Vet.Diag. Lab','2023-04-02 21:24:16','2023-04-02 21:24:16');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('9917e913-4c44-432a-b946-bce5d9d5575f','Gene Check Inc.','2023-04-02 21:24:16','2023-04-02 21:24:16');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('c46a6034-0d65-490f-a7db-ba7cdb5ba22f','Flock 54 Superior Farms','2023-04-02 21:24:16','2023-04-02 21:24:16');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('7d3e2fa2-c8da-4dba-910d-e0d2d29ca3e7','National Veterinary Services Laboratories-Ames','2024-07-22 07:51:18','2024-07-22 07:51:18');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('d2eb2274-865e-44f7-b2a0-9c85ac24664c','Iowa State University','2024-07-22 07:51:18','2024-07-22 07:51:18');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('54cd4831-14b4-4cdd-81a5-c816a6da1503','LSU School of Veterinary Medicine','2024-07-22 07:51:18','2024-07-22 07:51:18');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('77070f4b-decc-4b9d-a705-0c561c7b6c6c','National Sheep Improvement Program','2024-09-30 14:08:10','2024-09-30 14:08:10');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('169d93f1-7725-4904-be97-fcc4b417c1bd','NAGP','2023-02-24 21:42:29','2023-02-24 21:42:29');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('408318f9-0134-4f5e-b30a-e00120e661d0','Desert Weyr, LLC','2023-02-24 21:42:29','2023-02-24 21:42:29');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('3a7e2399-17fd-4a8f-af43-d66fde9e0539','American Black Welsh Mountain Sheep Association','2023-02-24 21:42:29','2023-02-24 21:42:29');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('dc9ffa44-049c-4b34-8430-61a442bbe025','American Chocolate Welsh Mountain Sheep Association','2023-02-24 21:42:29','2023-02-24 21:42:29');
INSERT INTO company_table (id_companyid,company,created,modified) VALUES ('d2122a99-b1c1-419f-8e18-28e1112dc7a8','American White Welsh Mountain Sheep Association','2023-02-24 21:42:29','2023-02-24 21:42:29');

INSERT INTO company_website_table (id_companywebsiteid, id_companyid, company_website, created, modified) VALUES ('95bd7784-0772-474b-b611-84a14deae6f2', 'e76333d0-4787-4aa0-b816-0ddbf273a634', 'optimallivestock.com', '2023-04-14 14:52:45', '2023-04-14 14:52:45');
INSERT INTO company_website_table (id_companywebsiteid, id_companyid, company_website, created, modified) VALUES ('88896973-6b62-4da5-8a59-388a0fcdfe1f', '690885da-41bd-47bb-9daa-2671bcef08a8', 'www.AnimalTrakker.com', '2023-04-14 14:52:45', '2023-04-14 14:52:45');
INSERT INTO company_website_table (id_companywebsiteid, id_companyid, company_website, created, modified) VALUES ('7fbd39bd-fef5-49b7-b38e-6361e3e9573a', '9917e913-4c44-432a-b946-bce5d9d5575f', 'www.GeneCheck.com', '2023-04-14 14:52:45', '2023-04-14 14:52:45');
INSERT INTO company_website_table (id_companywebsiteid, id_companyid, company_website, created, modified) VALUES ('44360650-4c11-489b-8601-d384f4f0d43d', 'c46a6034-0d65-490f-a7db-ba7cdb5ba22f', 'www.flock54.com', '2023-04-14 14:52:45', '2023-04-14 14:52:45');

INSERT INTO contact_company_table (id_contactcompanyid, id_contactid, id_companyid, created, modified) VALUES ('01e03f9d-5ced-475e-bb2f-f119050dcf89', 'cf384ce6-ff40-4e83-b898-67f9159717f3', '408318f9-0134-4f5e-b30a-e00120e661d0', '2023-02-24 14:42:28', '2023-02-24 14:42:28');
INSERT INTO contact_company_table (id_contactcompanyid, id_contactid, id_companyid, created, modified) VALUES ('116cab32-78b1-4564-a14f-a5d177e1a8a6', '0b597046-751d-4429-aefb-370dff913425', 'e76333d0-4787-4aa0-b816-0ddbf273a634', '2023-02-24 14:42:28', '2023-02-24 14:42:28');
INSERT INTO contact_company_table (id_contactcompanyid, id_contactid, id_companyid, created, modified) VALUES ('13fe4ddd-7ab7-4422-bfa5-219b632ded5f', 'cf384ce6-ff40-4e83-b898-67f9159717f3', '690885da-41bd-47bb-9daa-2671bcef08a8', '2023-02-24 14:42:28', '2023-02-24 14:42:28');
INSERT INTO contact_company_table (id_contactcompanyid, id_contactid, id_companyid, created, modified) VALUES ('1f37fdf3-fcac-41f8-ac08-bca9ab708de4', '515f2074-45aa-4e95-aa7b-c785fb6b290c', '322eb922-5a7a-44e0-9c53-159b9ea63878', '2024-06-07 14:40:09', '2024-06-07 14:40:09');
INSERT INTO contact_company_table (id_contactcompanyid, id_contactid, id_companyid, created, modified) VALUES ('224b07d8-3aa8-4117-980c-3cd74b329e86', '5044b591-4931-4522-bc92-bb4f7d19f629', 'c46a6034-0d65-490f-a7db-ba7cdb5ba22f', '2024-06-07 14:40:09', '2024-06-07 14:40:09');
INSERT INTO contact_company_table (id_contactcompanyid, id_contactid, id_companyid, created, modified) VALUES ('832644f2-b31b-4a62-871a-218324677350', 'cb84e893-1c3a-4198-94e5-abede5df9d87', 'd2eb2274-865e-44f7-b2a0-9c85ac24664c', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO contact_company_table (id_contactcompanyid, id_contactid, id_companyid, created, modified) VALUES ('f3f5990e-96c3-40ef-9796-8d0409935cf6', '9a70430e-d65a-4a82-b5d1-4ac85a201995', '54cd4831-14b4-4cdd-81a5-c816a6da1503', '2024-07-22 07:51:18', '2024-07-22 07:51:18');

INSERT INTO contact_email_table (id_contactemailid, id_contactid, id_emailtypeid, contact_email, created, modified) VALUES ('9d416241-06a1-40a5-ad79-b427d40decab', '0b597046-751d-4429-aefb-370dff913425', 'af3408ba-d8dd-4189-9091-21c17f374ea1', 'geri.parsons@optimalag.com', '2024-02-25 12:39:36', '2024-02-25 12:39:36');
INSERT INTO contact_email_table (id_contactemailid, id_contactid, id_emailtypeid, contact_email, created, modified) VALUES ('65a0462c-760a-4875-863a-91bfb12b82fa', '914b5b57-bfdc-4cff-b15e-c7f9cf57fddf', '351dfa5c-be92-4d8f-97e2-ec0fbebfdd4b', 'support@animaltrakker.com', '2024-02-25 12:39:36', '2024-02-25 12:39:36');
INSERT INTO contact_email_table (id_contactemailid, id_contactid, id_emailtypeid, contact_email, created, modified) VALUES ('be8dc247-9eb3-4ec0-8b14-4928c189aa27', '5044b591-4931-4522-bc92-bb4f7d19f629', 'af3408ba-d8dd-4189-9091-21c17f374ea1', 'karissa.isaacs@superiorfarms.com', '2024-02-25 12:39:36', '2024-02-25 12:39:36');

INSERT INTO contact_premise_table (id_contactpremiseid, id_contactid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('875bd006-2899-40fa-b178-fc41362813d3', 'cf384ce6-ff40-4e83-b898-67f9159717f3', '5212bfa2-2a75-47c7-9a04-975dc5d0cec2', '2000-10-19',NULL, '2024-02-22 13:21:35', '2024-02-22 13:21:35');
INSERT INTO contact_premise_table (id_contactpremiseid, id_contactid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('75aeb476-35ed-42bb-8285-c60a2411106f', '0b597046-751d-4429-aefb-370dff913425', 'ea386787-b020-4b62-bb82-d5606d953834', '2020-01-01', '2023-12-21', '2023-02-26 10:58:51', '2023-06-17 13:40:59');
INSERT INTO contact_premise_table (id_contactpremiseid, id_contactid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('64e81927-cf92-4715-8b1c-e1346d1379c4', '515f2074-45aa-4e95-aa7b-c785fb6b290c', '7f69ed4d-dd08-42db-b9e2-5bfd904ac4ab', '2020-01-01',NULL, '2023-02-26 10:58:51', '2023-02-26 10:58:51');
INSERT INTO contact_premise_table (id_contactpremiseid, id_contactid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('704785bb-cfe6-43bd-8317-9a031df8c862', '5044b591-4931-4522-bc92-bb4f7d19f629', '6cffd0ba-642c-47d0-8d55-2dbc2a2fcc67', '2020-01-01',NULL, '2023-02-26 10:58:51', '2023-02-26 10:58:51');
INSERT INTO contact_premise_table (id_contactpremiseid, id_contactid, id_premiseid, start_premise_use, end_premise_use, created, modified) VALUES ('a5bf1580-0f19-4026-9409-1a8dfd78fcaf', '0b597046-751d-4429-aefb-370dff913425', 'a03d6eb9-8dc0-4de6-81ea-f12593d1f1a0', '2023-12-21',NULL, '2024-12-15 08:25:24', '2024-12-15 08:25:24');

INSERT INTO contact_table (id_contactid, contact_last_name, contact_first_name, contact_middle_name, id_contacttitleid, created, modified) VALUES ('cf384ce6-ff40-4e83-b898-67f9159717f3', 'McGuire', 'Kenyon and Eugenie',NULL,NULL, '2023-02-24 21:47:50', '2023-02-24 21:47:50');
INSERT INTO contact_table (id_contactid, contact_last_name, contact_first_name, contact_middle_name, id_contacttitleid, created, modified) VALUES ('7b8bfd81-3021-401a-a174-9ec109a36e2e', 'Unknown','Unknown',NULL,NULL, '2023-02-24 21:47:50', '2023-02-24 21:47:50');
INSERT INTO contact_table (id_contactid, contact_last_name, contact_first_name, contact_middle_name, id_contacttitleid, created, modified) VALUES ('0b597046-751d-4429-aefb-370dff913425', 'Parsons', 'Geri',NULL,NULL, '2023-02-24 21:47:50', '2023-02-24 21:47:50');
INSERT INTO contact_table (id_contactid, contact_last_name, contact_first_name, contact_middle_name, id_contacttitleid, created, modified) VALUES ('515f2074-45aa-4e95-aa7b-c785fb6b290c', 'Brigner', 'Tiffany',NULL,NULL, '2023-04-02 21:30:27', '2023-04-02 21:30:27');
INSERT INTO contact_table (id_contactid, contact_last_name, contact_first_name, contact_middle_name, id_contacttitleid, created, modified) VALUES ('5044b591-4931-4522-bc92-bb4f7d19f629', 'Isaacs', 'Karissa',NULL,NULL, '2023-04-02 21:30:27', '2023-04-02 21:30:27');
INSERT INTO contact_table (id_contactid, contact_last_name, contact_first_name, contact_middle_name, id_contacttitleid, created, modified) VALUES ('cb84e893-1c3a-4198-94e5-abede5df9d87', 'Zhang', 'Qijing',NULL, '40663690-8363-4aa8-a8b1-fdfebbe4d587', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO contact_table (id_contactid, contact_last_name, contact_first_name, contact_middle_name, id_contacttitleid, created, modified) VALUES ('9a70430e-d65a-4a82-b5d1-4ac85a201995', 'Vatta', 'Adriano',NULL, '40663690-8363-4aa8-a8b1-fdfebbe4d587', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO contact_table (id_contactid, contact_last_name, contact_first_name, contact_middle_name, id_contacttitleid, created, modified) VALUES ('914b5b57-bfdc-4cff-b15e-c7f9cf57fddf', 'McGuire', 'Oogie',NULL,NULL, '2023-09-20 10:47:00', '2023-09-20 10:47:00');

INSERT INTO contact_title_table (id_contacttitleid, contact_title, contact_title_display_order, created, modified) VALUES ('5e3339f0-9839-4005-8fe0-6478faaf869a', 'Mr.', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO contact_title_table (id_contacttitleid, contact_title, contact_title_display_order, created, modified) VALUES ('e09a2cf2-748b-4e86-a9b0-e122449ce13f', 'Ms.', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO contact_title_table (id_contacttitleid, contact_title, contact_title_display_order, created, modified) VALUES ('3780165d-6dbb-44d2-9019-b613bca999cb', 'Mrs.', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO contact_title_table (id_contacttitleid, contact_title, contact_title_display_order, created, modified) VALUES ('bbba88f2-056f-4bd2-a223-da2efb4a4029', 'Mr. and Mrs.', '4', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO contact_title_table (id_contacttitleid, contact_title, contact_title_display_order, created, modified) VALUES ('40663690-8363-4aa8-a8b1-fdfebbe4d587', 'Dr.', '5', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO contact_title_table (id_contacttitleid, contact_title, contact_title_display_order, created, modified) VALUES ('6365fa40-f5b4-4395-98a3-89d1dec34481', 'Unknown', '6', '2024-02-18 14:37:57', '2024-02-18 14:37:57');

INSERT INTO contact_veterinarian_license_type_table (id_contactveterinarianlicensetypeid, vet_license_type, created, modified) VALUES ('9eb696a0-cf93-4d2f-87ad-93ba64ae415b', 'USDA LEVEL 1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO contact_veterinarian_license_type_table (id_contactveterinarianlicensetypeid, vet_license_type, created, modified) VALUES ('f5cbeadd-528f-45af-a2f9-b1a22fa4a142', 'USDA LEVEL 2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO contact_veterinarian_license_type_table (id_contactveterinarianlicensetypeid, vet_license_type, created, modified) VALUES ('7a312571-c383-44b4-a811-053c2281b17a', 'STATE', '2024-02-18 14:37:57', '2024-02-18 14:37:57');

INSERT INTO country_table (id_countryid, country_name, country_abbrev, country_eid_prefix, country_name_display_order, created, modified) VALUES ('b41f2392-2517-459d-b074-464d6915fafa', 'United States', 'US', '840', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO country_table (id_countryid, country_name, country_abbrev, country_eid_prefix, country_name_display_order, created, modified) VALUES ('07720e3a-e722-4707-a159-3c39aaf62b5b', 'Canada', 'CAN', '124', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO country_table (id_countryid, country_name, country_abbrev, country_eid_prefix, country_name_display_order, created, modified) VALUES ('3cbe355a-b195-4ec2-aad6-e5ec3b5a12a7', 'United Kingdom', 'UK', '826', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO country_table (id_countryid, country_name, country_abbrev, country_eid_prefix, country_name_display_order, created, modified) VALUES ('e262d3eb-ca54-4204-a817-1fccbc1c70ec', 'Mexico', 'MEX', '484', '4', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO country_table (id_countryid, country_name, country_abbrev, country_eid_prefix, country_name_display_order, created, modified) VALUES ('a25f7635-694c-4430-9d2e-5fdeb113bd82', 'Australia', 'AUS', '036', '5', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO country_table (id_countryid, country_name, country_abbrev, country_eid_prefix, country_name_display_order, created, modified) VALUES ('288da6f7-292c-4f2d-8863-784c02878379', 'New Zealand', 'NZL', '554', '6', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO country_table (id_countryid, country_name, country_abbrev, country_eid_prefix, country_name_display_order, created, modified) VALUES ('6fad67f9-26ff-40cf-9f22-f3dd7fe8525c', 'Switzerland', 'CH', '756', '7', '2024-05-20 05:15:57', '2024-05-20 05:15:57');

INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('224ebd68-b3bf-4cd0-a5ac-8f2f5052ab2f', 'Adams', 'AD', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '1', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('16338ad2-434c-48b3-905c-774b00f4466c', 'Alamosa', 'AL', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('b3ea6e79-c23c-4bad-9e72-0dfbd9094f82', 'Arapahoe', 'AR', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '3', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('6eed4174-1b22-4473-af14-072c472aa18e', 'Archuleta', 'AU', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('c98abb4f-ea5d-4ebf-8035-4d124abdb4c8', 'Baca', 'BC', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '5', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('3f85898b-a96a-4433-b7de-d729bc12ebb8', 'Bent', 'BN', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '6', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('3bae9fcf-1f45-47ad-95ea-bb4ec414261c', 'Boulder', 'BL', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '7', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('2c0ccac2-ef8e-418b-86ce-e366e4e8fee6', 'Broomfield', 'BR', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '8', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('5ee1aa5d-e06d-4656-a296-94a87fc1963e', 'Chaffee', 'CF', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '9', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('7203a87c-2f17-4cec-967b-f74e9473b962', 'Cheyenne', 'CY', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '10', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('1a977a02-8845-4ad4-bda5-6d79272aeaa6', 'Clear Creek', 'CC', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '11', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('178f7bae-72cf-4387-a5ae-2d6c253b9713', 'Conejos', 'CN', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '12', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('e3966e0b-a51e-4ec4-bd00-16c71434f3da', 'Costilla', 'CS', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '13', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('d4eebb63-b944-425b-b988-6b23d3c1042c', 'Crowley', 'CR', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '14', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('e2edb56a-3601-43d0-adaa-9153ee94ab5c', 'Custer', 'CT', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '15', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('d27cd52c-d066-4338-86d1-3252b6a2c132', 'Delta', 'DL', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '16', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('8feed707-5b1f-4efe-848a-c95cd03124a6', 'Denver', 'DN', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '17', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('31d51af0-9a21-4056-91b0-1cdbb35962be', 'Dolores', 'DO', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '18', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('454624f5-d8d0-490f-9c11-52f9d26a56f6', 'Douglas', 'DG', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '19', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('9f030be6-01f4-48ca-908f-e17d3cd3c08f', 'Eagle', 'EG', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '20', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('fcace27a-fc8e-4c5a-bb53-5c3af6b1795c', 'El Paso', 'EP', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '21', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('5a9d3176-1047-4cf6-8666-443c0dcfa35f', 'Elbert', 'EL', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '22', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('c99b3e30-bb29-4dc8-b2a7-de2b0cc10ccb', 'Fremont', 'FR', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '23', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('b783e180-38ad-4ba8-9879-9420e9bfbd20', 'Garfield', 'GF', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '24', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('3c57432b-9985-404c-a9c0-239eadbf21e1', 'Gilpin', 'GP', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '25', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('d957e3c5-4ca9-47ee-903e-ca8b78d97a0c', 'Grand', 'GR', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '26', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('cb1f8d2f-701d-4c2e-b649-41bd892664da', 'Gunnison', 'GU', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '27', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('f29e668f-8154-4ae8-8e10-5235d080b951', 'Hinsdale', 'HN', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '28', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('67de8bc0-ea8a-4af5-bf13-44c93198fdfe', 'Huerfano', 'HF', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '29', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('9fd05372-a4be-401f-910a-84abc7fe7e7d', 'Jackson', 'JL', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '30', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('8056580e-f3e3-4206-82e0-16ccee5388af', 'Jefferson', 'JF', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '31', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('1e7c82a9-1f87-4d7c-b148-f33e2af10eb2', 'Kiowa', 'KI', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '32', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('fa00421f-9e7e-4f80-a049-9464ff8337a1', 'Kit Carson', 'KC', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '33', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('3548d6d2-d6d5-43ab-8e47-75a77d25c4bb', 'La Plata', 'LP', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '40', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('f8b13552-7b1c-47fc-b078-530b51efbee5', 'Lake', 'LK', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '35', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('82a984fa-855d-4864-a77e-3c87449d5653', 'Larimer', 'LR', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '36', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('992fee7b-1802-43f1-abaa-f99c03585b0b', 'Las Animas', 'LA', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '37', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('44741d01-f2b0-49ed-8881-7f10709c4362', 'Lincoln', 'LN', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '38', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('649acbda-4430-4803-8e7d-4874237ea8c8', 'Logan', 'LO', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '39', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('f8d1aab1-628e-4f48-a938-6b172da88dca', 'Mesa', 'ME', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '40', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('b7b481dc-68c7-4b62-8801-26e7f221ed16', 'Mineral', 'MN', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '41', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('09e96715-9a2d-4e5c-9088-6e884fa4923a', 'Moffat', 'MF', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '42', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('9c4ce0b2-0f15-4260-adb5-01d0a808ee80', 'Montezuma', 'MZ', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '43', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('e678ddf8-5062-4c8c-8f64-1c313e6a54e6', 'Montrose', 'MO', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '44', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('0bc1d66d-a851-4bb8-9e07-19dc253bbfbe', 'Morgan', 'MR', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '45', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('0f6731c9-39a1-4eb4-aefd-c3c733fd40da', 'Otero', 'OT', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '46', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('1dc19f3c-5071-42d8-91eb-e5c405de5ac2', 'Ouray', 'OU', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '47', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('2d720f8a-2e99-408f-8a42-4fcb5f2d0d0f', 'Park', 'PK', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '48', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('6f5821e7-13ae-4eb8-8525-e35b16ee5b97', 'Phillips', 'PH', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '49', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('aca12972-00cb-407c-823d-92128ac6742a', 'Pitkin', 'PT', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '50', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('cf43897e-5594-4b6b-ae80-f51bfb73cc0c', 'Prowers', 'PR', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '51', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('ff407a6b-9b2b-4eba-b72b-258af78b7308', 'Pueblo', 'PB', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '52', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('992eb214-1f3c-4ef5-a90d-5c9c7880d185', 'Rio Blanco', 'RB', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '53', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('668ebba7-ecd7-46db-af0e-4dc4754f97de', 'Rio Grande', 'RG', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '54', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('420e818f-f395-4f3d-b03f-22f740000205', 'Routt', 'RT', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '55', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('08f9db8b-2282-4713-8967-fa7bd2fdb7d4', 'Saguache', 'SA', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '56', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('0fca6681-7b68-475f-bd22-32a5208a9abe', 'San Juan', 'SJ', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '57', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('9645275b-428f-4cc5-8cc9-fc566efaacf8', 'San Miguel', 'SM', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '58', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('7492eaf5-a702-456f-8d72-0fd910affdf1', 'Sedgwick', 'SE', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '59', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('59337ac3-9e1b-480b-adc1-9e1da1330ffd', 'Summit', 'ST', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '60', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('fa1d5efe-22fc-4ab2-b544-75136fd1457c', 'Teller', 'TL', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '61', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('f003f324-a17d-4780-b4b3-475187d29302', 'Washington', 'WA', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '62', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('89c517ef-f0b8-44e4-bde5-50d66be90b1b', 'Weld', 'WE', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '63', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO county_table (id_countyid, county_name, county_abbrev, id_stateid, county_name_display_order, created, modified) VALUES ('3513393d-fb5e-44f3-b6fc-9170036c3629', 'Yuma', 'YU', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '64', '2023-09-17 10:26:51', '2023-09-17 10:26:51');

INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('8a67915a-7f98-4809-ae3c-419dde1dffe7', 'f8e01215-e228-45d7-ac1a-cd7b1ff8419f', 'Engorged Vulva', '2', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('7525268b-3847-4cb0-8328-bcc85f3346de', 'f8e01215-e228-45d7-ac1a-cd7b1ff8419f', 'Mucus Present', '3', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('98830d78-d1f1-451d-83e3-2c05a45add9e', 'f8e01215-e228-45d7-ac1a-cd7b1ff8419f', 'Both Engorged Vulva & Mucus', '4', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('53ce5393-f389-47ec-808c-832837f72f19', 'f8e01215-e228-45d7-ac1a-cd7b1ff8419f', 'None', '1', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('62da608e-47fc-43e3-a40d-cd11b8fa50bd', '72a721eb-9738-4023-b910-93af6e33386d', 'Pregnant', '1', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('684d3420-d62d-4529-a738-7a9d70270e54', '72a721eb-9738-4023-b910-93af6e33386d', 'Unsure', '2', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('5749d153-d1cf-41d3-9696-2bfa984d6e9a', '72a721eb-9738-4023-b910-93af6e33386d', 'Not Pregnant', '3', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('0335479a-56d1-4425-b9fa-b2f569b099ed', '72a721eb-9738-4023-b910-93af6e33386d', 'Not Scanned', '4', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('fa4b9e07-68a1-449f-bc19-a69a7a3bd30d', '5e372585-ce1d-4bb6-893a-c765b915b882', 'Unassisted', '1', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('49b61002-6d12-4a88-ab1d-5f77fb52a8c8', '5e372585-ce1d-4bb6-893a-c765b915b882', 'Easy Pull', '2', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('5549d370-19ea-4371-a1ca-e4555a0a2995', '5e372585-ce1d-4bb6-893a-c765b915b882', 'Hard Pull', '3', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('f3415ee7-2acc-4c98-86d6-837395cd601b', '5e372585-ce1d-4bb6-893a-c765b915b882', 'Malpresentation', '4', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('8d6f488a-b8b4-4e30-bb70-38d09e2fe825', '5e372585-ce1d-4bb6-893a-c765b915b882', 'Veterinary Assistance', '5', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('bd51ab7b-98e6-4116-b7e7-229f925531cc', 'bafda80d-d004-400b-9fca-f97d609fd62c', 'No Udder', '1', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('0f657f75-210e-4e15-a98e-78dc23f9db8d', 'bafda80d-d004-400b-9fca-f97d609fd62c', 'Small Udder', '2', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('555c75b1-660d-4a6d-a00f-2ac12130e322', 'bafda80d-d004-400b-9fca-f97d609fd62c', 'Medium Udder', '3', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('42d87e9d-be41-4296-bd2f-fb7e36030f2b', 'bafda80d-d004-400b-9fca-f97d609fd62c', 'Large Udder', '4', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('ff497d06-c288-4978-b75b-5862451796c5', 'c00505e5-9d8b-48d6-b6fd-84cdb3c65127', 'Not Tested', '1', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('ccd30208-c613-4fea-80ee-37f801e1a0f4', 'c00505e5-9d8b-48d6-b6fd-84cdb3c65127', 'No Suck', '2', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('299a716c-4597-4dff-8a88-7980e7ce36d0', 'c00505e5-9d8b-48d6-b6fd-84cdb3c65127', 'Poor Suck', '3', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('7080f1e1-8ab4-44fd-9134-d9226708d66b', 'c00505e5-9d8b-48d6-b6fd-84cdb3c65127', 'Great Suck', '5', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('5363286e-ea26-4c31-89d0-5543ebaec427', 'c00505e5-9d8b-48d6-b6fd-84cdb3c65127', 'Good Suck', '4', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('40989080-2601-4c3d-b782-98d330cf5510', 'd8127b75-6dfe-49c5-9cef-77d79d44e07d', 'Satisfactory', '1', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('4a489a4f-7586-4105-a243-35fca93f2535', '08505284-9304-4f14-b3c5-6811d3fef54c', 'Unsatisfactory', '2', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('e3339ffe-3448-417c-9576-bb1f36551d99', 'd8127b75-6dfe-49c5-9cef-77d79d44e07d', 'Recheck', '3', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('e0115229-d0ef-4f54-aeac-de27efba68dd', 'a38a8c05-9c4c-46a5-8bb5-c3398b8b8c94', 'Black', '1', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('57bdb256-e116-49a1-83dd-fbf1de4ee674', 'a38a8c05-9c4c-46a5-8bb5-c3398b8b8c94', 'White', '3', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('2b27068e-d216-4d12-a602-ae7f18b0d1ef', 'a38a8c05-9c4c-46a5-8bb5-c3398b8b8c94', 'Badger Face', '4', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('fb747a85-224b-4636-b930-516accbe24be', 'a38a8c05-9c4c-46a5-8bb5-c3398b8b8c94', 'Chocolate', '2', '2023-04-14 21:22:42', '2023-04-14 21:22:42');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('64b79ac7-6eb0-4332-8246-c7e7ea9003df', '2d79d6c3-a096-40db-ae4a-2dfa18d05482', 'Excellent', '1', '2023-08-26 08:50:50', '2023-08-26 08:50:50');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('06200319-4807-49fa-a6c9-4b48305cc059', '2d79d6c3-a096-40db-ae4a-2dfa18d05482', 'Satisfactory', '2', '2023-08-26 08:50:50', '2023-08-26 08:50:50');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('2a6647e6-b29e-4240-8363-58fd008fbd6b', '2d79d6c3-a096-40db-ae4a-2dfa18d05482', 'Questionable', '3', '2023-08-26 08:50:50', '2023-08-26 08:50:50');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('0a56cbee-b55d-45ee-9e7b-68c287f62d4e', '2d79d6c3-a096-40db-ae4a-2dfa18d05482', 'Unsatisfactory', '4', '2023-08-26 08:50:50', '2023-08-26 08:50:50');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('aac8b648-ceac-451b-87ee-69582e778af3', 'a11275e7-0eeb-4e9f-b7dd-2adbb361ccfb', 'Keep', '1', '2023-08-26 08:50:50', '2023-08-26 08:50:50');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('13c72929-b2fb-4873-b95f-78d689deffb6', 'a11275e7-0eeb-4e9f-b7dd-2adbb361ccfb', 'Ship', '2', '2023-08-26 08:50:50', '2023-08-26 08:50:50');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('11459d3b-a712-4e31-bc80-0acc85301bcf', 'a11275e7-0eeb-4e9f-b7dd-2adbb361ccfb', 'Cull', '3', '2023-09-13 15:58:00', '2023-09-13 15:58:00');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('3aafef14-46f5-4f92-949a-57110101d19f', 'a11275e7-0eeb-4e9f-b7dd-2adbb361ccfb', 'Other', '4', '2023-09-13 15:58:00', '2023-09-13 15:58:00');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('e8f24d07-50e9-448f-a44d-95ca2656f679', '0c96e6f7-eb2e-406f-b639-d7d505f7ee3a', 'Satisfactory', '1', '2024-07-14 09:41:16', '2024-07-14 09:41:16');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('d061f696-bcf4-4e1e-93f1-ea3029011f44', '0c96e6f7-eb2e-406f-b639-d7d505f7ee3a', 'Unsatisfactory', '2', '2024-07-14 09:41:16', '2024-07-14 09:41:16');
INSERT INTO custom_evaluation_traits_table (id_customevaluationtraitsid, id_evaluationtraitid, custom_evaluation_item, custom_evaluation_order, created, modified) VALUES ('2731f09a-e2ea-42c0-8206-07d3acf29176', '0c96e6f7-eb2e-406f-b639-d7d505f7ee3a', 'Not Checked', '3', '2024-07-14 09:41:16', '2024-07-14 09:41:16');

INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('da1178b9-e4b9-4b25-8578-987aa5a8ecd2', 'Died Unknown', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '12', '2023-08-13 15:10:58', '2023-08-13 15:10:58');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('56c341d1-8f74-4c32-8ff7-54339346568b', 'Died Old Age', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '8', '2023-08-13 15:10:58', '2023-08-13 15:10:58');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('06423859-7830-481f-8abc-e813dfbc8bd3', 'Died Predator Unknown', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '13', '2023-08-13 15:10:58', '2023-08-13 15:10:58');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('265d5455-74b7-4532-b2a3-c8f97b12376d', 'Died Illness', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '5', '2023-08-13 15:10:58', '2023-08-13 15:10:58');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('e1b8f128-6606-4398-8e0a-ae7056127c83', 'Died Injury', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '7', '2023-08-13 15:10:58', '2023-08-13 15:10:58');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('9de9f67c-afc4-4d16-83d3-20c4337f4344', 'Died Stillborn', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '11', '2023-08-13 15:10:58', '2023-08-13 15:10:58');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('a979e8a2-05be-48fb-8d4f-d15a7c03af54', 'Died Birth Defect', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '1', '2023-08-13 15:10:58', '2023-08-13 15:10:58');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('926dc4c2-ee1c-4761-9e0a-c73c613e18a5', 'Died Meat', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '9', '2023-08-13 15:10:58', '2023-08-13 15:10:58');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('a06c138b-d9e5-4e29-9757-a4ad03537ebb', 'Died Euthanized', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '4', '2023-08-13 15:10:58', '2023-08-13 15:10:58');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('95637d5c-d9c1-412b-b9eb-48764a78c239', 'Died Dystocia', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '3', '2023-08-13 15:10:58', '2023-08-13 15:10:58');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('aabefa18-49fd-4e8b-aa86-da7d8fa9a6b9', 'Died Other', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '10', '2023-08-13 15:10:58', '2023-08-13 15:10:58');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('5bc56390-4ab7-406a-b412-f6674802083f', 'Died Culled', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '2', '2023-08-13 15:10:58', '2023-08-13 15:10:58');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('787b84e0-1e75-4977-a04c-62e6ffebdada', 'Died Administrative', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '22', '2023-08-13 15:10:58', '2023-08-13 15:10:58');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('f2b292d7-afe5-499e-b7b3-b10df6961a8b', 'Died Predator Dog', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '14', '2024-06-06 15:37:02', '2024-06-06 15:37:02');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('2cbda122-c6ac-4566-b753-33fc57f4118b', 'Died Predator Fox', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '15', '2024-06-06 15:37:02', '2024-06-06 15:37:02');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('1d6848fe-19aa-481a-a667-dd8f60c850a1', 'Died Predator Coyote', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '16', '2024-06-06 15:37:02', '2024-06-06 15:37:02');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('fb48f10a-d8be-49fa-a9d1-7486dabeba04', 'Died Predator Wolf', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '17', '2024-06-06 15:37:02', '2024-06-06 15:37:02');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('c160ae13-626b-4437-9513-90f9493ed933', 'Died Predator Bobcat', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '18', '2024-06-06 15:37:02', '2024-06-06 15:37:02');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('641e230a-6872-43b1-a5d5-e878d4228123', 'Died Predator Mountain Lion', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '19', '2024-06-06 15:37:02', '2024-06-06 15:37:02');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('38620818-a619-408a-92e3-713de0748c77', 'Died Predator Black Bear', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '20', '2024-06-06 15:37:02', '2024-06-06 15:37:02');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('9697f0d9-e4e2-4edf-bd8d-caa365dbf988', 'Died Predator Grizzly Bear', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '21', '2024-06-06 15:37:02', '2024-06-06 15:37:02');
INSERT INTO death_reason_table (id_deathreasonid, death_reason, id_contactid, id_companyid, death_reason_display_order, created, modified) VALUES ('b6bf79db-ac1e-4236-9aef-f35e80d01616', 'Died Parasites', NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '6', '2024-11-11 11:23:45', '2024-11-11 11:23:45');

INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('75a3bea8-2ffb-44fa-9fd4-08a2966f3a91', 'Subcutaneous Right Side Armpit', 'SQ RS Armpit', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('001c93a7-316a-4dc8-a32b-736e95f40ef3', 'Subcutaneous Left Side Armpit', 'SQ LS Armpit', '4', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('d4b45ad6-b1a7-4cbc-9f55-8afcc3dca908', 'Intermuscular Neck', 'IM Neck', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('38a6a777-0cdc-4310-a247-f7cd867b8ec2', 'Vagina', 'V', '11', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('784b8ccf-9477-4afe-85ec-577e551226c5', 'Mouth Drench', 'Mouth', '9', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('5b7b410b-64d8-463a-ad2f-36a979f6f23e', 'Intermuscular Leg', 'IM Leg', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('92897a9c-3367-4f45-a1ca-f5c9670fe727', 'Subcutaneous Right Side Neck', 'SQ RS Neck', '7', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('47763c9e-5a38-4f52-917e-43a1538d6436', 'Subcutaneous Left Side Neck', 'SQ LS Neck', '8', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('0788b045-c9cf-48d6-82c1-f014f3d4f2ad', 'Intravenous', 'IV', '10', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('4306f23d-ebd2-43d3-a847-ca3eb8490aee', 'Subcutaneous Right Side Behind Ear', 'SQ RS Ear', '5', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('0baed349-e8f4-429e-b4a4-7bd30f310495', 'Subcutaneous Left Side Behind Ear', 'SQ LS Ear', '6', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('72d49a78-9119-44e0-9ae3-ad90658ee2cd', 'Eye', 'Eye', '12', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('a38adc3b-5122-44c4-9f44-15755e976cb7', 'Subcutaneous Neck', 'SQ Neck', '13', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('97f6fba7-1db6-40a1-bac0-e5a3d38e2085', 'Unknown', 'Unk', '14', '2024-09-25 09:55:24', '2024-09-25 09:55:24');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('c557b93d-3295-411d-9176-8edd81c77313', 'Pour On Back', 'Pour On', '15', '2024-10-06 20:44:05', '2024-10-06 20:44:05');
INSERT INTO drug_location_table (id_druglocationid, drug_location_name, drug_location_abbrev, drug_location_display_order, created, modified) VALUES ('f93ec58b-6454-43d2-be24-9095b518bda8', 'Subcutaneous Behind Ear', 'SQ Behind Ear', '16', '2024-11-28 09:52:23', '2024-11-28 09:52:23');

INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('cc67900f-21c1-4795-9f4b-48ccbe99bd99', 'a735484d-d54d-45cf-aba6-4467b0484cad', 'Acepromazine', 'Acepromazine Maleate', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('f113f522-1049-4891-bae2-e1b4ab38623d', 'cd2c3b02-1335-4449-9563-bd76b7f0a915', 'Albon', 'Sulfadimethoxine', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('90e8a9aa-40b6-4c76-a6c3-7149451766ff', 'b280065e-fdde-4973-80dd-efe282e260a3', 'Ammonium Chloride', 'Ammonium Chloride', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('15f5599f-ce20-438a-89b9-10e0ec683bb7', '02f9ccfc-6165-48b8-bcd2-f026bfd651a2', 'Atroban', 'permethrin', '0', '2024-11-27 06:57:17', '2024-11-27 06:57:17');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('f934ba8a-8995-4b73-8f73-d2e18db8ef54', 'a735484d-d54d-45cf-aba6-4467b0484cad', 'Banamine', 'Flunixin Meglumine', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('e846fdf6-c1c1-483d-8186-f3249db062e0', '5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Bar-Vac 7', 'Bar-Vac 7', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('1132008f-31f0-49d9-9a15-a32f8ac35b28', '5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Bar-Vac CD&T', 'Bar-Vac CD&T', '0', '2024-11-21 14:03:35', '2024-11-21 14:03:35');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('f68e0c64-4d61-4b5d-9655-f9d20008d4dc', '5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Baycox', 'Toltrazuril', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('c86dc0e7-0981-4687-b9db-da08e8d73355', 'cd2c3b02-1335-4449-9563-bd76b7f0a915', 'Bio-Mycin 200', 'Oxytetracycline', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('7b61cb4d-0930-4eea-8aaf-3b57d2dc7138', '02f9ccfc-6165-48b8-bcd2-f026bfd651a2', 'Boss', 'permethrin', '0', '2024-11-21 14:03:35', '2024-11-21 14:03:35');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('9d050060-e00f-44f2-8e09-21092e5b4eea', 'a735484d-d54d-45cf-aba6-4467b0484cad', 'Bute', 'Phenylbutazone', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('1a8e05c7-0c5f-42cc-9b0f-865c2b8cc22f', 'bf7eb835-99ff-43a7-b63e-4b089f034b11', 'Chrono-Gest Sponges', 'Progesterone', '1', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('49d388c0-94e6-4f8d-9166-2bcaaf8f69af', '5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Clostridium A 14522-01', 'Clostridium Type A', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('e435531f-d3f9-447c-b94e-23d5e89ce6a1', '5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Clostridium C&D Antitoxin', 'Clostridium Type C&D Antitoxin', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('82682fef-2f03-44a0-9003-13c0c08b8b24', 'cd2c3b02-1335-4449-9563-bd76b7f0a915', 'Combi-Pen-48', 'Penicillin', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('7bca26c3-4f74-4882-b042-5942d78793bf', 'd37f46c5-4a9d-492a-b5eb-eb2f2f2bb4b6', 'Corid', 'Amprolium', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('1982707f-d69f-44c0-9065-d3ee657bb7d9', '5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Covexin 8', 'Covexin 8', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('96e6b5fb-3083-4104-afaf-2e66e4508d3d', '6976a1e8-2bff-4e36-9a5e-0e3ee58d5dea', 'Cydectin', 'Moxidectin', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('daa0e40b-851d-4966-b700-3ea48e749f42', 'a735484d-d54d-45cf-aba6-4467b0484cad', 'Dexamethasone', 'Dexamethasone', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('eac31274-1cf7-4763-a27a-c84f648e27ea', 'cd2c3b02-1335-4449-9563-bd76b7f0a915', 'Duramycin 72-200', 'Oxytetracycline', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('8be71a9c-5a47-46fa-8f5b-a7dc8a0329c3', 'bf7eb835-99ff-43a7-b63e-4b089f034b11', 'Eazi-Breed CIDR Sheep and Goat', 'Progesterone', '1', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('efdd0bea-f006-4292-9e80-9050f16eac18', 'a735484d-d54d-45cf-aba6-4467b0484cad', 'Epinephrine', 'Epinephrine', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('93fcdf3c-8a1e-46e8-b312-3eb7896f13bd', '6976a1e8-2bff-4e36-9a5e-0e3ee58d5dea', 'Equi-Phat ProTal', 'Pyrantel pamoate', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('caa7332c-1b1b-4f9b-a78f-09f46ae2711e', 'cd2c3b02-1335-4449-9563-bd76b7f0a915', 'Erythromycin Eye Ointment', 'Erythromycin Eye Ointment', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('794c7ef5-94bc-4d7d-97a7-ef92df1225ff', 'bf7eb835-99ff-43a7-b63e-4b089f034b11', 'Estrumate', 'Cloprostenol Sodium', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('b95fe9b8-039c-40ea-bbf5-bcde0f7c98e1', 'cd2c3b02-1335-4449-9563-bd76b7f0a915', 'Excede', 'Ceftiofur', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('80a6ea75-af52-43f8-b88d-88adfc9f8bc9', 'bf7eb835-99ff-43a7-b63e-4b089f034b11', 'Folligon PMSG', 'PMSG', '0', '2024-11-21 14:03:35', '2024-11-21 14:03:35');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('e58176ad-ee6a-49c0-9cd7-5798d45e060e', 'b280065e-fdde-4973-80dd-efe282e260a3', 'GastroGuard', 'Omeprazole', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('54f3abfe-3bac-48ff-93ee-af47493dced5', '5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Glanvac 6', 'Glanvac 6', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('b30b433e-1636-4123-9bbc-d6b7d8ced2a1', '6976a1e8-2bff-4e36-9a5e-0e3ee58d5dea', 'Ivermectin', 'Ivermectin', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('d7166798-0552-4e7f-b47f-4646474b94b5', '6976a1e8-2bff-4e36-9a5e-0e3ee58d5dea', 'Ivomec', 'Ivermectin', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('1c3510b8-4fa2-4a4d-806a-8bcd038ef423', 'cd2c3b02-1335-4449-9563-bd76b7f0a915', 'Liquamycin LA-200', 'Oxytetracycline', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('88b63f12-fa0d-43e6-b689-784597e19e3c', 'bf7eb835-99ff-43a7-b63e-4b089f034b11', 'Lutalyse', 'dinoprost trometamol', '0', '2024-11-21 14:03:35', '2024-11-21 14:03:35');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('fb732815-2517-4d0e-9324-a07ec1318257', 'a735484d-d54d-45cf-aba6-4467b0484cad', 'Meloxicam', 'Meloxicam', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('11f14152-767e-474e-aeab-6aaa149a73e7', 'cd2c3b02-1335-4449-9563-bd76b7f0a915', 'Naxcel', 'Naxcel', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('fb054437-d64f-4f72-a78c-6af0717f083a', 'cd2c3b02-1335-4449-9563-bd76b7f0a915', 'Nuflor', 'Florfenicol', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('f378ed38-ed2b-4508-88bb-99d10dbd82a1', 'bf7eb835-99ff-43a7-b63e-4b089f034b11', 'PG 600', 'PMSG and HCG', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('ea50386c-fc6c-4f9a-b1a4-66285047b502', 'bf7eb835-99ff-43a7-b63e-4b089f034b11', 'PMSG', 'PMSG', '0', '2024-11-21 14:03:35', '2024-11-21 14:03:35');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('d6f9e7b9-d531-4dae-8f82-735007988cae', '6976a1e8-2bff-4e36-9a5e-0e3ee58d5dea', 'Panacur', 'Fenbendazole', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('b058702a-c3f3-4221-8bb3-0679ef712afe', 'cd2c3b02-1335-4449-9563-bd76b7f0a915', 'PenOne Pro', 'Penicillin', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('b05cd66d-6732-4b86-b385-4ba93aefcf60', 'bf7eb835-99ff-43a7-b63e-4b089f034b11', 'Pregnecol PMSG', 'PMSG', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('2b181fa8-38a8-42e3-904b-a57b2567db45', 'a735484d-d54d-45cf-aba6-4467b0484cad', 'Prevail Banamine', 'Flunixin Meglumine', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('00fa39f7-c1fb-4dab-bc8d-4c75a943c4ed', '6976a1e8-2bff-4e36-9a5e-0e3ee58d5dea', 'Privermectin', 'Ivermectin', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('249436de-0568-4463-8d77-2664ffb9ac17', 'b280065e-fdde-4973-80dd-efe282e260a3', 'Probios Granules', 'Lactic Acid Bacteria', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('d21348ac-29bc-479c-bcad-e16dfb6b60f8', 'b280065e-fdde-4973-80dd-efe282e260a3', 'Probios Paste', 'Lactic Acid Bacteria', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('8584a3de-c03f-4b46-9eea-1425206e6c1f', '6976a1e8-2bff-4e36-9a5e-0e3ee58d5dea', 'Prohibit Levamisole', 'Levamisole Hydrochloride', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('9a482940-2190-4707-842c-111c7052cd66', '5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Rabies Imrab 3', 'Rabies', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('fe6988e0-eb1d-4d58-b4b8-031211911292', 'cd2c3b02-1335-4449-9563-bd76b7f0a915', 'SMZ Tablets', '800mg sulfamethazine and 160mg trimethoprim', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('ab30af47-c058-474b-b88f-dd2236619f93', '6976a1e8-2bff-4e36-9a5e-0e3ee58d5dea', 'Safeguard', 'Fenbendazole', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('299efc82-7fcd-4350-bd20-08e9d273b8cc', 'bf7eb835-99ff-43a7-b63e-4b089f034b11', 'Serum Gonadotrophin', 'PMSG', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('fe843f14-f96a-49eb-b668-166d8197f3aa', 'cd2c3b02-1335-4449-9563-bd76b7f0a915', 'Spectramast LC', 'Ceftiofur', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('c8b545a7-b69d-40ae-9c4d-117d2424ac99', '5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Super Tet', 'Tetanus Toxoid', '0', '2024-11-21 14:03:35', '2024-11-21 14:03:35');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('60915b60-25da-4682-a0a9-c60f11393396', '5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Tetanus Toxoid', 'Tetanus Toxoid', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('024eedda-3f13-4fe9-a158-13c629112f52', 'b280065e-fdde-4973-80dd-efe282e260a3', 'Therabloat', 'Poloxalene', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('d946312a-654a-4ba1-9b0a-22217f767fc2', '5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Ultrabac 8', 'Ultrabac 8', '0', '2024-11-21 14:03:35', '2024-11-21 14:03:35');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('0ce2e93e-a3f1-4ae1-bde3-89e1644638fc', '6976a1e8-2bff-4e36-9a5e-0e3ee58d5dea', 'Valbazen', 'Albendazole', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('b2c1a9f1-b3e1-4a25-913c-1fcb30a940e8', 'cd2c3b02-1335-4449-9563-bd76b7f0a915', 'VetriPen G', 'Penicillin', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('175e089e-bd62-4c14-bd3c-f24a56cff534', '5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Vision 8', 'Vision 8', '0', '2024-11-21 14:03:35', '2024-11-21 14:03:35');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('17eb6124-50a7-440d-af0e-b4b9aafd2baf', '5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Vision CD&T', 'Vision CD&T', '0', '2024-11-21 14:03:35', '2024-11-21 14:03:35');
INSERT INTO drug_table (id_drugid, id_drugtypeid, trade_drug_name, generic_drug_name, drug_removable, created, modified) VALUES ('815322fb-a07d-4d70-a0f2-3571aa2b0a91', 'b280065e-fdde-4973-80dd-efe282e260a3', 'Vitamine E-AD', 'd-alpha Tocopherol w/ AD', '0', '2024-08-10 10:36:05', '2024-11-12 11:41:47');

INSERT INTO drug_type_table (id_drugtypeid, drug_type, drug_type_display_order, created, modified) VALUES ('6976a1e8-2bff-4e36-9a5e-0e3ee58d5dea', 'Dewormer', '7', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_type_table (id_drugtypeid, drug_type, drug_type_display_order, created, modified) VALUES ('5a249b9b-dad9-4836-aa7e-3629e4e74d2d', 'Vaccine', '6', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_type_table (id_drugtypeid, drug_type, drug_type_display_order, created, modified) VALUES ('cd2c3b02-1335-4449-9563-bd76b7f0a915', 'Antibiotic', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_type_table (id_drugtypeid, drug_type, drug_type_display_order, created, modified) VALUES ('bf7eb835-99ff-43a7-b63e-4b089f034b11', 'Hormone', '5', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_type_table (id_drugtypeid, drug_type, drug_type_display_order, created, modified) VALUES ('d37f46c5-4a9d-492a-b5eb-eb2f2f2bb4b6', 'Coccidiostat', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_type_table (id_drugtypeid, drug_type, drug_type_display_order, created, modified) VALUES ('b280065e-fdde-4973-80dd-efe282e260a3', 'Feed Supplement', '4', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_type_table (id_drugtypeid, drug_type, drug_type_display_order, created, modified) VALUES ('a735484d-d54d-45cf-aba6-4467b0484cad', 'Analgesic', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO drug_type_table (id_drugtypeid, drug_type, drug_type_display_order, created, modified) VALUES ('02f9ccfc-6165-48b8-bcd2-f026bfd651a2', 'Insecticide', '8', '2024-10-07 07:05:42', '2024-10-07 07:05:42');

INSERT INTO ebv_calculation_method_table (id_ebvcalculationmethodid, ebv_table_name, ebv_calculation_method_abbreviation, ebv_calculation_method_display_order, created, modified) VALUES ('a270098c-bb01-4549-97f8-346aa1867939', 'sheep_nsip_ebv_table', 'NSIP', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO ebv_calculation_method_table (id_ebvcalculationmethodid, ebv_table_name, ebv_calculation_method_abbreviation, ebv_calculation_method_display_order, created, modified) VALUES ('41863de3-9fa7-434c-b384-afe03b09fdb2', 'sheep_lambplan_ebv_table', 'LambPlan', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO ebv_calculation_method_table (id_ebvcalculationmethodid, ebv_table_name, ebv_calculation_method_abbreviation, ebv_calculation_method_display_order, created, modified) VALUES ('ce84deb3-fb13-45ad-9f0f-8d7604a8a711', 'sheep_bvest_ebv_table', 'BVest', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO ebv_calculation_method_table (id_ebvcalculationmethodid, ebv_table_name, ebv_calculation_method_abbreviation, ebv_calculation_method_display_order, created, modified) VALUES ('eab27252-85e9-48b0-b58f-587cb1fefa05', 'sheep_signet_ebv_table', 'Signet', '4', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO ebv_calculation_method_table (id_ebvcalculationmethodid, ebv_table_name, ebv_calculation_method_abbreviation, ebv_calculation_method_display_order, created, modified) VALUES ('1c9b5969-bd4a-4cdd-9c9f-984a2add2d30', 'sheep_merinoselect_ebv_table', 'MerinoSelect', '5', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO ebv_calculation_method_table (id_ebvcalculationmethodid, ebv_table_name, ebv_calculation_method_abbreviation, ebv_calculation_method_display_order, created, modified) VALUES ('7ead4f14-95d3-4c0c-8647-ee2f5e10ed78', 'goat_kidplan_ebv_table', 'KidPlan', '6', '2024-02-18 14:37:57', '2024-02-18 14:37:57');

INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('73e866c5-d5ea-47b0-9163-eed7dad5a81d', 'a270098c-bb01-4549-97f8-346aa1867939', 'Birth', '0', '1', '1', '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('3d285533-212c-48e9-b008-02121f5475b1', 'a270098c-bb01-4549-97f8-346aa1867939', 'Weaning', '40', '120', '60', '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('646f2db3-9657-4b75-af6b-c58932687bfa', 'a270098c-bb01-4549-97f8-346aa1867939', 'Early Post Weaning', '120', '210',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('c534677f-cb7b-4c26-867a-9ee6b0a4ca84', 'a270098c-bb01-4549-97f8-346aa1867939', 'Postweaning', '160', '340',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('afc387d0-2f0c-496d-a83f-167861b09dca', 'a270098c-bb01-4549-97f8-346aa1867939', 'Yearling', '290', '430',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('8e99e843-1d09-4686-8822-6beee10bffd7', 'a270098c-bb01-4549-97f8-346aa1867939', 'Hogget', '410', '550',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('24bd00d7-88e4-4e50-b938-26b62c1340fd', 'a270098c-bb01-4549-97f8-346aa1867939', 'Adult', '421', '1107',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('6fa2258f-0630-46b2-b890-b25753321037', 'a270098c-bb01-4549-97f8-346aa1867939', 'Adult3', '655', '1549',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('9a1948c5-045a-402b-b7df-5776860b52e1', 'a270098c-bb01-4549-97f8-346aa1867939', 'Adult4', '918', '1990',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('bb710891-3ce4-4848-a5cb-05a7660b4a12', 'a270098c-bb01-4549-97f8-346aa1867939', 'Adult5', '1181', '2553',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('3e167f00-7001-44ea-bf1b-4d0c95e59cab', '41863de3-9fa7-434c-b384-afe03b09fdb2', 'Birth', '0', '1', '1', '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('345e0de5-d103-4c83-a56d-b666f95193aa', '41863de3-9fa7-434c-b384-afe03b09fdb2', 'Weaning', '42', '120', '80', '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('de4187b1-7cf9-4408-9136-f77f4d33a227', '41863de3-9fa7-434c-b384-afe03b09fdb2', 'Early Post Weaning', '120', '210',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('68395e95-0b94-49e6-9685-4c1818944042', '41863de3-9fa7-434c-b384-afe03b09fdb2', 'Postweaning', '210', '300',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('3cc46e1f-7ebb-4de5-afd3-092d5bf9173f', '41863de3-9fa7-434c-b384-afe03b09fdb2', 'Yearling', '300', '400',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('d5b8900a-7521-42f9-bd05-2e7c1a0e7691', '41863de3-9fa7-434c-b384-afe03b09fdb2', 'Hogget', '400', '540',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('d7425fae-01bc-418e-918e-b88a0e00080a', '41863de3-9fa7-434c-b384-afe03b09fdb2', 'Adult', '421', '1107',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('bdc1126a-d340-40f5-b72b-697af3b3bcaf', '41863de3-9fa7-434c-b384-afe03b09fdb2', 'Adult3', '655', '1549',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('30736cec-d9eb-42ef-b999-0263fdfc1140', '41863de3-9fa7-434c-b384-afe03b09fdb2', 'Adult4', '918', '1990',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');
INSERT INTO ebv_date_ranges_table (id_ebvdaterangesid, id_ebvcalculationmethodid, age_name, ebv_minimum_days, ebv_maximum_days, ebv_optimal_days, created, modified) VALUES ('57cd62c5-69fb-4e2a-92a9-ba528961c232', '41863de3-9fa7-434c-b384-afe03b09fdb2', 'Adult5', '1181', '2553',NULL, '2024-12-28 12:04:57', '2024-12-28 12:04:57');

INSERT INTO email_type_table (id_emailtypeid, email_type, email_type_display_order, created, modified) VALUES ('af3408ba-d8dd-4189-9091-21c17f374ea1', 'Primary', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO email_type_table (id_emailtypeid, email_type, email_type_display_order, created, modified) VALUES ('351dfa5c-be92-4d8f-97e2-ec0fbebfdd4b', 'Work', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO email_type_table (id_emailtypeid, email_type, email_type_display_order, created, modified) VALUES ('42c84696-aef4-4cf8-9eae-741351727371', 'Home', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO email_type_table (id_emailtypeid, email_type, email_type_display_order, created, modified) VALUES ('0ee1a7dc-706f-4d87-bbe1-579003dfcbfc', 'Personal', '4', '2023-11-26 11:54:58', '2023-11-26 11:54:58');

INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('aedb6197-7e31-4cdb-975e-1ba0605a85fb', 'Teeth Alignment', '79f17211-ea5d-44a5-a462-8607a5743b08', '2',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('08505284-9304-4f14-b3c5-6811d3fef54c', 'Missing Teeth', '79f17211-ea5d-44a5-a462-8607a5743b08', '3',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('3100171d-1e6e-4db6-9316-b9188e75cc8a', 'White on Nose', '79f17211-ea5d-44a5-a462-8607a5743b08', '4',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('127b277f-558a-435c-b323-a4b740bea3e8', 'Head', '79f17211-ea5d-44a5-a462-8607a5743b08', '5',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('d2e20574-0c8e-44d7-9234-7861c8474a55', 'Horn Shape', '79f17211-ea5d-44a5-a462-8607a5743b08', '6',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('a301e64d-55b2-4af8-8ba8-abe7998102b4', 'Body', '79f17211-ea5d-44a5-a462-8607a5743b08', '8',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('d1c3e8d7-87a9-42d5-8971-3ae8529e5022', 'Legs', '79f17211-ea5d-44a5-a462-8607a5743b08', '9',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('81085936-f338-421d-9e16-e2dab0aee9d2', 'Udder Lumps', '79f17211-ea5d-44a5-a462-8607a5743b08', '10',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('7bd2a8af-faa5-414d-ac3c-12bd7a94b748', 'White in Britch or Body', '79f17211-ea5d-44a5-a462-8607a5743b08', '11',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('3456c7f5-e293-4525-a1a1-b8a05d52e911', 'Mothering Ability', '79f17211-ea5d-44a5-a462-8607a5743b08', '12',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('8f111fe6-39d4-4532-a19d-9ca4625dfce5', 'Fleece Quality', '79f17211-ea5d-44a5-a462-8607a5743b08', '13',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('1425fe44-d99b-485d-a8ea-af627431ad5c', 'Fleece Soft', '79f17211-ea5d-44a5-a462-8607a5743b08', '14',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('275ddfba-a1c4-4b0a-b171-5a30142d9b9d', 'Fleece Length', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '14', 'b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('f0dba60a-19c2-414b-a78d-1a2648c2f974', 'Fleece Weight', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '15', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('be459431-f1b7-4c99-a044-d3baff3e3c46', 'Scrotal Circumference', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '23', 'b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('44d307ab-5c32-44c7-bb06-e65c11269716', 'Weight', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '26', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('6331f806-ff2a-4a12-b2e1-243723e3022d', 'Horn Buds', '79f17211-ea5d-44a5-a462-8607a5743b08', '7',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('ee2d69d4-40d9-4bee-9248-eb439308788e', 'Temperament', '79f17211-ea5d-44a5-a462-8607a5743b08', '17',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('6a0f4c54-06e7-49a6-9715-6624e2faf1ea', 'Fleece Crimp', '79f17211-ea5d-44a5-a462-8607a5743b08', '15',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('84f35d50-d8e9-44e8-a62c-9c41f7394a40', 'Insemination Depth', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '19', 'b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('f8e01215-e228-45d7-ac1a-cd7b1ff8419f', 'Estrus Characteristics', 'abfb56ad-52a4-4e3e-99f5-19e8582f228f', '8',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('72a721eb-9738-4023-b910-93af6e33386d', 'Pregnancy Status', 'abfb56ad-52a4-4e3e-99f5-19e8582f228f', '4',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('2046720b-c03b-4640-8e57-2eaf31e05b53', 'Number of Lambs Expected', '79f17211-ea5d-44a5-a462-8607a5743b08', '19',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('5e372585-ce1d-4bb6-893a-c765b915b882', 'LambEase', 'abfb56ad-52a4-4e3e-99f5-19e8582f228f', '6',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('bafda80d-d004-400b-9fca-f97d609fd62c', 'Udder Status', 'abfb56ad-52a4-4e3e-99f5-19e8582f228f', '5',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('1f603321-a363-4f03-ad4c-a4d8b4e592b2', 'Carcass Weight', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '4', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('780386a4-33ce-4e62-92d7-9372c15471f4', 'Tail Length', '79f17211-ea5d-44a5-a462-8607a5743b08', '16',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('e0b238d5-e740-4662-8284-b4573c3570c1', 'Blood LH via ELISA Test', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '1', '49638d1d-b964-48bd-86ac-498930612bc8', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('10953cf8-c67f-4b85-b2dd-441d57fe33dd', 'Loin Eye Muscle Depth Ultrasound', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '21', 'b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('d071c11a-a4cc-4462-b50c-33bcee244759', 'Loin Fat Depth Ultrasound', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '22', 'b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('837ee73d-c608-4c00-881d-a92d372402e5', 'Heartgirth', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '18', 'b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('0a39b244-7a44-4b1a-9211-5bbc07ced654', 'Body Length Poll to Pin Bone', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '3', 'b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('d382fd09-ebb5-41d9-8e06-ae7a8e72e372', 'Width Hip to Hip Across Back', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '27', 'b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('0e42c5ac-51ba-4b72-909e-62e4c835aaeb', 'Body Length Point of Shoulder to Point of Hip', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '2', 'b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('3092dd2e-43e7-49da-b583-bdbb164f6ab5', 'Loin Eye Area Ultrasound', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '20', 'a026b938-b873-4b04-bae7-e7dcf1100a29', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('c00505e5-9d8b-48d6-b6fd-84cdb3c65127', 'Suck Reflex', 'abfb56ad-52a4-4e3e-99f5-19e8582f228f', '7',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('e6fc96d2-c97e-4bb4-9a61-cbbc01b86ec0', 'FEC', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '5', '46101d80-0359-4c1c-a9f0-aa001fc29647', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('68117b62-e712-4155-beb4-39404fc3f2be', 'FEC Strongyles', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '11', '46101d80-0359-4c1c-a9f0-aa001fc29647', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('44e3efbb-33ca-42e5-9ec1-c891b15e1273', 'FEC Nematodirus', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '9', '46101d80-0359-4c1c-a9f0-aa001fc29647', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('46236c86-979d-4ee9-a52c-21e8f95c382c', 'FEC Trichuris', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '13', '46101d80-0359-4c1c-a9f0-aa001fc29647', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('cd3116a3-0a67-4a75-8b3e-0df038afed50', 'FEC Capillarid', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '6', '46101d80-0359-4c1c-a9f0-aa001fc29647', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('b5225cb9-a8c2-4440-ab93-b289b9c66230', 'FEC Moniezia', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '8', '46101d80-0359-4c1c-a9f0-aa001fc29647', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('0d9b98a3-6632-4bc6-9566-8fb4362c6c3d', 'FEC Eimeria', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '7', '46101d80-0359-4c1c-a9f0-aa001fc29647', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('cc477384-19c8-42f1-bbac-2a4ad24898f4', 'FEC Other', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '10', '46101d80-0359-4c1c-a9f0-aa001fc29647', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('d8127b75-6dfe-49c5-9cef-77d79d44e07d', 'Breeding Soundness Exam', 'abfb56ad-52a4-4e3e-99f5-19e8582f228f', '1',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('fa366b50-1980-409b-9934-f92bc1ffdead', 'Sperm Motility % Motile', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '25', '2532d531-bea9-4315-80f0-f2ab0c088d90', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('4e462d47-c7a3-427c-a424-f9271918ac16', 'Sperm Morphology % Normal', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '24', '2532d531-bea9-4315-80f0-f2ab0c088d90', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('3cf30d5f-8c59-4a44-8b0c-3fb162718532', 'Gestation Days Early', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '16', '2e55da17-1bd4-4178-a83b-5c1f2034d59e', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('81498ee6-d640-4fa3-a0ee-6820708bc4a3', 'Gestation Days Late', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '17', '2e55da17-1bd4-4178-a83b-5c1f2034d59e', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('b5865a35-f213-4b8a-9077-5472cd8a25b3', 'Body Condition Score', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '2', '49638d1d-b964-48bd-86ac-498930612bc8', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('367b7b4c-fd4a-4363-bc64-7b4a6045a918', 'FEC Strongyloides', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '12', '46101d80-0359-4c1c-a9f0-aa001fc29647', '2022-02-23 09:15:10', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('2d79d6c3-a096-40db-ae4a-2dfa18d05482', 'Optimal Livestock Ram Test', 'abfb56ad-52a4-4e3e-99f5-19e8582f228f', '2',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('a11275e7-0eeb-4e9f-b7dd-2adbb361ccfb', 'Simple Sort', 'abfb56ad-52a4-4e3e-99f5-19e8582f228f', '3',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('e49cb696-41ec-462e-9bd1-7ed0e1960d79', 'Male Animals Born', '79f17211-ea5d-44a5-a462-8607a5743b08', '27',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('391b2b69-4976-426d-ba3b-2911bc5a8f2a', 'Female Animals Born', '79f17211-ea5d-44a5-a462-8607a5743b08', '28',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('7065f88c-ee13-4682-8795-c9c14a65b1f4', 'Unknown Sex Animals Born', '79f17211-ea5d-44a5-a462-8607a5743b08', '29',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('8a2a31ee-8b85-4ca5-9ca5-176766e3ee71', 'Stillborn Animals', '79f17211-ea5d-44a5-a462-8607a5743b08', '30',NULL, '2022-02-23 09:15:10', '2022-02-23 09:15:10');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('262de734-24fe-4fb4-9026-a444f1c94e1d', 'Scrotal Palpation Scored', '79f17211-ea5d-44a5-a462-8607a5743b08', '18',NULL, '2022-02-23 09:15:10', '2024-05-02 14:13:28');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('968eae00-3c84-43bd-ab91-ce19246c2f79', 'Ram Lambs Born', '79f17211-ea5d-44a5-a462-8607a5743b08', '20',NULL, '2023-04-02 16:18:34', '2023-04-02 16:18:34');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('bd0925b6-bf1d-4ac8-a275-7ba6da122d41', 'Wether Lambs Born', '79f17211-ea5d-44a5-a462-8607a5743b08', '21',NULL, '2023-04-02 16:18:34', '2023-04-02 16:18:34');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('923ee982-d1b4-4968-a9f0-9aed7126abc2', 'Ewe Lambs Born', '79f17211-ea5d-44a5-a462-8607a5743b08', '22',NULL, '2023-04-02 16:18:34', '2023-04-02 16:18:34');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('601f910c-33fa-4894-87ae-9eff9f7d4b9e', 'Unknown Sex Lambs Born', '79f17211-ea5d-44a5-a462-8607a5743b08', '23',NULL, '2023-08-11 11:06:33', '2023-08-11 11:06:33');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('5f6f1a22-756c-4b12-8be3-4e2fd7d17fb5', 'Stillborn Lambs', '79f17211-ea5d-44a5-a462-8607a5743b08', '24',NULL, '2023-04-02 16:18:34', '2023-04-02 16:18:34');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('8644a896-c70c-463e-a3bd-12e7ae0e0a38', 'Aborted Lambs', '79f17211-ea5d-44a5-a462-8607a5743b08', '25',NULL, '2023-07-18 10:37:19', '2023-07-18 10:37:19');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('2b5e739c-06c8-4e24-915f-ba2484ffaa8e', 'Adopted Lambs', '79f17211-ea5d-44a5-a462-8607a5743b08', '26',NULL, '2023-08-11 11:06:33', '2023-08-11 11:06:33');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('a38a8c05-9c4c-46a5-8bb5-c3398b8b8c94', 'Fleece Color', 'abfb56ad-52a4-4e3e-99f5-19e8582f228f', '9',NULL, '2023-08-14 12:59:43', '2023-08-14 12:59:43');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('3cc02aad-abd0-4061-a26e-f60799656853', 'b. ovis S/P ratios', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '79', '49638d1d-b964-48bd-86ac-498930612bc8', '2023-10-01 15:30:00', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('0c96e6f7-eb2e-406f-b639-d7d505f7ee3a', 'Scrotal Palpation Custom', 'abfb56ad-52a4-4e3e-99f5-19e8582f228f', '1',NULL, '2024-08-24 11:36:27', '2024-08-24 11:36:27');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('e6651a9e-bf91-45eb-93d6-e2b36d1edfec', 'Retail Meat Breast', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '81', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('d8fd75d7-5c04-418c-aaae-51e842014d42', 'Retail Meat French Rack', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '83', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('e67e64f1-e01c-4e40-b605-36e18800c860', 'Retail Meat Ground', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '84', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('37ac151b-10b6-46e8-8242-5a190a360c90', 'Retail Meat Leg Bone In', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '85', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('c3c4df7f-151a-4bbc-8321-b4b23addd21f', 'Retail Meat Leg Boneless', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '86', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('f969cb36-7bad-41f5-bb5c-a7564d9aba5a', 'Retail Meat Loin', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '87', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('c374d1cd-1b0b-4155-9638-360327ffcc5f', 'Retail Meat Neck', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '88', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('d2fd22bc-6040-416b-adec-83426256d1cc', 'Retail Meat Rib Chops', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '89', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('daa5a4f0-b89b-4dfc-9da3-94df0e7882ab', 'Retail Meat Ribs', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '90', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('a3ed0fbf-7056-49d4-8eb8-8a6ff7a9768e', 'Retail Meat Sausage Kolbasi', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '91', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('d54fc12d-9c53-4ef4-9f53-8b13a633ed61', 'Retail Meat Sausage Sweet Italian', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '92', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('a7e459ae-1175-4acc-8ea6-981ef4b80772', 'Retail Meat Shank', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '93', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('c8809b52-14d8-4c97-a61d-2e52fc5a787b', 'Retail Meat Shoulder Boneless', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '94', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('845ddab0-702e-4ba6-b3f7-ce791faa90d9', 'Retail Meat Stew', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '95', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('920bce6b-cbe7-49e0-859b-9dcdb0a24d49', 'Retail Meat Total Weight', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '96', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2024-08-24 11:36:27', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('9984d8dd-5d2e-45e9-ac92-576630cb01fc', 'FAMACHA', '79f17211-ea5d-44a5-a462-8607a5743b08', '1',NULL, '2024-09-07 10:47:47', '2024-09-07 10:47:47');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('bb128c1e-de2d-4d33-aeca-9994f63ab427', 'Rectal Temperature', '7079e750-9fc2-41e1-b46c-8505cdb8e67f', '98', '8e827095-d519-4db8-ac18-56e502c34431', '2024-09-13 11:16:18', '2024-09-29 07:29:35');
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('619ffe00-86d8-4aa8-960f-141ed111092f', 'General Health', '79f17211-ea5d-44a5-a462-8607a5743b08', 99, NULL, '2024-11-25 15:24:41', '2024-11-25 15:24:41');  
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('0de3d707-59ad-4261-8f93-f4cd86480347', 'SubOrbital Pit Size', '79f17211-ea5d-44a5-a462-8607a5743b08', 100, NULL, '2024-12-11 15:53:38', '2024-12-11 15:53:38');  
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('4088e404-d30c-4765-b2b7-bfa4cf02c199', 'Extra Nipples', '79f17211-ea5d-44a5-a462-8607a5743b08', 101, NULL, '2024-12-11 15:53:38', '2024-12-11 15:53:38');  
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('c5631f34-e906-483f-9f11-ac5b74682450', 'Teeth Shape', '79f17211-ea5d-44a5-a462-8607a5743b08', 102, NULL, '2024-12-17 09:49:24', '2024-12-17 09:49:24');  
INSERT INTO evaluation_trait_table (id_evaluationtraitid, trait_name, id_evaluationtraittypeid, evaluation_trait_display_order, id_unitstypeid, created, modified) VALUES ('58f8290b-058d-4307-86dd-33b0c0407f6b', 'Horn Width', '79f17211-ea5d-44a5-a462-8607a5743b08', 103, NULL, '2025-01-12 08:07:47', '2025-01-12 08:07:47');

INSERT INTO evaluation_trait_type_table (id_evaluationtraittypeid, trait_type, trait_type_display_order, created, modified) VALUES ('79f17211-ea5d-44a5-a462-8607a5743b08', 'Score 1 to 5', '1', '2024-12-28 12:49:05', '2024-12-28 12:49:05');
INSERT INTO evaluation_trait_type_table (id_evaluationtraittypeid, trait_type, trait_type_display_order, created, modified) VALUES ('7079e750-9fc2-41e1-b46c-8505cdb8e67f', 'Real Value', '2', '2024-12-28 12:49:05', '2024-12-28 12:49:05');
INSERT INTO evaluation_trait_type_table (id_evaluationtraittypeid, trait_type, trait_type_display_order, created, modified) VALUES ('abfb56ad-52a4-4e3e-99f5-19e8582f228f', 'User List', '3', '2024-12-28 12:49:05', '2024-12-28 12:49:05');

INSERT INTO genetic_characteristic_calculation_method_table (id_geneticcharacteristiccalculationmethodid, genetic_characteristic_calculation_method, genetic_characteristic_calculation_method_display_order, created, modified) VALUES ('3fdbbcd5-cbde-4da7-8747-3400aee4005d', 'DNA Sample', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO genetic_characteristic_calculation_method_table (id_geneticcharacteristiccalculationmethodid, genetic_characteristic_calculation_method, genetic_characteristic_calculation_method_display_order, created, modified) VALUES ('1d809cb1-718b-41b2-bbb4-a79da6091624', 'Pedigree Deduction', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO genetic_characteristic_calculation_method_table (id_geneticcharacteristiccalculationmethodid, genetic_characteristic_calculation_method, genetic_characteristic_calculation_method_display_order, created, modified) VALUES ('1ae4b983-104a-4e8e-b269-ff3790608c8d', 'Observation', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');

INSERT INTO genetic_characteristic_table (id_geneticcharacteristicid, genetic_characteristic_table_name, genetic_characteristic_table_display_name, genetic_characteristic_table_display_order, created, modified) VALUES ('91d9cd52-817c-446a-b6a3-d5c30dfb7ad6', 'genetic_codon112_table', 'Codon 112', '1', '2023-05-13 17:13:22', '2023-05-13 17:13:22');
INSERT INTO genetic_characteristic_table (id_geneticcharacteristicid, genetic_characteristic_table_name, genetic_characteristic_table_display_name, genetic_characteristic_table_display_order, created, modified) VALUES ('6612a679-deaf-44ec-ad3a-ea752f422840', 'genetic_codon136_table', 'Codon 136', '2', '2023-05-13 17:13:22', '2023-05-13 17:13:22');
INSERT INTO genetic_characteristic_table (id_geneticcharacteristicid, genetic_characteristic_table_name, genetic_characteristic_table_display_name, genetic_characteristic_table_display_order, created, modified) VALUES ('3d52128e-81c0-40b1-b10e-14a6ece16acc', 'genetic_codon141_table', 'Codon 141', '3', '2023-05-13 17:13:22', '2023-05-13 17:13:22');
INSERT INTO genetic_characteristic_table (id_geneticcharacteristicid, genetic_characteristic_table_name, genetic_characteristic_table_display_name, genetic_characteristic_table_display_order, created, modified) VALUES ('1ae30219-33da-42e2-b43a-0b840a062679', 'genetic_codon154_table', 'Codon 154', '4', '2023-05-13 17:13:22', '2023-05-13 17:13:22');
INSERT INTO genetic_characteristic_table (id_geneticcharacteristicid, genetic_characteristic_table_name, genetic_characteristic_table_display_name, genetic_characteristic_table_display_order, created, modified) VALUES ('82270054-40c2-4cfd-ba38-b82dec380821', 'genetic_codon171_table', 'Codon 171', '5', '2023-05-13 17:13:22', '2023-05-13 17:13:22');
INSERT INTO genetic_characteristic_table (id_geneticcharacteristicid, genetic_characteristic_table_name, genetic_characteristic_table_display_name, genetic_characteristic_table_display_order, created, modified) VALUES ('52fee8a5-9b2d-4df7-b872-362eb9e2bc16', 'genetic_horn_type_table', 'Horn Type', '6', '2023-05-13 17:13:22', '2023-05-13 17:13:22');
INSERT INTO genetic_characteristic_table (id_geneticcharacteristicid, genetic_characteristic_table_name, genetic_characteristic_table_display_name, genetic_characteristic_table_display_order, created, modified) VALUES ('0972486b-7b99-427e-b942-fa5ec88c2678', 'genetic_coat_color_table', 'Coat Color', '7', '2023-05-13 17:13:22', '2023-05-13 17:13:22');

INSERT INTO genetic_coat_color_table (id_geneticcoatcolorid, id_registry_id_companyid, coat_color, coat_color_abbrev, coat_color_display_order, created, modified) VALUES ('b5a2ba36-d592-48fb-bdb9-32be43bdb030', 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', 'Black', 'Blk', '1', '2023-02-26 17:02:07', '2023-02-26 17:02:07');
INSERT INTO genetic_coat_color_table (id_geneticcoatcolorid, id_registry_id_companyid, coat_color, coat_color_abbrev, coat_color_display_order, created, modified) VALUES ('d54cccca-4db8-4854-afb2-56940c3f7a32', 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', 'Red', 'Red', '2', '2023-02-26 17:02:07', '2023-02-26 17:02:07');
INSERT INTO genetic_coat_color_table (id_geneticcoatcolorid, id_registry_id_companyid, coat_color, coat_color_abbrev, coat_color_display_order, created, modified) VALUES ('0d8a5ac3-6503-4b4a-a872-4d1d7deb6b9f', 'dc9ffa44-049c-4b34-8430-61a442bbe025', 'Chocolate', 'Choc', '3', '2023-02-26 17:02:07', '2023-02-26 17:02:07');
INSERT INTO genetic_coat_color_table (id_geneticcoatcolorid, id_registry_id_companyid, coat_color, coat_color_abbrev, coat_color_display_order, created, modified) VALUES ('4cb04242-7141-473f-9ee3-5e03614c5aea', 'd2122a99-b1c1-419f-8e18-28e1112dc7a8', 'White', 'W', '4', '2023-02-26 17:02:07', '2023-02-26 17:02:07');
INSERT INTO genetic_coat_color_table (id_geneticcoatcolorid, id_registry_id_companyid, coat_color, coat_color_abbrev, coat_color_display_order, created, modified) VALUES ('3570f83f-6dea-429a-be1d-d8ebc7064dde', '3a7e2399-17fd-4a8f-af43-d66fde9e0539', 'Badger Face', 'BF', '5', '2023-04-13 07:00:51', '2023-04-13 07:00:51');
INSERT INTO genetic_coat_color_table (id_geneticcoatcolorid, id_registry_id_companyid, coat_color, coat_color_abbrev, coat_color_display_order, created, modified) VALUES ('9217b3ff-d94f-4b8b-90e9-6513fd3740e3', '3a7e2399-17fd-4a8f-af43-d66fde9e0539', 'Black', 'Blk', '1', '2023-02-26 17:02:07', '2023-02-26 17:02:07');

INSERT INTO genetic_codon112_table (id_geneticcodon112id, codon112_alleles, codon112_display_order, created, modified) VALUES ('1a03b6f9-0309-4cda-82ce-43db65c9cb49', '??', '1', '2023-05-13 18:29:00', '2023-05-13 18:29:00');
INSERT INTO genetic_codon112_table (id_geneticcodon112id, codon112_alleles, codon112_display_order, created, modified) VALUES ('cfc5fe0d-e39c-439f-8822-6b75b37ae1d1', 'M?', '2', '2023-05-13 18:29:00', '2023-05-13 18:29:00');
INSERT INTO genetic_codon112_table (id_geneticcodon112id, codon112_alleles, codon112_display_order, created, modified) VALUES ('016edaa0-ceff-49e1-8f4b-441d0aea5e22', 'MM', '3', '2023-05-13 18:29:00', '2023-05-13 18:29:00');
INSERT INTO genetic_codon112_table (id_geneticcodon112id, codon112_alleles, codon112_display_order, created, modified) VALUES ('c654e3ba-16f7-452f-813a-1b90fe15e3d1', 'MT', '4', '2023-05-13 18:29:00', '2023-05-13 18:29:00');
INSERT INTO genetic_codon112_table (id_geneticcodon112id, codon112_alleles, codon112_display_order, created, modified) VALUES ('41b65d23-fa6e-44d7-b80e-5b0490c25d72', 'T?', '5', '2023-05-13 18:29:00', '2023-05-13 18:29:00');
INSERT INTO genetic_codon112_table (id_geneticcodon112id, codon112_alleles, codon112_display_order, created, modified) VALUES ('6094af3a-5d07-4693-96eb-729cecdbe150', 'TT', '6', '2023-05-13 18:29:00', '2023-05-13 18:29:00');

INSERT INTO genetic_codon136_table (id_geneticcodon136id, codon136_alleles, codon136_display_order, created, modified) VALUES ('797d50c6-2265-40e0-915b-99559245ff38', 'AA', '1', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon136_table (id_geneticcodon136id, codon136_alleles, codon136_display_order, created, modified) VALUES ('719f52a6-fa07-4479-9284-cb3264eebe0f', 'A?', '2', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon136_table (id_geneticcodon136id, codon136_alleles, codon136_display_order, created, modified) VALUES ('cf5dce03-96e7-45e6-96c3-36551c9e7b01', 'AV', '3', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon136_table (id_geneticcodon136id, codon136_alleles, codon136_display_order, created, modified) VALUES ('6f2a588f-38ab-4b57-b3f8-862769a01c85', 'V?', '4', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon136_table (id_geneticcodon136id, codon136_alleles, codon136_display_order, created, modified) VALUES ('385820e9-3171-4836-abc7-c323f922e69c', 'VV', '5', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon136_table (id_geneticcodon136id, codon136_alleles, codon136_display_order, created, modified) VALUES ('57649339-9cbb-4c58-ac17-1f7e02c377b1', '??', '6', '2023-05-15 14:35:00', '2023-05-15 14:35:00');

INSERT INTO genetic_codon141_table (id_geneticcodon141id, codon141_alleles, codon141_display_order, created, modified) VALUES ('5121aef6-4de5-43f7-afb7-7cfcf45746e3', '??', '1', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon141_table (id_geneticcodon141id, codon141_alleles, codon141_display_order, created, modified) VALUES ('efc2380a-a897-4957-b874-1c2283a178b4', 'FF', '2', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon141_table (id_geneticcodon141id, codon141_alleles, codon141_display_order, created, modified) VALUES ('b14e669a-2bbf-4df3-a016-c092ed302fe0', 'F?', '3', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon141_table (id_geneticcodon141id, codon141_alleles, codon141_display_order, created, modified) VALUES ('d8f50e03-a24a-43bc-a413-f2dadf7515c0', 'FL', '4', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon141_table (id_geneticcodon141id, codon141_alleles, codon141_display_order, created, modified) VALUES ('5c3941d5-fe39-4f58-8c50-8a344b5e3ffa', 'L?', '5', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon141_table (id_geneticcodon141id, codon141_alleles, codon141_display_order, created, modified) VALUES ('30138deb-a385-49c4-89c9-427fc4020d0e', 'LL', '6', '2023-05-15 14:35:00', '2023-05-15 14:35:00');

INSERT INTO genetic_codon154_table (id_geneticcodon154id, codon154_alleles, codon154_display_order, created, modified) VALUES ('2a8c03f8-6736-424b-a6b0-ac9f48eaa170', '??', '1', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon154_table (id_geneticcodon154id, codon154_alleles, codon154_display_order, created, modified) VALUES ('82d79342-0dbc-4723-b8d4-c135e66541ef', 'RR', '2', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon154_table (id_geneticcodon154id, codon154_alleles, codon154_display_order, created, modified) VALUES ('01ffdede-601e-498f-8e3b-f8d905830e4c', 'RH', '3', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon154_table (id_geneticcodon154id, codon154_alleles, codon154_display_order, created, modified) VALUES ('02d66e08-a189-4127-a083-08488a081863', 'R?', '4', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon154_table (id_geneticcodon154id, codon154_alleles, codon154_display_order, created, modified) VALUES ('22a18722-d5eb-478c-bc09-943a9af41162', 'HH', '5', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon154_table (id_geneticcodon154id, codon154_alleles, codon154_display_order, created, modified) VALUES ('23b669ab-6639-4d76-87ff-f0e7d3476241', 'H?', '6', '2023-05-15 14:35:00', '2023-05-15 14:35:00');

INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('c0bcd357-50b9-4112-9c00-fa0b9e58498d', 'QQ', '1', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('713c758b-60b3-499f-9869-aa9058c0fce6', 'Q?', '5', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('084bfbf1-75c8-446e-875d-437230be3e3e', 'QR', '2', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('20016fc6-ba2c-45f7-9663-8536eb2124df', 'R?', '9', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('6fa2dd89-98da-4b0c-a62a-867fa42f2515', 'RR', '6', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('14947401-f58d-4995-b378-d62bf056c57d', '??', '15', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('eca8cd5f-907e-4842-b032-d8f4d397355e', 'HH', '10', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('63bb11a3-f22a-43fc-9996-2ed253398632', 'QH', '3', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('436e9f81-b639-4264-942c-23f0b0c43dfe', 'H?', '12', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('b664abf9-6d96-44fe-8040-5876ee2997d7', 'RH', '7', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('993d6455-11e2-48f2-a49a-782f1897d0d8', 'KK', '13', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('9a712570-30a5-4c38-9b96-37d85ea22207', 'QK', '4', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('fffd6b51-d3d7-4e9f-a12f-4071e5e406ec', 'RK', '8', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('234f4e31-e087-40d8-89f9-fb2f274e9bcd', 'K?', '14', '2023-05-15 14:35:00', '2023-05-15 14:35:00');
INSERT INTO genetic_codon171_table (id_geneticcodon171id, codon171_alleles, codon171_display_order, created, modified) VALUES ('de636060-7952-4108-8459-7e7a3bc226d4', 'HK', '11', '2023-05-15 14:35:00', '2023-05-15 14:35:00');

INSERT INTO genetic_horn_type_table (id_genetichorntypeid, id_registry_id_companyid, horn_type, horn_type_abbrev, horn_type_display_order, created, modified) VALUES ('00b5641d-85d8-46ce-8186-9eaa675290fc', 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', 'Polled', 'P', '1', '2024-04-01 09:25:42', '2024-04-01 09:25:42');
INSERT INTO genetic_horn_type_table (id_genetichorntypeid, id_registry_id_companyid, horn_type, horn_type_abbrev, horn_type_display_order, created, modified) VALUES ('c8d2302a-0c43-4362-81c5-f0ccfd3f51b4', 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', 'Horned', 'H', '2', '2024-04-01 09:25:42', '2024-04-01 09:25:42');
INSERT INTO genetic_horn_type_table (id_genetichorntypeid, id_registry_id_companyid, horn_type, horn_type_abbrev, horn_type_display_order, created, modified) VALUES ('bcb954a7-7bae-4ca1-82cf-edff05ecdfc7', 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', 'Scurred', 'S', '3', '2024-04-01 09:25:42', '2024-04-01 09:25:42');

INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('709c3963-bbe6-4524-bc9d-a0fc3b754e68', 'Yellow', 'Y', '2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('cb9dd5a1-70ab-4498-b7a2-81ba7c967d04', 'Purple', 'PR', '9', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('8b382ec8-863f-4888-a8fe-c3abbd9f214f', 'White', 'W', '1', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('c5efea22-6a25-45dc-94cf-311499d097e7', 'Mint', 'MI', '8', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('d7228b86-cb15-4e16-bec7-5ee956de9641', 'Orange', 'O', '3', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('2026103a-8309-44bd-8790-ed8b854db80c', 'Pink', 'P', '4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('70c4a2d5-939f-41e4-a744-30a5961407a7', 'Brown', 'BR', '10', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('65a26592-8619-4c50-ad6f-c9832ebfb7bd', 'Red', 'R', '5', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('7dc8e280-4589-4bcb-9b51-c5fb61c4c4ae', 'Green', 'G', '6', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('3b64185d-24cb-4fda-923e-301cc5b1e72e', 'Blue', 'B', '7', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('cb218707-71f9-425a-9cac-8eb6cb178f35', 'Black', 'BK', '12', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('fce770d2-5cec-4188-b0b9-2008a90ed552', 'Grey', 'GY', '11', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('46bcfff9-208f-49c5-b940-1ff4fe328368', 'Metal', 'M', '13', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('150a3eaf-3f93-4e98-8246-d3b26856dfb8', 'Unknown', 'U', '14', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_color_table (id_idcolorid, id_color_name, id_color_abbrev, id_color_display_order, created, modified) VALUES ('066e24f7-a1ef-4c7e-b388-f9b0223c8053', 'Not Applicable', 'NA', '15', '2023-09-17 10:26:51', '2023-09-17 10:26:51');

INSERT INTO id_location_table (id_idlocationid, id_location_name, id_location_abbrev, id_location_display_order, created, modified) VALUES ('7969cccf-0c6f-49ab-ba1d-9a9b7f2012b1', 'Right Ear', 'R', '1', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_location_table (id_idlocationid, id_location_name, id_location_abbrev, id_location_display_order, created, modified) VALUES ('107da4a3-fec3-4ba3-97ae-898b387bfc7f', 'Left Ear', 'L', '2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_location_table (id_idlocationid, id_location_name, id_location_abbrev, id_location_display_order, created, modified) VALUES ('59ef6b4a-8182-4539-9771-00e1a7cd7ccc', 'Right Flank', 'RF', '3', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_location_table (id_idlocationid, id_location_name, id_location_abbrev, id_location_display_order, created, modified) VALUES ('dea3e82e-3f09-4ed6-8d1b-7ecb6a5661ef', 'Left Flank', 'LF', '4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_location_table (id_idlocationid, id_location_name, id_location_abbrev, id_location_display_order, created, modified) VALUES ('2e4170b9-eacc-433c-bb58-897db91051cc', 'Side', 'S', '5', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_location_table (id_idlocationid, id_location_name, id_location_abbrev, id_location_display_order, created, modified) VALUES ('f022b548-7daf-470a-9299-a61be69d059e', 'Unknown', 'U', '7', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_location_table (id_idlocationid, id_location_name, id_location_abbrev, id_location_display_order, created, modified) VALUES ('243da604-5341-4a94-9db0-7f1323630e23', 'Tail Web', 'TW', '6', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_location_table (id_idlocationid, id_location_name, id_location_abbrev, id_location_display_order, created, modified) VALUES ('fdd37ecc-d8d0-410a-ab82-32825c85e699', 'Right Neck', 'RN', '8', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_location_table (id_idlocationid, id_location_name, id_location_abbrev, id_location_display_order, created, modified) VALUES ('c6e2d2eb-2c22-45bc-8a18-830f22813629', 'Left Neck', 'LN', '9', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_location_table (id_idlocationid, id_location_name, id_location_abbrev, id_location_display_order, created, modified) VALUES ('8491e41b-60d5-49c4-a80e-025cdaa67f46', 'Neck', 'N', '10', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_location_table (id_idlocationid, id_location_name, id_location_abbrev, id_location_display_order, created, modified) VALUES ('95957011-64cd-4b10-970d-3f6d52cdf7f8', 'Not Applicable', 'NA', '11', '2024-08-24 11:36:27', '2024-08-24 11:36:27');

INSERT INTO id_remove_reason_table (id_idremovereasonid, id_remove_reason, id_remove_reason_display_order, created, modified) VALUES ('be98c69d-e7c5-4b0b-b2c3-d2bed29a9362', 'Lost', '1', '2023-03-14 14:49:12', '2023-03-14 14:49:12');
INSERT INTO id_remove_reason_table (id_idremovereasonid, id_remove_reason, id_remove_reason_display_order, created, modified) VALUES ('668b9bdf-77ac-4f44-a37a-fd37e0e3c57f', 'Unreadable', '2', '2023-03-14 14:49:12', '2023-03-14 14:49:12');
INSERT INTO id_remove_reason_table (id_idremovereasonid, id_remove_reason, id_remove_reason_display_order, created, modified) VALUES ('d8f979f2-1c2f-4f96-ab65-4b22408d06ea', 'Infection', '3', '2023-03-14 14:49:12', '2023-03-14 14:49:12');
INSERT INTO id_remove_reason_table (id_idremovereasonid, id_remove_reason, id_remove_reason_display_order, created, modified) VALUES ('b757f6c8-a172-42e3-9be2-2b58bc3b9929', 'Malfunction', '4', '2023-03-14 14:49:12', '2023-03-14 14:49:12');
INSERT INTO id_remove_reason_table (id_idremovereasonid, id_remove_reason, id_remove_reason_display_order, created, modified) VALUES ('62de9aea-807d-4646-80a0-e9cd8cd0f397', 'Incompatible with Management System', '5', '2023-03-14 14:49:12', '2023-03-14 14:49:12');
INSERT INTO id_remove_reason_table (id_idremovereasonid, id_remove_reason, id_remove_reason_display_order, created, modified) VALUES ('abe06f29-4250-460f-84e4-28ee8e7fcc3e', 'Wore Off', '6', '2023-03-14 14:49:12', '2023-03-14 14:49:12');
INSERT INTO id_remove_reason_table (id_idremovereasonid, id_remove_reason, id_remove_reason_display_order, created, modified) VALUES ('1e548000-988d-4d33-9671-afd219590895', 'Replaced', '7', '2023-03-14 14:49:12', '2023-03-14 14:49:12');
INSERT INTO id_remove_reason_table (id_idremovereasonid, id_remove_reason, id_remove_reason_display_order, created, modified) VALUES ('8658866c-2e00-4367-aea4-9a3998066cc0', 'Correct Tag Data', '8', '2023-03-14 14:49:12', '2023-03-14 14:49:12');

INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('d13a17a0-33c9-4590-850c-63a07d504993', 'Federal Scrapie', 'Fed', '4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('50f1c64f-e56e-420e-8150-9347fe51c0c1', 'Electronic', 'EID', '1', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('7255aaf5-b62f-4ff9-a7b3-78cf28e2b0e2', 'Paint', 'Pnt', '6', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('6af3845e-0abc-4afa-bcb4-4eea96f2ecc2', 'Farm', 'Farm', '2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('1f66a23f-8d3e-4a3c-8df5-6ba185df5a04', 'Tattoo', 'Tat', '9', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('e63c8c31-d186-4220-9756-9630a95773bc', 'Split', 'Sp', '8', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('175ebcc9-b142-438d-8e2c-474b0945ea8d', 'Notch', 'Nch', '7', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('1e57ea5b-b388-4b64-8d75-2ed87be998bd', 'Name', 'Name', '5', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('e3c57ebe-6310-448f-9327-8eb0412bf0cc', 'Freeze Brand', 'Fz', '10', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('59bef26c-ee6b-4dfa-9068-7fd45588fb75', 'Trich', 'Tr', '3', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('98cc5d2e-b8cc-4b6f-a051-fb5078ee9ce5', 'NUES', 'NUES', '11', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('cd6e881f-b6e9-4339-816d-a8ed1a3be1c9', 'Sale Order', 'Sale', '12', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('d0c59f03-78ee-4c11-9cb2-232406d1ecaf', 'Bangs', 'Bangs', '13', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('d8519b7a-be8d-4151-b0eb-6740ff015089', 'Unknown', 'Unk', '14', '2023-07-05 14:14:55', '2023-07-05 14:14:55');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('9e5a4712-224b-4cff-ae07-c6b8d6ef7b3f', 'Carcass Lot', 'Carcass', '15', '2023-07-05 14:14:55', '2023-07-05 14:14:55');
INSERT INTO id_type_table (id_idtypeid, id_type_name, id_type_abbrev, id_type_display_order, created, modified) VALUES ('3d2795d9-d0bf-46e6-8cef-3ff23bd0a7e1', 'Canadian Federal', 'Can Fed', '16', '2023-10-14 13:18:00', '2023-10-14 13:18:00');

INSERT INTO inbreeding_calculation_type_table (id_inbreedingcalculationtypeid, inbreeding_calculation_type, inbreeding_calculation_type_display_order, created, modified) VALUES ('7aeedae6-f152-4242-9349-a63734895361', '8 Generation Path', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO inbreeding_calculation_type_table (id_inbreedingcalculationtypeid, inbreeding_calculation_type, inbreeding_calculation_type_display_order, created, modified) VALUES ('42e4986c-9975-4344-93af-6c81ba096aa8', '8 Generation Wright', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO inbreeding_calculation_type_table (id_inbreedingcalculationtypeid, inbreeding_calculation_type, inbreeding_calculation_type_display_order, created, modified) VALUES ('70fc1d3f-0401-4377-818b-1bdae1a61f99', 'Full Path', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO inbreeding_calculation_type_table (id_inbreedingcalculationtypeid, inbreeding_calculation_type, inbreeding_calculation_type_display_order, created, modified) VALUES ('654c4b17-d3d1-4adf-a776-9ea5cb096e7b', 'Genomic Inbreeding', '4', '2024-02-18 14:37:57', '2024-02-18 14:37:57');

INSERT INTO phone_type_table (id_phonetypeid, phone_type, phone_type_display_order, created, modified) VALUES ('691ecec8-75dc-426a-af38-8666efe22409', 'Primary', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO phone_type_table (id_phonetypeid, phone_type, phone_type_display_order, created, modified) VALUES ('449b7c89-452c-4561-9b5c-2ce1604e9530', 'Work', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO phone_type_table (id_phonetypeid, phone_type, phone_type_display_order, created, modified) VALUES ('fbf00605-71cd-415f-8a20-0a8247fb718f', 'Home', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO phone_type_table (id_phonetypeid, phone_type, phone_type_display_order, created, modified) VALUES ('34c7d72a-993c-4147-bbe0-36235d925442', 'Mobile', '4', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO phone_type_table (id_phonetypeid, phone_type, phone_type_display_order, created, modified) VALUES ('9dd9b963-ca07-40d7-b942-c82d4b18ae05', 'Home Fax', '5', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO phone_type_table (id_phonetypeid, phone_type, phone_type_display_order, created, modified) VALUES ('46a9bd26-3015-4bae-8d01-3c3640238c43', 'Work Fax', '6', '2024-02-18 14:37:57', '2024-02-18 14:37:57');

INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('30d1290f-f7f9-4fa0-99b9-e2f9378b77d7', 'Behavior Good',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '5', '2023-02-24 10:48:04', '2023-02-24 10:48:04');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('375e8362-b53b-4ddb-92dd-5a071cb4c647', 'Behavior Bad',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '4', '2023-02-24 10:48:04', '2023-02-24 10:48:04');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('c121adae-2e80-4d77-81cb-0c41be09be8f', 'Horn Saw Right',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '36', '2023-02-24 10:48:04', '2023-02-24 10:48:04');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('ec49680e-b3f1-432e-93ef-3206e44c3a61', 'Horn Saw Left',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '35', '2023-02-24 10:48:04', '2023-02-24 10:48:04');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('982b1a17-394d-4db0-b2d0-2b9b78fec148', 'Horn Bad Right',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '33', '2023-02-24 10:48:04', '2023-02-24 10:48:04');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('2d843148-bb71-494f-8845-5bbe91112848', 'Horn Bad Left',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '32', '2023-02-24 10:48:04', '2023-02-24 10:48:04');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('8d0b96bc-fe8e-41db-b94b-76f54f2b5bc3', 'Horn Buds',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '34', '2023-02-24 10:48:04', '2023-02-24 10:48:04');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('d09cdc60-ec42-4aa8-8e9a-31e1e717ebfe', 'Trim Hooves',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '52', '2023-02-24 10:48:04', '2023-02-24 10:48:04');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('d99a7bef-ba7d-4b36-bdb6-149766d62b6e', 'Shorn',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '51', '2023-02-24 10:48:04', '2023-02-24 10:48:04');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('b6d6f521-c15c-443e-a62e-84ec8eea4b54', 'Horns Good',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '38', '2023-02-24 10:48:04', '2023-02-24 10:48:04');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('0fd32af5-31f7-4858-b557-8dac871026de', 'Horns Bad',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '37', '2023-02-24 10:48:04', '2023-02-24 10:48:04');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('c6369416-cf40-4bdc-9a1e-604ff40c5f1c', 'Shod',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '152', '2024-09-22 09:32:02', '2024-09-22 09:32:02');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('b594b66f-8694-424d-8292-e13f7074f29b', 'Trim Left Front Hoof',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '153', '2024-09-22 09:32:02', '2024-09-22 09:32:02');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('cc556913-6be8-4667-92d4-4ca45ff5e27b', 'Trim Right Front Hoof',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '154', '2024-09-22 09:32:02', '2024-09-22 09:32:02');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('ddf459a2-0537-49e8-9745-8548b83a9da7', 'Trim Left Hind Hoof',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '155', '2024-09-22 09:32:02', '2024-09-22 09:32:02');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('bfb7ebd1-321d-4bdb-a66f-5814b94180aa', 'Trim Right Hind Hoof',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '156', '2024-09-22 09:32:02', '2024-09-22 09:32:02');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('a0783def-fa45-47a9-bf6b-845d646aae06', 'Weaned',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '157', '2024-09-30 21:22:48', '2024-09-30 21:22:48');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('1d8e1cea-6551-4c56-b5ee-ce856816807a', 'Foot Scald all hooves',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '158', '2024-10-30 12:16:29', '2024-10-30 12:16:29');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('3f33be5c-2878-4f33-a150-170802096e24', 'Foot Scald  Left Front Hoof',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '159', '2024-10-30 12:16:29', '2024-10-30 12:16:29');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('29b7fef9-4488-4963-8540-cb8f379795d6', 'Foot Scald  Right Front Hoof',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '160', '2024-10-30 12:16:29', '2024-10-30 12:16:29');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('84c3c550-c7a1-4e42-b4ea-d7ddd4e0e035', 'Foot Scald  Left Hind Hoof',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '161', '2024-10-30 12:16:29', '2024-10-30 12:16:29');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('87af910a-2d1e-4318-b5fe-b18d7c3ac053', 'Foot Scald Right Hind Hoof',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '162', '2024-10-30 12:16:29', '2024-10-30 12:16:29');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('d6909e5e-3194-4736-8833-6ea66f700ded', 'Foot Rot all hooves',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '163', '2024-10-30 12:16:29', '2024-10-30 12:16:29');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('1ee1b5b4-f433-49cb-a69c-59524e7f0206', 'Foot Rot Left Front Hoof',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '164', '2024-10-30 12:16:29', '2024-10-30 12:16:29');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('4f695fb4-3de7-4903-b544-92a90a249534', 'Foot Rot Right Front Hoof',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '165', '2024-10-30 12:16:29', '2024-10-30 12:16:29');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('884e8b70-1f67-4123-a191-1476eb61b7c5', 'Foot Rot Left Hind Hoof',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '166', '2024-10-30 12:16:29', '2024-10-30 12:16:29');
INSERT INTO predefined_notes_table (id_predefinednotesid, predefined_note_text, id_contactid, id_companyid, predefined_note_display_order, created, modified) VALUES ('7a4212fa-34b4-40df-b083-21e21a578c12', 'Foot Rot Right Hind Hoof',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '167', '2024-10-30 12:16:29', '2024-10-30 12:16:29');

INSERT INTO premise_jurisdiction_table (id_premisejurisdictionid, premise_jurisdiction, premise_jurisdiction_display_order, created, modified) VALUES ('7982a3e1-cbd1-46ac-a74c-1c5d0a08e918', 'State', '1', '2023-03-10 17:32:47', '2023-03-10 17:32:47');
INSERT INTO premise_jurisdiction_table (id_premisejurisdictionid, premise_jurisdiction, premise_jurisdiction_display_order, created, modified) VALUES ('c351ae2d-a6e8-4829-9e0e-9ffe913fb25f', 'US Federal', '2', '2023-03-10 17:32:47', '2023-03-10 17:32:47');
INSERT INTO premise_jurisdiction_table (id_premisejurisdictionid, premise_jurisdiction, premise_jurisdiction_display_order, created, modified) VALUES ('b7e32f97-8e2c-4962-acc2-a4ba156bca5f', 'Canada Federal', '3', '2023-12-11 08:52:31', '2023-12-11 08:52:31');

INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('5212bfa2-2a75-47c7-9a04-975dc5d0cec2', '28e2b327-5ced-4f39-bc60-90450bc719c6', 'c351ae2d-a6e8-4829-9e0e-9ffe913fb25f', '00BTHWG', 'b2d6531b-9aab-4ebb-b794-766d25cd6668', '38.89631', '-107.5762', '16870 Garvin Mesa Rd.',NULL, 'Paonia', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '81428', 'd27cd52c-d066-4338-86d1-3252b6a2c132', 'b41f2392-2517-459d-b074-464d6915fafa', '2023-02-28 10:48:22', '2023-02-28 10:48:22');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('ea386787-b020-4b62-bb82-d5606d953834', '25ddcb3e-10a5-4afc-b4a0-4d1fafe8a7f2',NULL,NULL,NULL,NULL,NULL, '001-A East Harmony Rd', 'Number 522', 'Ft. Collins', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '80525', '82a984fa-855d-4864-a77e-3c87449d5653', 'b41f2392-2517-459d-b074-464d6915fafa', '2023-02-24 22:00:04', '2023-02-24 22:00:04');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('06a2f695-5c34-41d3-b20c-49386749119b', '28e2b327-5ced-4f39-bc60-90450bc719c6',NULL,NULL,NULL,NULL,NULL, '2450 Gilette Drive',NULL, 'Fort Collins', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '80526',NULL, 'b41f2392-2517-459d-b074-464d6915fafa', '2024-02-20 10:53:12', '2024-02-20 10:53:12');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('1784dc32-b1c7-47a4-a3cf-15f41bd5cb13', '28e2b327-5ced-4f39-bc60-90450bc719c6',NULL,NULL,NULL,NULL,NULL, '27847 Road 21',NULL, 'Rocky Ford', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '81067',NULL, 'b41f2392-2517-459d-b074-464d6915fafa', '2024-02-20 10:53:12', '2024-02-20 10:53:12');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('2e2c2107-87ca-4287-9c97-a38a53eb7a78', '28e2b327-5ced-4f39-bc60-90450bc719c6',NULL,NULL,NULL,NULL,NULL, '3164 B 1/2 Road',NULL, 'Grand Junction', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '81503',NULL, 'b41f2392-2517-459d-b074-464d6915fafa', '2024-02-20 10:53:12', '2024-02-20 10:53:12');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('e50d7be2-79af-41ea-b88a-17ced6a87087', '28e2b327-5ced-4f39-bc60-90450bc719c6',NULL,NULL,NULL,NULL,NULL, '1175 58th Ave.', 'Suite 100', 'Greeley', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '80634',NULL, 'b41f2392-2517-459d-b074-464d6915fafa', '2024-02-20 10:53:12', '2024-02-20 10:53:12');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('6cffd0ba-642c-47d0-8d55-2dbc2a2fcc67', '28e2b327-5ced-4f39-bc60-90450bc719c6',NULL,NULL,NULL,NULL,NULL, '2530 River Plaza Drive', 'Suite 200', 'Sacramento', '3f432960-e734-4f4e-8fe5-382383233aa2', '95833',NULL, 'b41f2392-2517-459d-b074-464d6915fafa', '2024-02-20 10:53:12', '2024-02-20 10:53:12');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('7f69ed4d-dd08-42db-b9e2-5bfd904ac4ab', '28e2b327-5ced-4f39-bc60-90450bc719c6',NULL,NULL,NULL,NULL,NULL, '300 S. Technology Ct.',NULL, 'Broomfield', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '80021',NULL, 'b41f2392-2517-459d-b074-464d6915fafa', '2024-02-20 10:53:12', '2024-02-20 10:53:12');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('f85f9438-02c5-4845-9c7a-37f2d53c0e75', '28e2b327-5ced-4f39-bc60-90450bc719c6',NULL,NULL,NULL,NULL,NULL, 'Attn: Sample Processing Department', '1920 Dayton Ave.', 'Ames', '6e42c9cc-3757-4c02-99f2-8ad8b630602c', '50010',NULL, 'b41f2392-2517-459d-b074-464d6915fafa', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('80a5f0ac-de36-4eae-8b6c-a5bbb3cf01c2', '28e2b327-5ced-4f39-bc60-90450bc719c6',NULL,NULL,NULL,NULL,NULL, '1800 Christensen Drive',NULL, 'Ames', '6e42c9cc-3757-4c02-99f2-8ad8b630602c', '50011',NULL, 'b41f2392-2517-459d-b074-464d6915fafa', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('1687e6c7-83bd-4465-bf1b-79019f709807', '28e2b327-5ced-4f39-bc60-90450bc719c6',NULL,NULL,NULL,NULL,NULL, '1909 Skip Bertman Drive', 'Pathological Sciences Department', 'Baton Rouge', 'c67b8ae1-3482-4100-bd25-93f0364d23b2', '70803',NULL, 'b41f2392-2517-459d-b074-464d6915fafa', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('f55e0fc8-26af-45db-8078-92d0256e846c', '28e2b327-5ced-4f39-bc60-90450bc719c6',NULL,NULL,NULL,NULL,NULL, 'C/O David Scales', '"474 S. Shaw Lane Rm. 1290 Anthony Hall','East Lansing', '4035f757-84cf-4d4e-8ad6-f77741808780', '48824',NULL, 'b41f2392-2517-459d-b074-464d6915fafa', '2024-09-30 14:08:10', '2024-09-30 14:08:10');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('a03d6eb9-8dc0-4de6-81ea-f12593d1f1a0', '25ddcb3e-10a5-4afc-b4a0-4d1fafe8a7f2',NULL,NULL,NULL,NULL,NULL, '1766 Littlewick Dr',NULL, 'Windsor', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '80550', '89c517ef-f0b8-44e4-bde5-50d66be90b1b', 'b41f2392-2517-459d-b074-464d6915fafa', '2024-12-15 08:25:24', '2024-12-15 08:25:24');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('eb932824-53a9-4d07-b24d-3900b66cde70', '25ddcb3e-10a5-4afc-b4a0-4d1fafe8a7f2',NULL,NULL,NULL,NULL,NULL, 'PO Box 534',NULL, 'Paonia', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', '81428', 'd27cd52c-d066-4338-86d1-3252b6a2c132', 'b41f2392-2517-459d-b074-464d6915fafa', '2024-12-15 08:25:24', '2024-12-15 08:25:24');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('8f6adf49-cf66-4cb5-affd-135c4dd67189', '28e2b327-5ced-4f39-bc60-90450bc719c6',NULL,NULL,NULL,NULL,NULL, '551 Gem Lane',NULL, 'Ramona', '3f432960-e734-4f4e-8fe5-382383233aa2', '92065',NULL, 'b41f2392-2517-459d-b074-464d6915fafa', '2023-02-24 22:00:04', '2023-02-24 22:00:04');
INSERT INTO premise_table (id_premiseid, id_premisetypeid, id_premisejurisdictionid, premise_number, premise_lat_long_id_unitsid, premise_latitude, premise_longitude, premise_address1, premise_address2, premise_city, premise_id_stateid, premise_postcode, premise_id_countyid, premise_id_countryid, created, modified) VALUES ('4a04fcfe-889f-4cd9-8abe-bdd59807ad07', '28e2b327-5ced-4f39-bc60-90450bc719c6',NULL,NULL,NULL,NULL,NULL, 'Unknown',NULL, 'Unknown', 'c06bee74-8e98-48f0-9a29-83fd1a76e2fd', 'Unknown',NULL, 'b41f2392-2517-459d-b074-464d6915fafa', '2023-02-24 22:00:04', '2023-02-24 22:00:04');

INSERT INTO premise_type_table (id_premisetypeid, premise_type, premise_display_order, created, modified) VALUES ('7f58eb72-40bc-4dde-a929-611944eb291d', 'Physical', '1', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO premise_type_table (id_premisetypeid, premise_type, premise_display_order, created, modified) VALUES ('25ddcb3e-10a5-4afc-b4a0-4d1fafe8a7f2', 'Mailing', '2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO premise_type_table (id_premisetypeid, premise_type, premise_display_order, created, modified) VALUES ('28e2b327-5ced-4f39-bc60-90450bc719c6', 'Both', '3', '2023-09-17 10:26:51', '2023-09-17 10:26:51');

INSERT INTO saved_evaluations_table (id_savedevaluationsid, evaluation_name, saved_evaluation_id_contactid, saved_evaluation_id_companyid, trait_name01, trait_name02, trait_name03, trait_name04, trait_name05, trait_name06, trait_name07, trait_name08, trait_name09, trait_name10, trait_name11, trait_name12, trait_name13, trait_name14, trait_name15, trait_units11, trait_units12, trait_units13, trait_units14, trait_units15, trait_name16, trait_name17, trait_name18, trait_name19, trait_name20, trait_name01_deferred, trait_name02_deferred, trait_name03_deferred, trait_name04_deferred, trait_name05_deferred, trait_name06_deferred, trait_name07_deferred, trait_name08_deferred, trait_name09_deferred, trait_name10_deferred, trait_name11_deferred, trait_name12_deferred, trait_name13_deferred, trait_name14_deferred, trait_name15_deferred, trait_name16_deferred, trait_name17_deferred, trait_name18_deferred, trait_name19_deferred, trait_name20_deferred, trait_name01_optional, trait_name02_optional, trait_name03_optional, trait_name04_optional, trait_name05_optional, trait_name06_optional, trait_name07_optional, trait_name08_optional, trait_name09_optional, trait_name10_optional, trait_name11_optional, trait_name12_optional, trait_name13_optional, trait_name14_optional, trait_name15_optional, trait_name16_optional, trait_name17_optional, trait_name18_optional, trait_name19_optional, trait_name20_optional, add_alert_summary, created, modified, is_system_only) VALUES ('f5efab5f-d4e1-48dc-b122-5e2ef7fc36f9', 'Simple Lambing',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', 'bd0925b6-bf1d-4ac8-a275-7ba6da122d41', '923ee982-d1b4-4968-a9f0-9aed7126abc2', '968eae00-3c84-43bd-ab91-ce19246c2f79', '601f910c-33fa-4894-87ae-9eff9f7d4b9e', '5f6f1a22-756c-4b12-8be3-4e2fd7d17fb5', '8644a896-c70c-463e-a3bd-12e7ae0e0a38', '2b5e739c-06c8-4e24-915f-ba2484ffaa8e',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2024-03-09 14:01:27', '2024-03-09 14:01:27', '1');
INSERT INTO saved_evaluations_table (id_savedevaluationsid, evaluation_name, saved_evaluation_id_contactid, saved_evaluation_id_companyid, trait_name01, trait_name02, trait_name03, trait_name04, trait_name05, trait_name06, trait_name07, trait_name08, trait_name09, trait_name10, trait_name11, trait_name12, trait_name13, trait_name14, trait_name15, trait_units11, trait_units12, trait_units13, trait_units14, trait_units15, trait_name16, trait_name17, trait_name18, trait_name19, trait_name20, trait_name01_deferred, trait_name02_deferred, trait_name03_deferred, trait_name04_deferred, trait_name05_deferred, trait_name06_deferred, trait_name07_deferred, trait_name08_deferred, trait_name09_deferred, trait_name10_deferred, trait_name11_deferred, trait_name12_deferred, trait_name13_deferred, trait_name14_deferred, trait_name15_deferred, trait_name16_deferred, trait_name17_deferred, trait_name18_deferred, trait_name19_deferred, trait_name20_deferred, trait_name01_optional, trait_name02_optional, trait_name03_optional, trait_name04_optional, trait_name05_optional, trait_name06_optional, trait_name07_optional, trait_name08_optional, trait_name09_optional, trait_name10_optional, trait_name11_optional, trait_name12_optional, trait_name13_optional, trait_name14_optional, trait_name15_optional, trait_name16_optional, trait_name17_optional, trait_name18_optional, trait_name19_optional, trait_name20_optional, add_alert_summary, created, modified, is_system_only) VALUES ('01040d27-4019-4e30-8149-dc22c070980e', 'Simple Sort',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, 'a11275e7-0eeb-4e9f-b7dd-2adbb361ccfb',NULL,NULL,NULL,NULL, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '2024-03-09 14:01:27', '2024-03-09 14:01:27', '1');
INSERT INTO saved_evaluations_table (id_savedevaluationsid, evaluation_name, saved_evaluation_id_contactid, saved_evaluation_id_companyid, trait_name01, trait_name02, trait_name03, trait_name04, trait_name05, trait_name06, trait_name07, trait_name08, trait_name09, trait_name10, trait_name11, trait_name12, trait_name13, trait_name14, trait_name15, trait_units11, trait_units12, trait_units13, trait_units14, trait_units15, trait_name16, trait_name17, trait_name18, trait_name19, trait_name20, trait_name01_deferred, trait_name02_deferred, trait_name03_deferred, trait_name04_deferred, trait_name05_deferred, trait_name06_deferred, trait_name07_deferred, trait_name08_deferred, trait_name09_deferred, trait_name10_deferred, trait_name11_deferred, trait_name12_deferred, trait_name13_deferred, trait_name14_deferred, trait_name15_deferred, trait_name16_deferred, trait_name17_deferred, trait_name18_deferred, trait_name19_deferred, trait_name20_deferred, trait_name01_optional, trait_name02_optional, trait_name03_optional, trait_name04_optional, trait_name05_optional, trait_name06_optional, trait_name07_optional, trait_name08_optional, trait_name09_optional, trait_name10_optional, trait_name11_optional, trait_name12_optional, trait_name13_optional, trait_name14_optional, trait_name15_optional, trait_name16_optional, trait_name17_optional, trait_name18_optional, trait_name19_optional, trait_name20_optional, add_alert_summary, created, modified, is_system_only) VALUES ('b1f535e0-ab49-4497-8784-7fefc9c2a0a6', 'Male Breeding Soundness',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, 'be459431-f1b7-4c99-a044-d3baff3e3c46', 'fa366b50-1980-409b-9934-f92bc1ffdead', '4e462d47-c7a3-427c-a424-f9271918ac16', '44d307ab-5c32-44c7-bb06-e65c11269716',NULL, '9f2b567e-7458-467f-b90c-8494bdfc9d11', '02cb78b3-9bff-41df-a579-ae744f339389', '6a17ca51-1d0c-4961-a68b-1e49298d0655', 'fe4678ef-c2a0-470e-950b-b25088867ed6',NULL, 'd8127b75-6dfe-49c5-9cef-77d79d44e07d',NULL,NULL,NULL,NULL, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '2024-03-09 14:01:27', '2024-03-09 14:01:27', '1');
INSERT INTO saved_evaluations_table (id_savedevaluationsid, evaluation_name, saved_evaluation_id_contactid, saved_evaluation_id_companyid, trait_name01, trait_name02, trait_name03, trait_name04, trait_name05, trait_name06, trait_name07, trait_name08, trait_name09, trait_name10, trait_name11, trait_name12, trait_name13, trait_name14, trait_name15, trait_units11, trait_units12, trait_units13, trait_units14, trait_units15, trait_name16, trait_name17, trait_name18, trait_name19, trait_name20, trait_name01_deferred, trait_name02_deferred, trait_name03_deferred, trait_name04_deferred, trait_name05_deferred, trait_name06_deferred, trait_name07_deferred, trait_name08_deferred, trait_name09_deferred, trait_name10_deferred, trait_name11_deferred, trait_name12_deferred, trait_name13_deferred, trait_name14_deferred, trait_name15_deferred, trait_name16_deferred, trait_name17_deferred, trait_name18_deferred, trait_name19_deferred, trait_name20_deferred, trait_name01_optional, trait_name02_optional, trait_name03_optional, trait_name04_optional, trait_name05_optional, trait_name06_optional, trait_name07_optional, trait_name08_optional, trait_name09_optional, trait_name10_optional, trait_name11_optional, trait_name12_optional, trait_name13_optional, trait_name14_optional, trait_name15_optional, trait_name16_optional, trait_name17_optional, trait_name18_optional, trait_name19_optional, trait_name20_optional, add_alert_summary, created, modified, is_system_only) VALUES ('e709cb74-f72f-45ff-9dcb-7abaf2bd66b3', 'Optimal Livestock Ram Test',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, 'be459431-f1b7-4c99-a044-d3baff3e3c46', 'fa366b50-1980-409b-9934-f92bc1ffdead', '4e462d47-c7a3-427c-a424-f9271918ac16', 'b5865a35-f213-4b8a-9077-5472cd8a25b3',NULL, '9f2b567e-7458-467f-b90c-8494bdfc9d11', '02cb78b3-9bff-41df-a579-ae744f339389', '6a17ca51-1d0c-4961-a68b-1e49298d0655', '38907ea4-e6a5-4e64-bf6d-6fcb0dc028d1',NULL, '2d79d6c3-a096-40db-ae4a-2dfa18d05482', '0c96e6f7-eb2e-406f-b639-d7d505f7ee3a',NULL,NULL,NULL, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '2024-03-09 14:01:27', '2024-03-09 14:01:27', '1');
INSERT INTO saved_evaluations_table (id_savedevaluationsid, evaluation_name, saved_evaluation_id_contactid, saved_evaluation_id_companyid, trait_name01, trait_name02, trait_name03, trait_name04, trait_name05, trait_name06, trait_name07, trait_name08, trait_name09, trait_name10, trait_name11, trait_name12, trait_name13, trait_name14, trait_name15, trait_units11, trait_units12, trait_units13, trait_units14, trait_units15, trait_name16, trait_name17, trait_name18, trait_name19, trait_name20, trait_name01_deferred, trait_name02_deferred, trait_name03_deferred, trait_name04_deferred, trait_name05_deferred, trait_name06_deferred, trait_name07_deferred, trait_name08_deferred, trait_name09_deferred, trait_name10_deferred, trait_name11_deferred, trait_name12_deferred, trait_name13_deferred, trait_name14_deferred, trait_name15_deferred, trait_name16_deferred, trait_name17_deferred, trait_name18_deferred, trait_name19_deferred, trait_name20_deferred, trait_name01_optional, trait_name02_optional, trait_name03_optional, trait_name04_optional, trait_name05_optional, trait_name06_optional, trait_name07_optional, trait_name08_optional, trait_name09_optional, trait_name10_optional, trait_name11_optional, trait_name12_optional, trait_name13_optional, trait_name14_optional, trait_name15_optional, trait_name16_optional, trait_name17_optional, trait_name18_optional, trait_name19_optional, trait_name20_optional, add_alert_summary, created, modified, is_system_only) VALUES ('129b3b03-2de2-4200-8480-b5df6f3cc8ff', 'Optimal Livestock Ewe Ultrasound',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '2046720b-c03b-4640-8e57-2eaf31e05b53',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '3cf30d5f-8c59-4a44-8b0c-3fb162718532', '81498ee6-d640-4fa3-a0ee-6820708bc4a3',NULL,NULL,NULL, 'ffabfa21-ef36-4144-b5c6-58c8e369b1a7', 'ffabfa21-ef36-4144-b5c6-58c8e369b1a7',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '1', '2024-03-09 14:01:27', '2024-03-09 14:01:27', '1');
INSERT INTO saved_evaluations_table (id_savedevaluationsid, evaluation_name, saved_evaluation_id_contactid, saved_evaluation_id_companyid, trait_name01, trait_name02, trait_name03, trait_name04, trait_name05, trait_name06, trait_name07, trait_name08, trait_name09, trait_name10, trait_name11, trait_name12, trait_name13, trait_name14, trait_name15, trait_units11, trait_units12, trait_units13, trait_units14, trait_units15, trait_name16, trait_name17, trait_name18, trait_name19, trait_name20, trait_name01_deferred, trait_name02_deferred, trait_name03_deferred, trait_name04_deferred, trait_name05_deferred, trait_name06_deferred, trait_name07_deferred, trait_name08_deferred, trait_name09_deferred, trait_name10_deferred, trait_name11_deferred, trait_name12_deferred, trait_name13_deferred, trait_name14_deferred, trait_name15_deferred, trait_name16_deferred, trait_name17_deferred, trait_name18_deferred, trait_name19_deferred, trait_name20_deferred, trait_name01_optional, trait_name02_optional, trait_name03_optional, trait_name04_optional, trait_name05_optional, trait_name06_optional, trait_name07_optional, trait_name08_optional, trait_name09_optional, trait_name10_optional, trait_name11_optional, trait_name12_optional, trait_name13_optional, trait_name14_optional, trait_name15_optional, trait_name16_optional, trait_name17_optional, trait_name18_optional, trait_name19_optional, trait_name20_optional, add_alert_summary, created, modified, is_system_only) VALUES ('a42e39e3-c892-4783-b5bc-2665f1cc8d2c', 'Suck Reflex',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, 'c00505e5-9d8b-48d6-b6fd-84cdb3c65127',NULL,NULL,NULL,NULL, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2024-03-09 14:01:27', '2024-03-09 14:01:27', '1');
INSERT INTO saved_evaluations_table (id_savedevaluationsid, evaluation_name, saved_evaluation_id_contactid, saved_evaluation_id_companyid, trait_name01, trait_name02, trait_name03, trait_name04, trait_name05, trait_name06, trait_name07, trait_name08, trait_name09, trait_name10, trait_name11, trait_name12, trait_name13, trait_name14, trait_name15, trait_units11, trait_units12, trait_units13, trait_units14, trait_units15, trait_name16, trait_name17, trait_name18, trait_name19, trait_name20, trait_name01_deferred, trait_name02_deferred, trait_name03_deferred, trait_name04_deferred, trait_name05_deferred, trait_name06_deferred, trait_name07_deferred, trait_name08_deferred, trait_name09_deferred, trait_name10_deferred, trait_name11_deferred, trait_name12_deferred, trait_name13_deferred, trait_name14_deferred, trait_name15_deferred, trait_name16_deferred, trait_name17_deferred, trait_name18_deferred, trait_name19_deferred, trait_name20_deferred, trait_name01_optional, trait_name02_optional, trait_name03_optional, trait_name04_optional, trait_name05_optional, trait_name06_optional, trait_name07_optional, trait_name08_optional, trait_name09_optional, trait_name10_optional, trait_name11_optional, trait_name12_optional, trait_name13_optional, trait_name14_optional, trait_name15_optional, trait_name16_optional, trait_name17_optional, trait_name18_optional, trait_name19_optional, trait_name20_optional, add_alert_summary, created, modified, is_system_only) VALUES ('2402f686-9be9-4247-b925-e37c36906007', 'Simple Births',NULL, 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', 'e49cb696-41ec-462e-9bd1-7ed0e1960d79', '391b2b69-4976-426d-ba3b-2911bc5a8f2a', '7065f88c-ee13-4682-8795-c9c14a65b1f4', '8a2a31ee-8b85-4ca5-9ca5-176766e3ee71',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '1', '1', '1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '2024-03-09 14:01:27', '2024-03-09 14:01:27', '1');

INSERT INTO service_type_table (id_servicetypeid, service_type, service_abbrev, service_type_display_order, created, modified) VALUES ('dadd0840-cf4e-4257-867d-7e8a525edcf5', 'Natural Cover', 'N', '1', '2024-03-09 14:01:27', '2024-03-09 14:01:27');
INSERT INTO service_type_table (id_servicetypeid, service_type, service_abbrev, service_type_display_order, created, modified) VALUES ('aa46e122-cc6e-4468-b177-93cb117d24c6', 'Artificial Insemination Frozen Laproscopic', 'AI Z L', '5', '2024-03-09 14:01:27', '2024-03-09 14:01:27');
INSERT INTO service_type_table (id_servicetypeid, service_type, service_abbrev, service_type_display_order, created, modified) VALUES ('f513a321-120b-4b71-af4c-6f6c818ffe19', 'Artificial Insemination Frozen Vaginal', 'AI Z V', '3', '2024-03-09 14:01:27', '2024-03-09 14:01:27');
INSERT INTO service_type_table (id_servicetypeid, service_type, service_abbrev, service_type_display_order, created, modified) VALUES ('a1d65472-4bc7-4ecc-8182-fe4698c07400', 'Artificial Insemination Fresh Laproscopic', 'AI F L', '4', '2024-03-09 14:01:27', '2024-03-09 14:01:27');
INSERT INTO service_type_table (id_servicetypeid, service_type, service_abbrev, service_type_display_order, created, modified) VALUES ('bf24cffa-3435-4e4e-87bf-518c861aeb0e', 'Artificial Insemination Fresh Vaginal', 'AI F V', '2', '2024-03-09 14:01:27', '2024-03-09 14:01:27');

INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('23c415a7-b60f-4231-8cfc-c205a79faef3', 'Ram', 'R', 'Male', 'M', '1', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('ef27e059-b9db-4fe5-8017-8b1e10acf59f', 'Ewe', 'E', 'Female', 'F', '2', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('3230b749-e2a3-444c-a862-5f701cee5672', 'Wether', 'W', 'Castrate', 'C', '3', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('7ae73a29-f79f-4bfb-b009-3933c48cebf6', 'Unknown', 'U', 'Unknown', 'U', '4', '3ca0b500-3f96-4342-8620-bfda6e900222', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('125d0f5e-05bf-4b91-b5f2-d0bb2ea4b32e', 'Buck', 'B', 'Male', 'M', '1', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('9a725f0a-2979-4063-9602-136a49fec454', 'Doe', 'D', 'Female', 'F', '2', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('9239fbb6-52f0-474e-8d1a-bf9d688706fa', 'Wether', 'W', 'Castrate', 'C', '3', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('f63ab268-623d-4364-9160-d1a8c287cc71', 'Unknown', 'U', 'Unknown', 'U', '4', '3ea774ba-9103-410e-a904-d91a22b38276', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('9363482e-341c-453c-977b-a4f2898fbaf1', 'Bull', 'B', 'Male', 'M', '1', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('d89896da-5f58-4e3f-a8ce-23e5a104508b', 'Cow', 'C', 'Female', 'F', '2', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('86333f95-7a78-4d34-85d9-a7a52e17f132', 'Steer', 'S', 'Castrate', 'C', '3', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('778c9996-ac82-4fac-a787-e9b6f728d5a7', 'Unknown', 'U', 'Unknown', 'U', '4', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('f8482618-57e7-4dc2-beac-a1974338713f', 'Stallion', 'S', 'Male', 'M', '1', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('3a53ad48-05bd-4e99-8261-70e412772000', 'Mare', 'M', 'Female', 'F', '2', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('bd3b67f0-b42d-4d74-9bfe-6b1f7019d82f', 'Gelding', 'G', 'Castrate', 'C', '3', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('ae6d2a12-89d8-4af4-95c3-2f75a477d260', 'Unknown', 'U', 'Unknown', 'U', '4', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('f7d2d691-e880-4907-bdcb-033f9fa55a3f', 'Jack', 'JK', 'Male', 'M', '1', '67966e17-2e3d-44e3-9401-a2c6112fdbb4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('eb885c80-59ea-4656-aa2d-9a7c27de7a75', 'Jenny', 'JN', 'Female', 'F', '2', '67966e17-2e3d-44e3-9401-a2c6112fdbb4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('2750cfe3-dbb3-49d4-bae0-3ed8ffc42b18', 'Gelding', 'G', 'Castrate', 'C', '3', '67966e17-2e3d-44e3-9401-a2c6112fdbb4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('6477f915-f9c1-4abb-b64a-f53c460d610e', 'Unknown', 'U', 'Unknown', 'U', '4', '67966e17-2e3d-44e3-9401-a2c6112fdbb4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('8ff2b1e9-82a4-4a5d-b005-2e2ea406e22d', 'Boar', 'B', 'Male', 'M', '1', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('24f10675-75e5-45da-83c9-37b72656c67a', 'Sow', 'S', 'Female', 'F', '2', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('8d8f7b85-8e6b-415b-a4f6-c31eb2b03441', 'Barrow', 'BW', 'Castrate', 'C', '3', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('c358d1e6-e290-47f2-821d-a16006c1f6c7', 'Unknown', 'U', 'Unknown', 'U', '4', 'ce4876de-69c7-4e11-b123-c333d557eece', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('79184358-1b5f-4e5e-811f-0dae722ea469', 'Spayed Ewe', 'SE', 'Spayed', 'SE', '5', '3ca0b500-3f96-4342-8620-bfda6e900222', '2024-12-02 11:39:55', '2024-12-02 11:39:55');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('5ba27366-8880-4f59-9e88-e593a40919f1', 'Spayed Doe', 'SD', 'Spayed', 'SP', '5', '3ea774ba-9103-410e-a904-d91a22b38276', '2024-12-02 11:39:55', '2024-12-02 11:39:55');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('ca5b2865-4508-4dd5-9254-35cf678f3439', 'Spayed Cow', 'SC', 'Spayed', 'SP', '5', '93d8dd69-8d85-44e3-a757-9d215b4295be', '2024-12-02 11:39:55', '2024-12-02 11:39:55');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('566868de-74e5-4d5c-a945-97cf34d33efe', 'Spayed Mare', 'SM', 'Spayed', 'SP', '5', 'a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', '2024-12-02 11:39:55', '2024-12-02 11:39:55');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('109dfd64-79b7-4164-bfaa-2f000e214b62', 'Spayed Jenny', 'SJ', 'Spayed', 'SP', '5', '67966e17-2e3d-44e3-9401-a2c6112fdbb4', '2024-12-02 11:39:55', '2024-12-02 11:39:55');
INSERT INTO sex_table (id_sexid, sex_name, sex_abbrev, sex_standard, sex_abbrev_standard, sex_display_order, id_speciesid, created, modified) VALUES ('b436d6a8-cf86-4866-915e-c4e084dbca90', 'Spayed Sow', 'SS', 'Spayed', 'SP', '5', 'ce4876de-69c7-4e11-b123-c333d557eece', '2024-12-02 11:39:55', '2024-12-02 11:39:55');

INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('357a1870-abe7-4a8b-83af-d8950f75f3bd', 'animal_ebv_table', 'animal_ebv_id_number', 'ID', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('e2b94a36-1125-4b99-b949-74075e0c03c2', 'sheep_table', 'id_sexid', 'SEX', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('143aa19c-5557-4db7-a064-cddc2fd93f12', 'sheep_nsip_ebv_table', 'ebv_date', 'DATE', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('b23369a0-fb46-4531-a40b-09e2baa021d3', 'sheep_nsip_ebv_table', 'which_run', 'RUN_NAME', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('6128466e-1f8a-4bc5-9de6-f8038496865f', 'sheep_nsip_ebv_table', 'ebv_birth_weight', 'BWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('5b728f18-a104-4d6e-9732-fa45911d854b', 'sheep_nsip_ebv_table', 'ebv_birth_weight_acc', 'ACC_BWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('9c340201-fbd4-49dd-9316-2ebab0a32b2f', 'sheep_nsip_ebv_table', 'ebv_wean_weight', 'WWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('cd784474-6fee-4c38-9b63-9f6efc85209d', 'sheep_nsip_ebv_table', 'ebv_wean_weight_acc', 'ACC_WWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('09fbadbc-c4fa-417c-b915-3b5e8c4b6e3a', 'sheep_nsip_ebv_table', 'ebv_post_wean_weight', 'PWWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('1def5c45-2f0b-4f7c-8edc-e264fc1bae4d', 'sheep_nsip_ebv_table', 'ebv_post_wean_weight_acc', 'ACC_PWWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('648e219c-6a3f-407c-b73f-d6c6a376fc1f', 'sheep_nsip_ebv_table', 'ebv_yearling_weight', 'YWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('e7e1c5f7-4e8b-40fa-b33d-42a237fb46e8', 'sheep_nsip_ebv_table', 'ebv_yearling_weight_acc', 'ACC_YWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('cd1fd422-c87d-41c9-8157-3d3ead22704a', 'sheep_nsip_ebv_table', 'ebv_hogget_weight', 'HWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('ac33018e-c7da-46d2-a525-39be0e4a171f', 'sheep_nsip_ebv_table', 'ebv_hogget_weight_acc', 'ACC_HWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('8b9a175a-85b8-4726-89f3-540cdb202a6a', 'sheep_nsip_ebv_table', 'ebv_adult_weight', 'AWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('d49a76ed-b960-4108-952b-14ab59f39782', 'sheep_nsip_ebv_table', 'ebv_adult_weight_acc', 'ACC_AWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('c6c6d01d-ca37-4089-a617-5eee72bbe367', 'sheep_nsip_ebv_table', 'ebv_wean_fat', 'WFAT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('d8f5a6ef-5d3c-4d18-a571-6170b2b91ae6', 'sheep_nsip_ebv_table', 'ebv_wean_fat_acc', 'ACC_WCF', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('6fdb8478-405b-4464-8b86-c860c53cc846', 'sheep_nsip_ebv_table', 'ebv_post_wean_fat', 'PFAT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('42f1f87f-c8a7-4dd3-b37a-945ec8b639bb', 'sheep_nsip_ebv_table', 'ebv_post_wean_fat_acc', 'ACC_PWCF', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('1cd48787-f14d-4a31-a184-15257021d16c', 'sheep_nsip_ebv_table', 'ebv_yearling_fat', 'YFAT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('e7c8167b-45fa-4b23-bb1a-8d5e7cfcd24f', 'sheep_nsip_ebv_table', 'ebv_yearling_fat_acc', 'ACC_YCF', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('1ec440ff-a280-4886-9fd3-e84130567b56', 'sheep_nsip_ebv_table', 'ebv_hogget_fat', 'HFAT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('7adae6e1-1284-4e40-bbe5-65b44042f132', 'sheep_nsip_ebv_table', 'ebv_hogget_fat_acc', 'ACC_HCF', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('4d8331a7-c447-483d-8cfe-81b5c75d61c0', 'sheep_nsip_ebv_table', 'ebv_wean_emd', 'WEMD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('9e4af7ef-a4e8-4510-87e2-bb4d88fce92a', 'sheep_nsip_ebv_table', 'ebv_wean_emd_acc', 'ACC_WEMD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('d75395dc-58a5-48a6-b800-90bf9496dcbe', 'sheep_nsip_ebv_table', 'ebv_post_wean_emd', 'PEMD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('9008385b-35bd-4c78-842a-8fb2f0d7184e', 'sheep_nsip_ebv_table', 'ebv_post_wean_emd_acc', 'ACC_PEMD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('1e708696-60b1-4da7-b202-13a3dd01b85c', 'sheep_nsip_ebv_table', 'ebv_yearling_emd', 'YEMD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('8f9d9177-d3e6-4beb-9ceb-d37bb7fc3d4a', 'sheep_nsip_ebv_table', 'ebv_yearling_emd_acc', 'ACC_YEMD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('59e5bcd7-f7aa-468c-8536-876c3ffb44b2', 'sheep_nsip_ebv_table', 'ebv_hogget_emd', 'HEMD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('21c23bc7-ff59-44d0-ad62-f0f87528822a', 'sheep_nsip_ebv_table', 'ebv_hogget_emd_acc', 'ACC_HEMD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('d07473fd-b910-43f3-a76e-a379e59b1258', 'sheep_nsip_ebv_table', 'ebv_yearling_fleece_diameter', 'YFD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('838f6a52-2681-4212-b17e-105b7b33e18b', 'sheep_nsip_ebv_table', 'ebv_yearling_fleece_diameter_acc', 'ACC_YFD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('1716be61-0885-474b-9fa0-6302e2000de7', 'sheep_nsip_ebv_table', 'ebv_hogget_fleece_diameter', 'HFD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('6497a6e3-1c72-4789-9d62-4bf1e7494c30', 'sheep_nsip_ebv_table', 'ebv_hogget_fleece_diameter_acc', 'ACC_HFD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('fd1a143a-1e10-4c19-b53e-172daeb58c97', 'sheep_nsip_ebv_table', 'ebv_adult_fleece_diameter', 'AFD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('58ef90d3-d5c2-4a02-b926-497d8ab8b888', 'sheep_nsip_ebv_table', 'ebv_adult_fleece_diameter_acc', 'ACC_AFD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('be1ab49c-9ae9-4185-bbc7-0d7f10f89477', 'sheep_nsip_ebv_table', 'ebv_yearling_greasy_fleece_weight', 'YGFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('f829d9ea-f60c-484a-8712-6218d57860dd', 'sheep_nsip_ebv_table', 'ebv_yearling_greasy_fleece_weight_acc', 'ACC_YGFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('2ef7de55-cb11-49fd-9d09-89da80d78faa', 'sheep_nsip_ebv_table', 'ebv_hogget_greasy_fleece_weight', 'HGFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('fd0443f1-2f9c-49f6-a23e-9dce123f4362', 'sheep_nsip_ebv_table', 'ebv_hogget_greasy_fleece_weight_acc', 'ACC_HGFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('9a5ced86-d4a9-4a90-aeff-e5c3466bb3a0', 'sheep_nsip_ebv_table', 'ebv_adult_greasy_fleece_weight', 'AGFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('d407f53e-e22d-4f59-b6eb-0ac7e23dfb83', 'sheep_nsip_ebv_table', 'ebv_adult_greasy_fleece_weight_acc', 'ACC_AGFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('1534b303-5406-4ec6-8ffc-c4cb8d1c9e29', 'sheep_nsip_ebv_table', 'ebv_yearling_clean_fleece_weight', 'YCFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('d23d01bf-ffeb-4239-b1de-1371d0ef344c', 'sheep_nsip_ebv_table', 'ebv_yearling_clean_fleece_weight_acc', 'ACC_YCFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('923f7c73-3f52-48e6-bd01-53eb8fcf6184', 'sheep_nsip_ebv_table', 'ebv_hogget_clean_fleece_weight', 'HCFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('1423614f-d812-4ff7-aa98-c1e89a7b214c', 'sheep_nsip_ebv_table', 'ebv_hogget_clean_fleece_weight_acc', 'ACC_HCFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('a26d8d7a-ddab-4448-b0a7-b9fcda82dad0', 'sheep_nsip_ebv_table', 'ebv_adult_clean_fleece_weight', 'ACFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('ecf39bc8-0a81-4e80-a74b-6675ec652a87', 'sheep_nsip_ebv_table', 'ebv_adult_clean_fleece_weight_acc', 'ACC_ACFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('b3f36a37-8132-4c53-9797-aa4b7489c20a', 'sheep_nsip_ebv_table', 'ebv_yearling_fleece_yield', 'YYLD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('1cdd9f5f-d907-4eeb-9403-4faea206d691', 'sheep_nsip_ebv_table', 'ebv_yearling_fleece_yield_acc', 'ACC_YYLD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('d6233110-dfdb-43af-ad26-c5c7981992a0', 'sheep_nsip_ebv_table', 'ebv_hogget_fleece_yield', 'HYLD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('59fa74d8-c3a3-445f-9ed1-1d9986b913fb', 'sheep_nsip_ebv_table', 'ebv_hogget_fleece_yield_acc', 'ACC_HYLD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('538a4aa7-7dd1-4102-9e84-5a14cbf8ab51', 'sheep_nsip_ebv_table', 'ebv_adult_fleece_yield', 'AYLD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('3a6ea82a-a029-4023-8430-5cdd79c687d7', 'sheep_nsip_ebv_table', 'ebv_adult_fleece_yield_acc', 'ACC_AYLD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('d1f932cc-2cc1-4acd-88dd-263df4004a4a', 'sheep_nsip_ebv_table', 'ebv_yearling_fiber_diameter_variation', 'YFDCV', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('549e5a47-975a-466a-986e-18c1a724660a', 'sheep_nsip_ebv_table', 'ebv_yearling_fiber_diameter_variation_acc', 'ACC_YFDCV', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('2bc8aeca-4f5c-4198-bf97-279fca461791', 'sheep_nsip_ebv_table', 'ebv_hogget_fiber_diameter_variation', 'HFDCV', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('457de5fe-36c7-46d1-83c0-fab99fe9b26f', 'sheep_nsip_ebv_table', 'ebv_hogget_fiber_diameter_variation_acc', 'ACC_HFDCV', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('d92e26e6-a849-48a8-a8c9-6aa78ffb8a58', 'sheep_nsip_ebv_table', 'ebv_adult_fiber_diameter_variation', 'AFDCV', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('07381e74-74a9-4458-b7ba-85fdaa4a79bc', 'sheep_nsip_ebv_table', 'ebv_adult_fiber_diameter_variation_acc', 'ACC_AFDCV', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('afd4dd59-a793-4305-bc7a-10c4bfa42d64', 'sheep_nsip_ebv_table', 'ebv_yearling_staple_strength', 'YSS', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('534ae7ac-48be-4c2f-a72a-070b3bec3889', 'sheep_nsip_ebv_table', 'ebv_yearling_staple_strength_acc', 'ACC_YSS', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('7f351aba-60e1-44bc-bda4-38e1e214c193', 'sheep_nsip_ebv_table', 'ebv_hogget_staple_strength', 'HSS', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('a67dc752-520a-4711-aa84-a6625a09157e', 'sheep_nsip_ebv_table', 'ebv_hogget_staple_strength_acc', 'ACC_HSS', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('07a0e38d-37e2-4da2-88b7-579e4f2987f7', 'sheep_nsip_ebv_table', 'ebv_adult_staple_strength', 'ASS', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('59dbe391-b7ff-45bd-a280-93dfdc481e27', 'sheep_nsip_ebv_table', 'ebv_adult_staple_strength_acc', 'ACC_ASS', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('37eb87f9-14d4-4d7c-8f0c-c4c5bc362f7b', 'sheep_nsip_ebv_table', 'ebv_yearling_staple_length', 'YSL', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('1008384f-1338-4d55-87f2-07974babd0e1', 'sheep_nsip_ebv_table', 'ebv_yearling_staple_length_acc', 'ACC_YSL', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('bcc6db4f-8de3-4e40-8836-42f3836705bb', 'sheep_nsip_ebv_table', 'ebv_hogget_staple_length', 'HSL', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('3257c762-da33-4cf3-9b30-4c8a1b7337f9', 'sheep_nsip_ebv_table', 'ebv_hogget_staple_length_acc', 'ACC_HSL', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('120d7aa4-7249-415c-8571-b0ed26ec0390', 'sheep_nsip_ebv_table', 'ebv_adult_staple_length', 'ASL', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('97bbfa22-52dd-44f9-86d5-f11b755b3c4c', 'sheep_nsip_ebv_table', 'ebv_adult_staple_length_acc', 'ACC_ASL', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('9136455f-24cb-41be-9093-18f7cf7a77b8', 'sheep_nsip_ebv_table', 'ebv_yearling_fleece_curvature', 'YCURV', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('45862d6e-92a5-4454-9bf4-baafc55ad524', 'sheep_nsip_ebv_table', 'ebv_yearling_fleece_curvature_acc', 'ACC_YCURV', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('0cac69b8-0276-419c-a0ed-f06bd76f12e6', 'sheep_nsip_ebv_table', 'ebv_hogget_fleece_curvature', 'HCURV', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('261b4099-e5ff-4734-83cb-7e5ca10071b2', 'sheep_nsip_ebv_table', 'ebv_hogget_fleece_curvature_acc', 'ACC_HCURV', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('4046a0b4-9dc2-4eb3-88e0-02da2f8d971f', 'sheep_nsip_ebv_table', 'ebv_adult_fleece_curvature', 'ACURV', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('6e929929-458f-4512-86cb-9ea46a7d37bb', 'sheep_nsip_ebv_table', 'ebv_adult_fleece_curvature_acc', 'ACC_ACURV', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('bd257887-5316-494f-b759-cb56f5703356', 'sheep_nsip_ebv_table', 'ebv_wean_fec', 'WFEC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('f841a21f-1d42-4ea6-8fde-a3856a309714', 'sheep_nsip_ebv_table', 'ebv_wean_fec_acc', 'ACC_WFEC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('884ec9b1-a5e7-4add-87a4-8d3385ddb71d', 'sheep_nsip_ebv_table', 'ebv_post_wean_fec', 'PFEC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('66163492-2db2-4544-b31f-c3175d6d2f29', 'sheep_nsip_ebv_table', 'ebv_post_wean_fec_acc', 'ACC_PFEC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('41f16ff8-25d0-405d-a40b-8fa6e0857c1c', 'sheep_nsip_ebv_table', 'ebv_yearling_fec', 'YFEC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('27d876aa-7534-4940-8b89-9cfaf90d7b3a', 'sheep_nsip_ebv_table', 'ebv_yearling_fec_acc', 'ACC_YFEC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('75bc0832-f495-4417-a0cd-5e4bfa1800da', 'sheep_nsip_ebv_table', 'ebv_hogget_fec', 'HFEC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('6dd98bac-93a0-4113-b4bf-48bbcd63d401', 'sheep_nsip_ebv_table', 'ebv_hogget_fec_acc', 'ACC_HFEC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('2e960ed2-553b-409b-957f-8ff66f9dcb0d', 'sheep_nsip_ebv_table', 'ebv_post_wean_scrotal', 'PSC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('ad6a125a-e12c-4227-b26a-1b76cd410614', 'sheep_nsip_ebv_table', 'ebv_post_wean_scrotal_acc', 'ACC_PSC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('a2a93584-5cba-4861-bce9-9f0e2a6f72ef', 'sheep_nsip_ebv_table', 'ebv_yearling_scrotal', 'YSC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('75281581-ce48-4bc8-97f3-a252204521d9', 'sheep_nsip_ebv_table', 'ebv_yearling_scrotal_acc', 'ACC_YSC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('a9bc9e11-86ee-44c5-a829-68e07da88292', 'sheep_nsip_ebv_table', 'ebv_hogget_scrotal', 'HSC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('85c3b415-bf6b-4e90-a595-056035b67b8c', 'sheep_nsip_ebv_table', 'ebv_hogget_scrotal_acc', 'ACC_HSC', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('81948fa2-9d15-437e-90af-a4aab16be56b', 'sheep_nsip_ebv_table', 'ebv_number_lambs_born', 'NLB', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('150677ab-c95c-454e-82d8-7139b556cf8c', 'sheep_nsip_ebv_table', 'ebv_number_lambs_born_acc', 'ACC_NLB', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('50e94239-7008-4d32-8aba-51d3b7982cd7', 'sheep_nsip_ebv_table', 'ebv_yearling_number_lambs_born', 'YNLB', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('8c6e864f-d542-4dae-91cb-7471f0f912a5', 'sheep_nsip_ebv_table', 'ebv_yearling_number_lambs_born_acc', 'ACC_YNLB', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('fb7b7df2-637e-4389-8a4c-3383cf18d738', 'sheep_nsip_ebv_table', 'ebv_number_lambs_weaned', 'NLW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('8931dcfb-76ab-4a06-8675-9dccf6aa1dc9', 'sheep_nsip_ebv_table', 'ebv_number_lambs_weaned_acc', 'ACC_NLW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('2b61992b-9283-4fd9-84a1-ce68a77b5877', 'sheep_nsip_ebv_table', 'ebv_yearling_number_lambs_weaned', 'YNLW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('b90a1fca-e405-4038-ba69-5f945069fed0', 'sheep_nsip_ebv_table', 'ebv_yearling_number_lambs_weaned_acc', 'ACC_YNLW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('5f561209-fcba-4f9f-a1c6-0857f44f408a', 'sheep_nsip_ebv_table', 'ebv_maternal_birth_weight', 'MBWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('324a3203-d95f-4b51-a7c1-54b4f798f3b2', 'sheep_nsip_ebv_table', 'ebv_maternal_birth_weight_acc', 'ACC_MBWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('866f1fa1-e794-467d-8b52-e031717b07f5', 'sheep_nsip_ebv_table', 'ebv_maternal_wean_weight', 'MWWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('b0c07298-04d4-4c09-a564-55d54920ec3c', 'sheep_nsip_ebv_table', 'ebv_maternal_wean_weight_acc', 'ACC_MLWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('16f5ad26-bed9-4219-a088-bcbeaa631b65', 'sheep_nsip_ebv_table', 'border_dollar_index', 'INDEX_2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('8233c43a-5a87-428d-8f52-dd2e5fe07c6b', 'sheep_nsip_ebv_table', 'border_dollar_acc', 'A_INDEX_2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('491f6309-b124-4a52-8f71-cb67460c84fb', 'sheep_nsip_ebv_table', 'self_replacing_carcass_index', 'INDEX_4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('8b850cfe-fda2-4c24-a67d-df1ee4e4419d', 'sheep_nsip_ebv_table', 'self_replacing_carcass_acc', 'A_INDEX_4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('b7768999-8a74-4dfe-90ba-e9c989e500e4', 'sheep_nsip_ebv_table', 'self_replacing_carcass_no_repro_index', 'INDEX04', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('6d1aaa63-8c1c-4b54-8886-f19c45ddebf8', 'sheep_nsip_ebv_table', 'self_replacing_carcass_no_repro_acc', 'A_INDEX04', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('dded525f-c980-4afd-8172-552d6ed413ed', 'sheep_nsip_ebv_table', 'carcass_plus_index', 'INDEX_8', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('97ba5591-2891-4e51-843a-b061cadd15c3', 'sheep_nsip_ebv_table', 'carcass_plus_acc', 'A_INDEX_8', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('042fdeb1-35c9-4fb9-bfbb-41542382365d', 'sheep_nsip_ebv_table', 'spare_index', 'INDEX_9', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('7e0f61aa-4a62-4fa1-b1af-b98638a8a36f', 'sheep_nsip_ebv_table', 'spare_index_acc', 'A_INDEX_9', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('3b428b79-8dcf-4ea1-86db-9729922e771e', 'sheep_nsip_ebv_table', 'lamb_2020_index', 'INDEX_13', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('a7b287c9-5a73-4738-8124-3ccd379da79d', 'sheep_nsip_ebv_table', 'lamb_2020__acc', 'A_INDEX_13', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('1ef4f558-ffd5-45ab-8b7b-e960cfbac9a6', 'sheep_nsip_ebv_table', 'maternal_dollar_index', 'INDEX_14', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('2f6babfe-b21c-48f7-a2eb-eb6971a36d85', 'sheep_nsip_ebv_table', 'maternal_dollar_acc', 'A_INDEX_14', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('74801c7a-de51-44d7-99f1-3444ff7b1f29', 'sheep_nsip_ebv_table', 'maternal_dollar_no_repro_index', 'INDEX014', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('ba3d683c-5b9d-4eea-af2c-4eeba1f98c7d', 'sheep_nsip_ebv_table', 'maternal_dollar_no_repro_acc', 'A_INDEX014', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('9e61da4d-8977-4ba2-9360-8be6effb6d00', 'sheep_nsip_ebv_table', 'coopworth_dollar_index', 'INDEX_15', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('d0244987-f05d-4685-9e58-a5384e0ec1e9', 'sheep_nsip_ebv_table', 'coopworth_dollar_acc', 'A_INDEX_15', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('9eb5bd86-5962-4b52-81a9-cea53b48e73f', 'sheep_nsip_ebv_table', 'spare_2_index', 'INDEX_24', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('d5e6fb5a-6d95-41a2-b18c-9ff4fb624e57', 'sheep_nsip_ebv_table', 'spare_2_index_acc', 'A_INDEX_24', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('e33532c4-37a3-418b-9172-81fbbf32f058', 'sheep_nsip_ebv_table', 'dual_purpose_dollar_index', 'INDEX_25', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('5ee23c87-3949-4bff-a071-cfd35d5131d1', 'sheep_nsip_ebv_table', 'dual_purpose_dollar_acc', 'A_INDEX_25', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('f2240027-7dcf-49c1-acc3-8eb35a1ea7e1', 'sheep_nsip_ebv_table', 'dual_purpose_dollar_no_repro_index', 'INDEX025', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('12c5574c-6408-49dc-a4a1-2ad8afef9fe4', 'sheep_nsip_ebv_table', 'dual_purpose_dollar_no_repro_acc', 'A_INDEX025', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('c9e75f90-99f1-4b9c-a0ba-feca2b0f1186', 'sheep_nsip_ebv_table', 'export_index', 'INDEX_E', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('cde30bf4-6eab-4993-a069-0795ddbd8a44', 'sheep_nsip_ebv_table', 'trade_index', 'INDEX_T', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('a6e65f77-ba11-4294-aeaf-789e6f865316', 'sheep_nsip_ebv_table', 'merino_dp_index', 'INDEX1', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('f11d3bee-267f-430e-915e-ca6e2f7f8084', 'sheep_nsip_ebv_table', 'merino_dp_acc', 'A_INDEX1', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('9603168c-2734-42bc-980d-cfd567d1b8b4', 'sheep_nsip_ebv_table', 'merino_dp_plus_index', 'INDEX2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('c0065b21-281c-47db-80c4-d318d0c53c52', 'sheep_nsip_ebv_table', 'merino_dp_plus_acc', 'A_INDEX2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('886f1e59-6094-4948-a85a-0f87f956db3e', 'sheep_nsip_ebv_table', 'fine_medium_index', 'INDEX3', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('c9d3890d-f1bd-4a70-8081-b9db6a7ef6a8', 'sheep_nsip_ebv_table', 'fine_medium_acc', 'A_INDEX3', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('4fe1ddf2-0f1f-41c4-a9b6-9e535ad3f9c1', 'sheep_nsip_ebv_table', 'fine_medium_plus_index', 'INDEX4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('82f09110-d618-45f4-96cc-45139c22c280', 'sheep_nsip_ebv_table', 'fine_medium_plus_acc', 'A_INDEX4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('208a0a31-4266-49b1-ab78-d08873890a08', 'sheep_nsip_ebv_table', 'samm_index', 'INDEX5', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('f2c6d99d-6190-40af-a8be-d4815c59f617', 'sheep_nsip_ebv_table', 'samm_acc', 'A_INDEX5', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('7866c736-dc0a-46b2-b309-9d158ad91ee5', 'sheep_nsip_ebv_table', 'dohne_no_repro_index', 'INDEX6', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('cf87caa0-45e5-4aa2-b79a-78d3a0052d68', 'sheep_nsip_ebv_table', 'dohne_no_repro_acc', 'A_INDEX6', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('1e9ef660-07b5-421c-b049-6e6de0461397', 'sheep_nsip_ebv_table', 'superfine_index', 'INDEX7', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('dccbefc9-5c11-4c34-a064-89dbe7958a05', 'sheep_nsip_ebv_table', 'superfine_acc', 'A_INDEX7', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('89c62d78-7744-41aa-9408-c233300765dd', 'sheep_nsip_ebv_table', 'superfine_plus_index', 'INDEX8', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('330b105d-2ffd-478d-9f2c-f9591975e6aa', 'sheep_nsip_ebv_table', 'superfine_plus_acc', 'A_INDEX8', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('9420db6b-81cf-4d39-a509-42c5a4b6c9cd', 'sheep_nsip_ebv_table', 'usa_maternal_index', 'USERINDEX', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('b7f4b28f-8fa1-45d2-892a-eae5fba641a5', 'sheep_nsip_ebv_table', 'maternal_greasy_fleece_weight', 'MGFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('13ef3130-7cc5-4ed9-b769-1d2faeff6027', 'sheep_nsip_ebv_table', 'maternal_greasy_fleece_weight_acc', 'ACC_MGFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('6accbfa5-f80c-491c-b97b-b1dc62ca667b', 'sheep_nsip_ebv_table', 'maternal_clean_fleece_weight', 'MCFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('1e89d5bb-a2eb-4145-ace1-7ac97be4630a', 'sheep_nsip_ebv_table', 'maternal_clean_fleece_weight_acc', 'ACC_MCFW', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('89c99f1e-7e31-42ae-9dbe-beff6c9e0728', 'sheep_nsip_ebv_table', 'ebv_lambease_direct', 'LE_DIR', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('491c9c55-e8a1-49ba-9d90-708236e78cb1', 'sheep_nsip_ebv_table', 'ebv_lambease_direct_acc', 'ACC_LE_DIR', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('45a21d66-37a6-4624-a244-962c0b2b00d2', 'sheep_nsip_ebv_table', 'ebv_lambease_daughter', 'LE_DAU', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('06f50679-8bfe-409a-a728-040d80d4d215', 'sheep_nsip_ebv_table', 'ebv_lambease_daughter_acc', 'ACC_LE_DAU', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('6a048234-87c7-4115-8813-44bd4edc43e6', 'sheep_nsip_ebv_table', 'ebv_gestation_length', 'GL_DIR', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('2d768983-6f6b-4528-8f86-2dfa5c18675d', 'sheep_nsip_ebv_table', 'ebv_gestation_length_acc', 'ACC_GL_DIR', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('e3a79dfc-793e-4f94-aba7-179f45a064bc', 'sheep_nsip_ebv_table', 'ebv_gestation_length_daughter', 'GL_DAU', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('ea5b82f3-4700-41a0-998b-b5b404bb1767', 'sheep_nsip_ebv_table', 'ebv_gestation_length_daughter_acc', 'ACC_GL_DAU', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('b98f5241-a7a3-4639-a875-f65c48480d33', 'sheep_nsip_ebv_table', 'early_breech_wrinkle', 'EBWR', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('e4fa2acc-d236-4d1c-a834-89fd50703947', 'sheep_nsip_ebv_table', 'early_breech_wrinkle_acc', 'ACC_EBWR', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('6340b34b-0822-41eb-8142-998a58f24f03', 'sheep_nsip_ebv_table', 'late_breech_wrinkle', 'LBWR', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('842915c5-c26c-4664-8d50-9b5d9a8af7d8', 'sheep_nsip_ebv_table', 'late_breech_wrinkle_acc', 'ACC_LBWR', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('cfbad5ef-afb2-4dfc-940b-75591b4e9ffb', 'sheep_nsip_ebv_table', 'early_body_wrinkle', 'EDBWR', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('8faedfbf-e70d-46d3-97b5-09b8a5696260', 'sheep_nsip_ebv_table', 'early_body_wrinkle_acc', 'ACC_EDBWR', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('dafcb65d-f069-46e6-863a-db72288e985d', 'sheep_nsip_ebv_table', 'late_body_wrinkle', 'LDBWR', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('4452608b-20f5-4ed1-aae4-0421039a5a41', 'sheep_nsip_ebv_table', 'late_body_wrinkle_acc', 'ACC_LDBWR', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('4fde6689-7cb4-4ed8-89f4-47b5a7139c62', 'sheep_nsip_ebv_table', 'late_dag', 'LDAG', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('0b5086ab-e33e-4114-9c22-24c99b8b07cd', 'sheep_nsip_ebv_table', 'late_dag_acc', 'ACC_LDAG', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('4301e792-f428-4e94-bbaf-76b5a81b2827', 'sheep_nsip_ebv_table', 'intramuscular_fat', 'IMF', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('5313d556-ec69-477e-b30b-f37e4b1b1e9c', 'sheep_nsip_ebv_table', 'intramuscular_fat_acc', 'ACC_IMF', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('cf4868b8-acf8-41a8-a9fc-26de15946913', 'sheep_nsip_ebv_table', 'carcass_shear_force', 'SHRF5', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('1a359280-ddbd-4c32-a801-cadfdf316cba', 'sheep_nsip_ebv_table', 'carcass_shear_force_acc', 'ACC_SHRF5', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('d3078afe-782e-4a70-9322-5e2dd160a960', 'sheep_nsip_ebv_table', 'dressing_percentage', 'DRESS', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('652f8201-48e3-4341-a261-c385afa6f0c1', 'sheep_nsip_ebv_table', 'dressing_percentage_acc', 'ACC_DRESS', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('30959b6e-c9c5-4470-937a-643c9db7da69', 'sheep_nsip_ebv_table', 'lean_meat_yield', 'LMY', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('78d763f5-ee10-409b-b367-99caa63b9fad', 'sheep_nsip_ebv_table', 'lean_meat_yield_acc', 'ACC_LMY', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('389573f5-86b5-446c-8739-b831f915ea89', 'sheep_nsip_ebv_table', 'hot_carcass_weight', 'HCWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('d06b00c0-c6d5-45de-be81-95d6f3e347d3', 'sheep_nsip_ebv_table', 'hot_carcass_weight_acc', 'ACC_HCWT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('02e8a1af-815b-4d11-ae90-85a343daae82', 'sheep_nsip_ebv_table', 'carcass_fat', 'CCFAT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('7d43a763-6e09-4b7b-bbee-6194800b86bb', 'sheep_nsip_ebv_table', 'carcass_fat_acc', 'ACC_CCFAT', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('78f99b46-d0f4-4b92-bc46-876f7584d3fb', 'sheep_nsip_ebv_table', 'carcass_eye_muscle_depth', 'CCEMD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_ebv_nsip_lambplan_cross_reference_table (id_sheepebvnsiplambplancrossreferenceid, animaltrakker_table_name, animaltrakker_field_name, lambplan_field_name, created, modified) VALUES ('cf5c8135-3acf-4fee-a85d-d3e9a2a9b76d', 'sheep_nsip_ebv_table', 'carcass_eye_muscle_depth_acc', 'ACC_CCEMD', '2023-09-17 10:26:51', '2023-09-17 10:26:51');

INSERT INTO sheep_fleece_weight_adjustment_table (id_sheepfleeceweightadjustmentid, adjustment_name, ewe_age_2yr, ewe_age_3yr, ewe_age_4_5yr, ewe_age_6_7yr, ewe_age_8yr_up, source_reference, created, modified) VALUES ('e01faec9-8583-4161-a375-6f879349334b', 'Generic', '1.07', '1.02', '1.0', '1.03', '1.05', '2015 Vol. 8 edition of the Sheep Production Manual page 52', '2023-09-17 10:26:51', '2023-09-17 10:26:51');

INSERT INTO sheep_lambs_born_adjustment_table (id_sheeplambsbornadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3yr, ewe_age_4yr, ewe_age_5yr, ewe_age_6yr, ewe_age_7yr, ewe_age_8yr, ewe_age_9yr_up, source_reference, created, modified) VALUES ('a1cfcc4a-818d-46ff-b7ba-bf9f82634540', 'Generic', '1.48', '1.17', '1.05', '1.01', '1.0', '1.0', '1.02', '1.05', '1.13', '2015 Vol. 8 edition of the Sheep Production Manual page 52', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_lambs_born_adjustment_table (id_sheeplambsbornadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3yr, ewe_age_4yr, ewe_age_5yr, ewe_age_6yr, ewe_age_7yr, ewe_age_8yr, ewe_age_9yr_up, source_reference, created, modified) VALUES ('fa14e3f1-c152-4fff-bb4e-a70cf6a19b24', 'Targhee', '1.57', '1.24', '1.1', '1.03', '1.0', '0.99', '1.03', '1.03', '1.13', '2015 Vol. 8 edition of the Sheep Production Manual page 52', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_lambs_born_adjustment_table (id_sheeplambsbornadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3yr, ewe_age_4yr, ewe_age_5yr, ewe_age_6yr, ewe_age_7yr, ewe_age_8yr, ewe_age_9yr_up, source_reference, created, modified) VALUES ('c5206bad-f09a-44d7-9274-a936caa337b3', 'Suffolk', '1.34', '1.08', '1.01', '1.0', '1.0', '1.0', '1.0', '1.03', '1.12', '2015 Vol. 8 edition of the Sheep Production Manual page 52', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_lambs_born_adjustment_table (id_sheeplambsbornadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3yr, ewe_age_4yr, ewe_age_5yr, ewe_age_6yr, ewe_age_7yr, ewe_age_8yr, ewe_age_9yr_up, source_reference, created, modified) VALUES ('7200628e-324f-4333-bd96-005c038559ae', 'PolyPay', '1.52', '1.2', '1.05', '1.0', '1.0', '1.02', '1.02', '1.08', '1.13', '2015 Vol. 8 edition of the Sheep Production Manual page 52', '2023-09-17 10:26:51', '2023-09-17 10:26:51');

INSERT INTO sheep_weaning_adjustment_table (id_sheepweaningadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3_6yr, ewe_age_over_6yr, lamb_sex_ram, lamb_sex_ewe, lamb_sex_wether, birth_and_rear_11, birth_and_rear_12, birth_and_rear_21, birth_and_rear_22, birth_and_rear_31, birth_and_rear_32, birth_and_rear_33, source_reference, created, modified) VALUES ('1c723765-131e-411e-a050-73e429b13485', 'Generic', '1.14', '1.08', '1.0', '1.05', '0.91', '1.0', '0.97', '1.0', '1.17', '1.11', '1.21', '1.19', '1.29', '1.36', '2015 Vol. 8 edition of the Sheep Production Manual page 52', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_weaning_adjustment_table (id_sheepweaningadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3_6yr, ewe_age_over_6yr, lamb_sex_ram, lamb_sex_ewe, lamb_sex_wether, birth_and_rear_11, birth_and_rear_12, birth_and_rear_21, birth_and_rear_22, birth_and_rear_31, birth_and_rear_32, birth_and_rear_33, source_reference, created, modified) VALUES ('ea8898f0-df26-4953-a28f-d053dc5ce751', 'Targhee', '1.14', '1.08', '1.0', '1.02', '0.91', '1.0', '0.97', '1.0', '1.17', '1.12', '1.23', '1.2', '1.31', '1.36', '2015 Vol. 8 edition of the Sheep Production Manual page 52', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_weaning_adjustment_table (id_sheepweaningadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3_6yr, ewe_age_over_6yr, lamb_sex_ram, lamb_sex_ewe, lamb_sex_wether, birth_and_rear_11, birth_and_rear_12, birth_and_rear_21, birth_and_rear_22, birth_and_rear_31, birth_and_rear_32, birth_and_rear_33, source_reference, created, modified) VALUES ('f82e859f-cd7b-4ec9-a859-ff3a18c72b4f', 'Suffolk', '1.14', '1.05', '1.0', '1.06', '0.91', '1.0', '0.97', '1.0', '1.14', '1.11', '1.19', '1.17', '1.29', '1.38', '2015 Vol. 8 edition of the Sheep Production Manual page 52', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO sheep_weaning_adjustment_table (id_sheepweaningadjustmentid, adjustment_name, ewe_age_1yr, ewe_age_2yr, ewe_age_3_6yr, ewe_age_over_6yr, lamb_sex_ram, lamb_sex_ewe, lamb_sex_wether, birth_and_rear_11, birth_and_rear_12, birth_and_rear_21, birth_and_rear_22, birth_and_rear_31, birth_and_rear_32, birth_and_rear_33, source_reference, created, modified) VALUES ('0555f23d-853d-4629-96bb-352030955d36', 'Polypay', '1.0', '1.0', '1.0', '1.04', '0.93', '1.0', '0.97', '1.0', '1.1', '1.1', '1.18', '1.15', '1.24', '1.36', '2015 Vol. 8 edition of the Sheep Production Manual page 52', '2023-09-17 10:26:51', '2023-09-17 10:26:51');

INSERT INTO species_table (id_speciesid, species_common_name, species_generic_name, species_scientific_family, species_scientific_sub_family, species_scientific_name, early_male_breeding_age_days, early_female_breeding_age_days, early_gestation_length_days, late_gestation_length_days, typical_gestation_length_days, created, modified) VALUES ('3ca0b500-3f96-4342-8620-bfda6e900222', 'Sheep', 'Ovine', 'Bovidae', 'Caprinae', 'Ovis aries', '123', '153', '141', '159', '145', '2023-09-17 10:26:51', '2024-01-18 13:21:05');
INSERT INTO species_table (id_speciesid, species_common_name, species_generic_name, species_scientific_family, species_scientific_sub_family, species_scientific_name, early_male_breeding_age_days, early_female_breeding_age_days, early_gestation_length_days, late_gestation_length_days, typical_gestation_length_days, created, modified) VALUES ('3ea774ba-9103-410e-a904-d91a22b38276', 'Goat', 'Caprine', 'Bovidae', 'Caprinae', 'Capra hircus', '120', '120', '140', '160', '151', '2023-09-17 10:26:51', '2024-01-18 13:21:05');
INSERT INTO species_table (id_speciesid, species_common_name, species_generic_name, species_scientific_family, species_scientific_sub_family, species_scientific_name, early_male_breeding_age_days, early_female_breeding_age_days, early_gestation_length_days, late_gestation_length_days, typical_gestation_length_days, created, modified) VALUES ('93d8dd69-8d85-44e3-a757-9d215b4295be', 'Cattle', 'Bovine', 'Bovidae', 'Bovinae', 'Bos taurus', '234', '243', '278', '292', '283', '2023-09-17 10:26:51', '2024-01-18 13:21:05');
INSERT INTO species_table (id_speciesid, species_common_name, species_generic_name, species_scientific_family, species_scientific_sub_family, species_scientific_name, early_male_breeding_age_days, early_female_breeding_age_days, early_gestation_length_days, late_gestation_length_days, typical_gestation_length_days, created, modified) VALUES ('a4652a1b-b5fc-4d3a-a2b3-55b8843c33cf', 'Horse', 'Equine', 'Equidae',NULL, 'Equus caballus', '365', '365', '330', '342', '336', '2023-09-17 10:26:51', '2024-01-18 13:21:05');
INSERT INTO species_table (id_speciesid, species_common_name, species_generic_name, species_scientific_family, species_scientific_sub_family, species_scientific_name, early_male_breeding_age_days, early_female_breeding_age_days, early_gestation_length_days, late_gestation_length_days, typical_gestation_length_days, created, modified) VALUES ('67966e17-2e3d-44e3-9401-a2c6112fdbb4', 'Donkey', 'Equine', 'Equidae',NULL, 'Equus asinus', '365', '365', '335', '426', '365', '2023-09-17 10:26:51', '2024-01-18 13:21:05');
INSERT INTO species_table (id_speciesid, species_common_name, species_generic_name, species_scientific_family, species_scientific_sub_family, species_scientific_name, early_male_breeding_age_days, early_female_breeding_age_days, early_gestation_length_days, late_gestation_length_days, typical_gestation_length_days, created, modified) VALUES ('ce4876de-69c7-4e11-b123-c333d557eece', 'Pig', 'Porcine', 'Suidae', 'Suinae', 'Sus scrofa', '122', '122', '111', '115', '114', '2023-09-17 10:26:51', '2024-01-18 13:21:05');

INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('107f6631-e457-4c21-9a27-907a16c6cbf1', 'Alabama', 'AL', 'b41f2392-2517-459d-b074-464d6915fafa', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('6e9c85bb-c09e-4541-9352-adbc11edd371', 'Alaska', 'AK', 'b41f2392-2517-459d-b074-464d6915fafa', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('0217a37c-9438-4383-9dab-0682effc1ac1', 'Arizona', 'AZ', 'b41f2392-2517-459d-b074-464d6915fafa', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('488384ff-09d0-48d8-b5aa-537a4b78adc0', 'Arkansas', 'AR', 'b41f2392-2517-459d-b074-464d6915fafa', '4', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('3f432960-e734-4f4e-8fe5-382383233aa2', 'California', 'CA', 'b41f2392-2517-459d-b074-464d6915fafa', '5', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('c06bee74-8e98-48f0-9a29-83fd1a76e2fd', 'Colorado', 'CO', 'b41f2392-2517-459d-b074-464d6915fafa', '6', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('df152a5f-a6a8-44b5-b0d0-e6b39d6c77b0', 'Connecticut', 'CT', 'b41f2392-2517-459d-b074-464d6915fafa', '7', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('899f20c5-a738-40bd-bbc5-de70dede6905', 'Delaware', 'DE', 'b41f2392-2517-459d-b074-464d6915fafa', '8', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('de424711-d05f-452f-863b-abb936f5f06d', 'District Of Columbia', 'DC', 'b41f2392-2517-459d-b074-464d6915fafa', '9', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('06106d63-399f-4a72-ac4d-a1fdf1d8825b', 'Florida', 'FL', 'b41f2392-2517-459d-b074-464d6915fafa', '10', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('e20238e1-3c69-4d74-94dc-5ec98fc422cd', 'Georgia', 'GA', 'b41f2392-2517-459d-b074-464d6915fafa', '11', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('b2095b36-c57e-4440-b682-fd76282a5519', 'Hawaii', 'HI', 'b41f2392-2517-459d-b074-464d6915fafa', '12', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('c575756c-dd2f-4eed-b761-939d4fe2c88b', 'Idaho', 'ID', 'b41f2392-2517-459d-b074-464d6915fafa', '13', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('8097b2e4-239a-4a53-994d-5c5b0223c21c', 'Illinois', 'IL', 'b41f2392-2517-459d-b074-464d6915fafa', '14', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('8568f999-b016-4920-a003-559ae0bdce2b', 'Indiana', 'IN', 'b41f2392-2517-459d-b074-464d6915fafa', '15', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('6e42c9cc-3757-4c02-99f2-8ad8b630602c', 'Iowa', 'IA', 'b41f2392-2517-459d-b074-464d6915fafa', '16', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('1ca9bbee-8ef3-47b1-9517-8764bc37f664', 'Kansas', 'KS', 'b41f2392-2517-459d-b074-464d6915fafa', '17', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('0db6b54e-a947-4eca-b64e-c65fce49f393', 'Kentucky', 'KY', 'b41f2392-2517-459d-b074-464d6915fafa', '18', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('c67b8ae1-3482-4100-bd25-93f0364d23b2', 'Louisiana', 'LA', 'b41f2392-2517-459d-b074-464d6915fafa', '19', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('fb3d5752-c327-47bf-ad89-46937fe799e2', 'Maine', 'ME', 'b41f2392-2517-459d-b074-464d6915fafa', '20', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('b97f8956-c1df-4999-a02e-46f0ca80670d', 'Maryland', 'MD', 'b41f2392-2517-459d-b074-464d6915fafa', '21', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('49d2cc86-cf0d-4c01-bdb1-6512d690a12f', 'Massachusetts', 'MA', 'b41f2392-2517-459d-b074-464d6915fafa', '22', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('4035f757-84cf-4d4e-8ad6-f77741808780', 'Michigan', 'MI', 'b41f2392-2517-459d-b074-464d6915fafa', '23', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('54c2f34c-f7e0-4865-a0fe-4de4b54c1731', 'Minnesota', 'MN', 'b41f2392-2517-459d-b074-464d6915fafa', '24', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('c1dbf739-c49e-4ac0-b59e-690b947c68ab', 'Mississippi', 'MS', 'b41f2392-2517-459d-b074-464d6915fafa', '25', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('9c651e1e-77be-4319-9040-110236b899c9', 'Missouri', 'MO', 'b41f2392-2517-459d-b074-464d6915fafa', '26', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('73fd1fcd-df47-4ce4-b9e4-0391e0560ba7', 'Montana', 'MT', 'b41f2392-2517-459d-b074-464d6915fafa', '27', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('7d28293e-55fd-4d69-a1d1-2879c6c9c08b', 'Nebraska', 'NE', 'b41f2392-2517-459d-b074-464d6915fafa', '28', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('3dbe0d30-22b8-4a72-8bf4-e6e437c10cbc', 'Nevada', 'NV', 'b41f2392-2517-459d-b074-464d6915fafa', '29', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('f8fb3881-7496-45b4-bd42-ae1e90e901df', 'New Hampshire', 'NH', 'b41f2392-2517-459d-b074-464d6915fafa', '30', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('a6cdb025-0506-4932-b96a-f8f9839ffb5f', 'New Jersey', 'NJ', 'b41f2392-2517-459d-b074-464d6915fafa', '31', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('fd4ecfbf-5e14-44ff-98a5-a7454fcbe478', 'New Mexico', 'NM', 'b41f2392-2517-459d-b074-464d6915fafa', '32', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('afcdb2d7-bcf0-4671-a048-2e643cdba903', 'New York', 'NY', 'b41f2392-2517-459d-b074-464d6915fafa', '33', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('d896bca0-f453-4d84-8529-ee65eb9d9ea3', 'North Carolina', 'NC', 'b41f2392-2517-459d-b074-464d6915fafa', '34', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('999bf35d-b709-48de-865f-55fad3d7d429', 'North Dakota', 'ND', 'b41f2392-2517-459d-b074-464d6915fafa', '35', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('cdf2157b-ea3f-44dc-81a6-83077a718d92', 'Ohio', 'OH', 'b41f2392-2517-459d-b074-464d6915fafa', '36', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('032eb88f-0392-4597-ab33-000102638e8a', 'Oklahoma', 'OK', 'b41f2392-2517-459d-b074-464d6915fafa', '37', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('dbdd1ff3-865a-4445-b0cf-409fa49a66ab', 'Oregon', 'OR', 'b41f2392-2517-459d-b074-464d6915fafa', '38', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('d3b57509-a6d5-4719-b931-0862af92f78c', 'Pennsylvania', 'PA', 'b41f2392-2517-459d-b074-464d6915fafa', '39', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('ce35a5ee-7400-4e7e-9bc3-0757b82462fa', 'Rhode Island', 'RI', 'b41f2392-2517-459d-b074-464d6915fafa', '40', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('1b49d8ca-5e51-4ab1-b608-7735a82fcb50', 'South Carolina', 'SC', 'b41f2392-2517-459d-b074-464d6915fafa', '41', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('b528bbd6-dd44-49c0-a7ff-83d4c2a22940', 'South Dakota', 'SD', 'b41f2392-2517-459d-b074-464d6915fafa', '42', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('abaec407-fa89-4256-a4cb-0fc0080e2d48', 'Tennessee', 'TN', 'b41f2392-2517-459d-b074-464d6915fafa', '43', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('ceefda93-0b8a-4559-bdd5-ba8a6f26ba7f', 'Texas', 'TX', 'b41f2392-2517-459d-b074-464d6915fafa', '44', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('2490a321-01dd-4e78-a5bd-b98a7a88a2c0', 'Utah', 'UT', 'b41f2392-2517-459d-b074-464d6915fafa', '45', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('af855dff-5253-4d71-b34e-e1bf6752001e', 'Vermont', 'VT', 'b41f2392-2517-459d-b074-464d6915fafa', '46', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('0d3675f5-d016-4f4b-b6f2-3dc9fd4db4ba', 'Virginia', 'VA', 'b41f2392-2517-459d-b074-464d6915fafa', '47', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('b8476af1-b78a-472f-96fe-b71a7ea8fa83', 'Washington', 'WA', 'b41f2392-2517-459d-b074-464d6915fafa', '48', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('e3670b08-3c83-42d3-a3dd-1be3f1fe1fbd', 'West Virginia', 'WV', 'b41f2392-2517-459d-b074-464d6915fafa', '49', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('881f5cff-949b-41e2-82b4-c0bd9b3a3212', 'Wisconsin', 'WI', 'b41f2392-2517-459d-b074-464d6915fafa', '50', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('83d990fb-4814-4276-8e11-238de577eaf5', 'Wyoming', 'WY', 'b41f2392-2517-459d-b074-464d6915fafa', '51', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('0b14204a-15e2-439f-9020-4855ae578273', 'Alberta', 'AB', '07720e3a-e722-4707-a159-3c39aaf62b5b', '52', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('03479360-91c6-46fb-a3dc-af491258308e', 'British Columbia', 'BC', '07720e3a-e722-4707-a159-3c39aaf62b5b', '53', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('66118c21-c610-4a38-856f-8f1de7ab585e', 'Manitoba', 'MB', '07720e3a-e722-4707-a159-3c39aaf62b5b', '54', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('450dffca-ee8d-40df-8fc8-ed4d58889d6d', 'New Brunswick', 'NB', '07720e3a-e722-4707-a159-3c39aaf62b5b', '55', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('ba707d89-5175-4298-b10b-44e762f3e0d5', 'Newfoundland', 'NL', '07720e3a-e722-4707-a159-3c39aaf62b5b', '56', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('6bd51c4f-fd6f-440d-9d9b-9cb02d8ea26b', 'Northwest Territories', 'NT', '07720e3a-e722-4707-a159-3c39aaf62b5b', '57', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('8b733b1d-3c2d-4cf8-bc37-17d8ed3e99ed', 'Nova Scotia', 'NS', '07720e3a-e722-4707-a159-3c39aaf62b5b', '58', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('3e8ca2ca-27dc-44ef-9000-bc8bd46c4d0c', 'Nunavut', 'NU', '07720e3a-e722-4707-a159-3c39aaf62b5b', '59', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('fb26610d-5bf6-4ca7-a5b9-b4de9154b669', 'Ontario', 'ON', '07720e3a-e722-4707-a159-3c39aaf62b5b', '60', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('d31e8916-da95-4d73-8bc7-af6138ad7950', 'Prince Edward Island', 'PE', '07720e3a-e722-4707-a159-3c39aaf62b5b', '61', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('1de892bd-0f59-4fdf-b118-005c4c53a8c0', 'Quebec', 'QC', '07720e3a-e722-4707-a159-3c39aaf62b5b', '62', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('d504ca52-80a5-4c77-890e-505a0b427823', 'Saskatchewan', 'SK', '07720e3a-e722-4707-a159-3c39aaf62b5b', '63', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('0f13683e-55a7-4f02-ba6e-b5dc6f4eeb99', 'Yukon', 'YT', '07720e3a-e722-4707-a159-3c39aaf62b5b', '64', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('6d5b8188-9b0a-4585-b3ec-e34a2b6322c6', 'Devon', 'DEV', '3cbe355a-b195-4ec2-aad6-e5ec3b5a12a7', '65', '2023-11-26 11:36:18', '2023-11-26 11:36:18');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('f00dd2ae-6394-45e0-9ada-4fe397f972e8', 'Buckinghamshire', 'BKM', '3cbe355a-b195-4ec2-aad6-e5ec3b5a12a7', '66', '2023-11-26 11:36:18', '2023-11-26 11:36:18');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('fdc434bf-a1d1-48fb-b650-854e06898f6c', 'Gwynedd', 'GWN', '3cbe355a-b195-4ec2-aad6-e5ec3b5a12a7', '67', '2023-11-26 11:36:18', '2023-11-26 11:36:18');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('4623d520-da11-4316-81b8-4435ec1fc4be', 'Somerset', 'SOM', '3cbe355a-b195-4ec2-aad6-e5ec3b5a12a7', '68', '2023-11-26 11:36:18', '2023-11-26 11:36:18');
INSERT INTO state_table (id_stateid, state_name, state_abbrev, id_countryid, state_display_order, created, modified) VALUES ('565f65df-797f-472e-85ec-fc08b90ad366', 'Breconshire', 'BRE', '3cbe355a-b195-4ec2-aad6-e5ec3b5a12a7', '69', '2024-01-08 07:15:26', '2024-01-08 07:15:26');

INSERT INTO tissue_sample_container_type_table (id_tissuesamplecontainertypeid, tissue_sample_container_name, tissue_sample_container_abbrev, tissue_sample_container_display_order, created, modified) VALUES ('526cec5c-5761-49b0-832c-cd9dae3288cb', 'Purple Top Tube', 'Purple Top', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_sample_container_type_table (id_tissuesamplecontainertypeid, tissue_sample_container_name, tissue_sample_container_abbrev, tissue_sample_container_display_order, created, modified) VALUES ('a37f432c-0b37-45f4-b0ca-79de6e76042e', 'Red Top Tube', 'Red Top', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_sample_container_type_table (id_tissuesamplecontainertypeid, tissue_sample_container_name, tissue_sample_container_abbrev, tissue_sample_container_display_order, created, modified) VALUES ('cd94c4de-0bf6-4577-8908-00f5ed66cc47', 'AllFlex TSU', 'TSU', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_sample_container_type_table (id_tissuesamplecontainertypeid, tissue_sample_container_name, tissue_sample_container_abbrev, tissue_sample_container_display_order, created, modified) VALUES ('462ca2bb-5d2c-4762-9d88-cc9819ae98ba', 'Formalin Preservative', 'Formalin', '4', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_sample_container_type_table (id_tissuesamplecontainertypeid, tissue_sample_container_name, tissue_sample_container_abbrev, tissue_sample_container_display_order, created, modified) VALUES ('56e50cb9-4f47-49fc-88af-72a13da16349', 'Whirl-Pak® Sample Bag', 'Whirl-Pak®', '5', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_sample_container_type_table (id_tissuesamplecontainertypeid, tissue_sample_container_name, tissue_sample_container_abbrev, tissue_sample_container_display_order, created, modified) VALUES ('353f651f-4d90-43e2-b8b9-6221540d1167', 'Trich Tube', 'Trich', '6', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_sample_container_type_table (id_tissuesamplecontainertypeid, tissue_sample_container_name, tissue_sample_container_abbrev, tissue_sample_container_display_order, created, modified) VALUES ('904d2eef-b13b-4d88-8a06-1cb727d79904', 'Plain Tube', 'Plain', '7', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_sample_container_type_table (id_tissuesamplecontainertypeid, tissue_sample_container_name, tissue_sample_container_abbrev, tissue_sample_container_display_order, created, modified) VALUES ('0ffa1173-a153-44ea-8005-83bce6df0c4e', 'Blue Top Tube', 'Blue Top', '8', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_sample_container_type_table (id_tissuesamplecontainertypeid, tissue_sample_container_name, tissue_sample_container_abbrev, tissue_sample_container_display_order, created, modified) VALUES ('82383730-de9b-4566-b7f6-9a04e8dffc78', 'Yellow Top Tube', 'Yellow Top', '9', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_sample_container_type_table (id_tissuesamplecontainertypeid, tissue_sample_container_name, tissue_sample_container_abbrev, tissue_sample_container_display_order, created, modified) VALUES ('8168413a-7ff9-49d2-830d-dd697756659c', 'White Top Tube', 'White Top', '10', '2024-02-23 13:40:13', '2024-02-23 13:40:13');
INSERT INTO tissue_sample_container_type_table (id_tissuesamplecontainertypeid, tissue_sample_container_name, tissue_sample_container_abbrev, tissue_sample_container_display_order, created, modified) VALUES ('263d5e0e-caee-416e-a2b8-3671db36270a', 'Ziplock plastic bag', 'Plastic Bag', '11', '2024-09-30 14:08:10', '2024-09-30 14:08:10');

INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('e0988535-f057-4696-b324-61d8a7fe09f9', 'Blood', 'Bl', '1', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('ae1e540e-4b62-4a15-9e9b-d1ff1bdf6675', 'Ear Punch', 'Ear', '2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('73e43494-f73c-4512-a7a5-9579393a9742', 'Hair', 'Hair', '3', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('53836915-d1b9-435d-819d-9da970670142', 'Feces', 'Fec', '4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('6938ca9f-d341-4d3f-8a71-ba83b93bbe02', 'Milk', 'M', '5', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('6e508e4e-c892-46d9-a1b0-04ec845320dd', 'Urine', 'Ur', '6', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('3ceb2809-f6c8-4a12-a15e-95278eeb6b9e', 'Lung', 'Lung', '7', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('7f68bb3e-4043-4cdb-a2b2-49f52c0e9122', 'Liver', 'Liv', '8', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('4e2817bf-d847-4bf9-95a4-c372a6c427c0', 'Lymph', 'Lym', '9', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('2d4690c7-1df8-4a30-beb2-4da14835d3d5', 'Intestine', 'Int', '10', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('346fe13e-b5e1-4e60-9643-5de7ea0ea698', 'Kidney', 'Kid', '11', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('ad3f6b83-249e-425f-956e-4835975a6565', 'Brain', 'Br', '12', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('05c9d1e4-fd89-45e1-9be6-36c7af121edd', 'Fetus', 'Fet', '13', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('1e964fe9-4e98-4605-ba4e-166387581715', 'Whole Animal', 'Whole Animal', '14', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('ed34bba6-80ad-42aa-a349-ef3d00165b0f', 'Preputial Scraping', 'Preputial', '15', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('d75e9ebb-cd30-4ea4-b25b-719f58e57b80', 'Semen', 'Sem', '16', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('6e91db9a-9969-4010-8ca9-eb692c047a91', 'Wool', 'Wool', '17', '2024-05-03 09:07:00', '2024-05-03 09:07:00');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('f7537792-e797-44d5-9052-d90235668d9d', 'Foot Swab', 'Foot Swab', '18', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('20358133-ce44-4340-8e1e-d5631d7e25f2', 'Abscess Swab', 'Abscess', '19', '2024-09-30 14:08:10', '2024-09-30 14:08:10');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('3311d4fa-f0f4-4586-bd28-f69a357a6908', 'Cyst', 'Cyst', '20', '2024-09-30 14:08:10', '2024-09-30 14:08:10');
INSERT INTO tissue_sample_type_table (id_tissuesampletypeid, tissue_sample_type_name, tissue_sample_type_abbrev, tissue_sample_type_display_order, created, modified) VALUES ('47670de9-a9c2-4213-90be-9f5dc13e4965', 'Unknown', 'Unk', '21', '2024-09-30 14:08:10', '2024-09-30 14:08:10');

INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('bf785162-dd52-4ce9-99ec-bf4cee0a7187', 'Flock 54 DNA Analysis', 'Flock54', '1', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('5297b99b-1b81-4799-9bae-5c5ec53206b3', 'Scrapie Codons', 'Scrapie', '2', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('3f1bd9d1-f881-42e4-9eb5-4772def2a4b9', 'Illumina OvineSNP50', 'OvineSNP50', '67', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('9c162570-21eb-4262-b887-c58604690366', 'Axiom Ovine Genotyping Array', 'Axiom51SNP', '16', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('c83904b7-fcbb-4437-9a16-14a78bbd61f5', 'NSIP Genomics', 'NSIP DNA', '10', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('818dc911-2333-4182-a54a-aea712a6c568', 'Anaplasmosis ELISA', 'Anaplas ELISA', '13', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('93307c34-d37e-4b62-936f-b530cc6c5ee6', 'Blue Tongue AGID', 'BT AGID', '25', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('69390116-d1d9-4f7d-8e09-a3d05170b100', 'Blue Tongue ELISA', 'BT ELISA', '26', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('d01d059c-b019-414b-9876-27b0326a3b3b', 'Bovine Leukosis ELISA', 'BL ELISA', '27', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('e347b954-cc2c-456d-8f2f-ab5560ff8929', 'Bovine Viral Diarrhea ELISA', 'BVD ELISA', '28', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('024e0120-cbce-4a4b-bb4e-226b3a99f388', 'Brucellosi sp.', 'Brucellosi', '35', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('d71ee472-081b-48e8-8eb6-e479bcc4c0e3', 'B. abortis', 'B. abortis', '17', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('886dc01f-c291-4b43-afa0-1b81d2d76885', 'B. canis', 'B. canis', '18', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('eb3076ac-703a-4646-92eb-a969da42c9af', 'B. melitensis', 'B. melitensis', '19', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('4b0c438c-ce11-49a0-911f-d1b1a03e62b1', 'B. ovis ELISA', 'B. ovis ELISA', '5', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('f1f5a2c4-cb48-47e3-821a-836aae417ce8', 'B. suis', 'B. suis', '20', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('676176ba-d305-4173-af52-4b64c99b3940', 'Johnes ELISA', 'Johnes ELISA', '69', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('70cd5d2f-1b60-4bdd-9b6a-f4fd41f41472', 'Johnes PCR', 'Johnes PCR', '70', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('d37cd172-54da-416e-a60b-24a8d095ec1d', 'Pseudorabies ELISA', 'Pseudorabies ELISA', '82', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('9234114b-b6c2-485c-8db2-8aafa24b4782', 'CAE Elisa', 'CAE Elisa', '36', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('4ee2b55d-1992-4501-922a-c7a474852637', 'Tritrichomonas foetus PCR Pooled', 'Trich Pooled', '8', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('ff62f085-e63c-4d5e-9302-df51970afa28', 'Tritrichomonas foetus PCR', 'Trich PCR', '7', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('078e723e-5077-46dd-947d-dbac74eed606', 'Fecal Egg Count', 'FEC', '4', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('293644ad-0e45-4c44-9a23-f31f9bb1a890', 'Fecal Egg Count McMasters', 'FEC M', '64', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('7b2cb8ff-8c91-43f6-b13c-f8d1c7c1b540', 'Semen Analysis', 'Semen', '3', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('389328e7-b92b-46ff-99e5-71bc948c361e', 'FEC Strongyles', 'FEC Strongyles', '61', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('4534eea0-2cd8-48d3-88bd-27ecba4b85ec', 'FEC Nematodirus', 'FEC Nematodirus', '60', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('ba52359e-09df-43d6-83f7-5d5124d1778d', 'FEC Trichuris spp', 'FEC Trichuris spp', '63', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('fa620629-d16a-4cb1-b194-957d19bdd18b', 'FEC Capillarid', 'FEC Capillarid', '57', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('ef945404-5ac8-4a54-9e6c-aa19ecd1365c', 'FEC Moniezia', 'FEC Moniezia', '59', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('d578378b-2bbd-471c-bec3-0458f0032d7f', 'FEC Elmeria', 'FEC Elmeria', '58', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('2c73c664-4217-4eff-be78-40f4a9a8956d', 'FEC Strongyloides', 'FEC Strongyloides', '62', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('1e87a1ad-5414-4b3a-b56c-9f681346d5e7', 'GGP Ovine 50K SNP', 'GGP 50K SNP', '9', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('cc134b86-4b4f-475d-9ffc-8064083f185b', 'NAGP Storage', 'NAGP', '76', '2024-02-18 14:37:57', '2024-02-18 14:37:57');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('95e921fc-b268-49cb-a747-961f3ee1c1be', 'Milk Culture', 'Milk Culture', '74', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('a07ba1c3-6d76-42cb-98f9-75816acbf8ac', 'Footrot', 'Footrot', '75', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('65db037e-3753-4063-b854-dcee70655adb', 'FEC Pooled', 'FEC Pooled', '37', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('c329e31a-69aa-4008-b3ba-a365be342665', 'Enteric Microbes', 'Enteric Microbes', '39', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('63a78fff-74fa-4eb1-b8f7-0e46bf89e130', 'Clostridium Fecal Culture', 'Clostridium Culture', '44', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('a07c6301-3571-45cb-a085-721199c46d2a', 'Necropsy', 'Necropsy', '100', '2024-09-30 14:08:10', '2024-09-30 14:08:10');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('c16e9e39-c01b-4696-b06f-c891aa5e933b', 'Tularemia PCR', 'Tularemia PCR', '89', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('4c40bd06-65d2-4a52-a6f7-dd8edce9ae2c', 'Yersinia pestis PCR', 'Yersinia PCR', '95', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('19fcaf3e-975b-4d7a-972e-9fdef84f47db', 'Leptospirosis 5 Mat Panel', 'Lepto 5 Mat', '71', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('04f55781-2358-40f7-8483-8c31c985e2e0', 'Leptospirosis PCR', 'Lepto PCR', '72', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('98204256-8e3a-4bf9-a960-0d970dd3f31a', 'Johnes AGID', 'Johnes AGID', '68', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('54829877-1fd8-4cf7-a607-a14784b085b0', 'Copper', 'Copper', '45', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('c103410f-9a2c-450d-961d-147fe35e2de3', 'Selenium', 'Selenium', '86', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('d7956dc6-2374-46fd-a073-a326b3293594', 'Vitamin A', 'Vit A', '91', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('9bd61ad9-5ec3-4112-9be6-101e81f7837d', 'Vitamin E', 'Vit E', '92', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('205479df-cad6-4717-9a80-112d5f908060', 'CBC', 'CBC', '40', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('0f585f0e-28ad-47a1-a187-1b5e547bd647', 'OPP ELISA', 'OPP ELISA', '78', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('09979d11-3a26-4650-9755-620408ce9015', 'OPP AGID', 'OPP AGID', '77', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('c9409974-d393-4856-939d-298be7b989f8', 'Cytology', 'Cytology', '46', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('b9e6584f-84a8-47d3-bf28-9caf07b1006e', 'Urinalysis', 'Urinalysis', '90', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('d4d07e6f-ea58-4f4e-a018-129bb5ad7a28', 'Chemistry SADP', 'Chemistry SADP', '42', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('f0fb2524-3e5d-4cc3-ae37-54fdfa4c0f03', 'Chemistry EDP', 'Chemistry EDP', '41', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('303c99a1-c009-4131-afa5-adc0556c1022', 'Rabies FAS', 'Rabies FAS', '83', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('cf935f14-64c0-4ea1-bfde-e5132f957d90', 'EHD AGID', 'EHD AGID', '48', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('93656cbc-034c-4cd0-9ccf-52f5de698797', 'EHD PCR', 'EHD PCR', '49', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('1d1723f5-b161-47dc-b2fe-ef23f6478a64', 'BRSV FA', 'BRSV FA', '31', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('695f4e6a-c43e-4743-923a-c7b7617a86df', 'BRSV PCR', 'BRSV PCR', '32', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('75477619-7f90-4b1d-a931-cd56166536b7', 'BRSV SN', 'BRSV SN', '33', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('36821200-b6c9-4b5c-bc33-4d1a6891d697', 'BRSV VI', 'BRSV VI', '34', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('a7800b0a-9a40-468f-a92e-d873f3091673', 'BHV (IBR) FA', 'BHV (IBR) FA', '22', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('5b8b6cbb-2009-4371-9670-c057ed18ec94', 'BHV (IBR) PCR', 'BHV (IBR) PCR', '23', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('f2d53b67-848d-461f-a674-f4825a445b7f', 'BHV (IBR) SN', 'BHV (IBR) SN', '24', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('c18734c8-49bb-4666-8277-188a64cb245a', 'WNV IgM ELISA', 'WNV IgM ELISA', '93', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('ae2c931f-c1f9-432e-81a0-6738642b4ff1', 'WNV PCR', 'WNV PCR', '94', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('2db090a0-df0b-4f51-a91b-b783a675746c', 'EHV PCR 1 & 4', 'EHV PCR 1 & 4', '50', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('806b85ef-e0b1-41ed-b389-0411bfc1f552', 'EHV PCR 3', 'EHV PCR 3', '51', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('fec99353-a710-48a8-ad34-8d814a6efcc3', 'Encephalitis EEEV', 'Encephalitis EEEV', '54', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('4ed1caed-645c-44a8-b6ce-0aa6eba0cc14', 'Encephalitis VEEV', 'Encephalitis VEEV', '55', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('88e16888-74e7-41a8-9d8f-160248308009', 'Encephalitis WEEV', 'Encephalitis WEEV', '56', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('be96640e-b5b3-4c91-9fa7-4ba75b01b294', 'EIA AGID', 'EIA AGID', '52', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('a4cf4bb9-e1f8-414c-9257-2badbf13ef6d', 'EIA ELISA', 'EIA ELISA', '53', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('e5118e51-3432-4893-b818-88686f41a4b5', 'Ruminant Pregnancy ELISA', 'Ruminant Preg ELISA', '85', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('6fe8ff03-eadb-475e-b1ba-b331a9886d52', 'Fungal Culture', 'Fungal Culture', '65', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('8074a43d-04b3-4375-8f50-47c03e85901d', 'Phenobarbital CLIA', 'Phenobarbital CLIA', '79', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('78c10e4c-aa87-43b8-b8c9-26a2ef67cfc8', 'Total T4 CLIA', 'Total T4 CLIA', '87', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('11ef78d0-4044-4c1a-b048-b10a48203ad1', 'Basal Cortisol CLA', 'Basal Cortisol CLA', '21', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('8450f3d5-1da7-4352-914e-e3f7e1978cea', 'Tritrichomonas foetus Culture', 'TRICH Culture', '88', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('0586d8e1-9f0b-48be-94e0-0aa1579e22c9', 'Bovine Viral Diarrhea PCR Indiv', 'BVD PCR Indiv', '29', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('5c961bf3-f160-4bcb-b394-038318e0206a', 'Bovine Viral Diarrhea PCR Pooled', 'BVD PCR Pooled', '30', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('42c83e87-9bba-4e5b-949f-a5dcc324eed2', 'B. ovis PCR', 'B. ovis PCR', '6', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('0f6f813d-aa80-4931-835a-395f2ad74b7a', 'Chlamydophila PCR', 'Chlamydophila PCR', '43', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('2562b9f7-b8fe-43ca-b183-bc97531f73ed', 'Pinworm', 'Pinworm', '80', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('c6956e91-4791-498b-8d1c-4e20199e43ab', 'Piroplasmosis ELISA', 'Piroplasmosis ELISA', '81', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('9086f8a7-421d-4600-8dbe-a4e6384d9928', 'Liver Panel', 'Liver Panel', '73', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('f84e14bc-35dc-48e6-9a77-9845dc5565c5', 'Giardia PCR', 'Giardia PCR', '66', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('af9c1b99-5a92-409b-b309-b4524ef83500', 'Antibiotic Susceptibility', 'Antibiotic Suscept.', '15', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('6cfeb891-3a4b-40d1-b740-55fdccf65a8c', 'Abortion Serology Panel', 'Abortion Panel', '12', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('392860f5-968f-4a5c-8df9-18c96f8d7875', 'Respiratory Serology Panel', 'Respiratory Panel', '84', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('657d3a79-3177-41a5-bd45-666bc43db388', 'Diarrhea Screen', 'Diarrhea Screen', '47', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('752476db-35c3-489c-9240-4a3d0f48cdde', 'Abortion Screen', 'Abortion Screen', '11', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('bfa4e59d-b20a-4710-bc0b-277a0745eabf', 'Campylobacter Culture', 'Campylobacter Culture', '38', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('54dae690-dcfc-4c88-a348-b2352090629f', 'Anthrax PCR', 'Anthrax PCR', '14', '2024-06-27 07:25:08', '2024-06-27 07:25:08');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('969122cf-d4fc-41fb-9d61-fe947dfa13f6', 'Parasite Drug Resistance', 'Parasite Drug Resit.', '96', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('8aae6613-0fbf-4a14-ad3d-e1793858f181', 'Storage', 'Storage', '97', '2024-07-22 07:51:18', '2024-07-22 07:51:18');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('e77f6b87-692f-45b3-8708-053a7dab420c', 'Parasite Identification', 'Parasite ID', '98', '2024-09-30 14:08:10', '2024-09-30 14:08:10');
INSERT INTO tissue_test_table (id_tissuetestid, tissue_test_name, tissue_test_abbrev, tissue_test_display_order, created, modified) VALUES ('530e90d5-9e80-447d-b630-80f4b307a3d7', 'Histopathology', 'Histopath', '99', '2024-09-30 14:08:10', '2024-09-30 14:08:10');

INSERT INTO transfer_reason_table (id_transferreasonid, transfer_reason, id_registry_id_companyid, transfer_reason_display_order, created, modified) VALUES ('2d3e0aed-84c9-48e8-b11f-00fad83288c6', 'Sold Breeding', 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '1', '2024-02-28 09:20:18', '2024-02-28 09:20:18');
INSERT INTO transfer_reason_table (id_transferreasonid, transfer_reason, id_registry_id_companyid, transfer_reason_display_order, created, modified) VALUES ('b289b13c-78a4-44fc-897f-8438efa02dad', 'Sold Meat', 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '2', '2024-02-28 09:20:18', '2024-02-28 09:20:18');
INSERT INTO transfer_reason_table (id_transferreasonid, transfer_reason, id_registry_id_companyid, transfer_reason_display_order, created, modified) VALUES ('07651217-bf40-4bdd-a63d-d4c484cbc9df', 'Sold Unregistered', 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '3', '2024-02-28 09:20:18', '2024-02-28 09:20:18');
INSERT INTO transfer_reason_table (id_transferreasonid, transfer_reason, id_registry_id_companyid, transfer_reason_display_order, created, modified) VALUES ('58d08bf7-5d1f-4dda-80a7-009bbfe7084f', 'Natural Addition', 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '4', '2024-02-28 09:20:18', '2024-02-28 09:20:18');
INSERT INTO transfer_reason_table (id_transferreasonid, transfer_reason, id_registry_id_companyid, transfer_reason_display_order, created, modified) VALUES ('b0d6f22f-a817-4bbc-9af2-a5d41f158efb', 'Sold Auction', 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '5', '2024-03-22 13:43:23', '2024-03-22 13:43:23');
INSERT INTO transfer_reason_table (id_transferreasonid, transfer_reason, id_registry_id_companyid, transfer_reason_display_order, created, modified) VALUES ('48c176c4-61d8-434c-8abb-dfe19e7d849d', 'Sold Showing', 'ed382247-5cef-48ea-b7fe-b09491dc9ad6', '6', '2024-04-29 14:57:09', '2024-04-29 14:57:09');

INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('fe4678ef-c2a0-470e-950b-b25088867ed6', 'Decimal Pounds', 'lbs', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '1', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('8c8ac721-84d3-4358-902c-1c78771d04d6', 'Kilograms', 'kg', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('d832ca99-4a89-4051-b20c-fddc05c179df', 'Inches', 'in', 'b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', '6', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('9f2b567e-7458-467f-b90c-8494bdfc9d11', 'Centimeters', 'cm', 'b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', '10', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('ffabfa21-ef36-4144-b5c6-58c8e369b1a7', 'Days', 'd', '2e55da17-1bd4-4178-a83b-5c1f2034d59e', '4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('d2f3e19d-bae7-476c-92f3-e354eca0c209', 'Hours', 'h', '2e55da17-1bd4-4178-a83b-5c1f2034d59e', '5', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('95162ff3-e6b8-4676-81db-e08a52217c81', 'Micron', 'u', 'b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', '13', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('9b023940-1cc1-455a-996e-166700750650', 'US Dollars', '$', 'aca60c83-db19-4cfb-86ed-c458a9413878', '5', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('c13a3fc2-99c7-489c-84fc-ee511d9a3433', 'Acres', 'Ac', 'a026b938-b873-4b04-bae7-e7dcf1100a29', '15', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('2836276e-08a6-449a-bdfd-e1ef72771f3f', 'Tons', 't', '26a0a6c1-01a9-42f4-9601-53e9e03bde71', '3', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('41a2b8a3-fde7-4612-8fe9-c7c94c2e1170', 'Hectares', 'ha', 'a026b938-b873-4b04-bae7-e7dcf1100a29', '14', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('a9673a9e-762f-4d92-8cda-58b581d78e7b', 'milli-International Unit per milliliter', 'mIU/ml', '46101d80-0359-4c1c-a9f0-aa001fc29647', '4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('2440f4bb-51ac-4203-a589-3652400533a2', 'Square Inches', 'sq in', 'a026b938-b873-4b04-bae7-e7dcf1100a29', '16', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('b405f449-fa3f-4377-b2e5-4f42c2d68c74', 'Square Centimeters', 'sq cm', 'a026b938-b873-4b04-bae7-e7dcf1100a29', '17', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('3d62e777-7007-4a31-b74e-1dc848a44e62', 'Milliliter', 'ml', '4379d5ad-1991-43b6-843b-984e3bcc82bd', '12', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('554db139-cfa9-42c9-832e-284fad15c63e', 'UK Currency Pounds', 'L', 'aca60c83-db19-4cfb-86ed-c458a9413878', '6', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('2d09a8f5-e3c7-4bee-a575-5710c965d1a8', 'Feet', 'ft', 'b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', '11', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('3892218c-dd16-4ad6-996f-fc532be07c11', 'Eggs per gram', 'epg', '46101d80-0359-4c1c-a9f0-aa001fc29647', '18', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('02cb78b3-9bff-41df-a579-ae744f339389', 'Forward Progressive Motility %', 'FPM', '2532d531-bea9-4315-80f0-f2ab0c088d90', '19', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('6a17ca51-1d0c-4961-a68b-1e49298d0655', 'Normal  Morphology %', 'Morph', '2532d531-bea9-4315-80f0-f2ab0c088d90', '20', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('7f717de7-7ed1-4562-b1bc-9f82be9d77a5', 'Years', 'y', '2e55da17-1bd4-4178-a83b-5c1f2034d59e', '1', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('f747af3b-b3be-43fc-8591-193df3bd77f9', 'Months', 'm', '2e55da17-1bd4-4178-a83b-5c1f2034d59e', '2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('ef56d7b8-1b54-4d03-9ae7-5c44719c5358', 'Weeks', 'w', '2e55da17-1bd4-4178-a83b-5c1f2034d59e', '3', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('c5f2c6cb-f313-4861-ac6e-8d606dcf5d9e', 'Seconds', 's', '2e55da17-1bd4-4178-a83b-5c1f2034d59e', '6', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('38907ea4-e6a5-4e64-bf6d-6fcb0dc028d1', 'Decimal Value', 'Value', '49638d1d-b964-48bd-86ac-498930612bc8', '9', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('b2d6531b-9aab-4ebb-b794-766d25cd6668', 'Decimal Degrees', 'Dec Degrees', '39f83e14-bff6-4cb1-af71-465cacc5bb67', '1', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('fe7b6c72-e46a-4241-8066-09f72a590e8c', 'Decimal Minutes', 'Dec Minutes', '39f83e14-bff6-4cb1-af71-465cacc5bb67', '2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('15963e26-5d36-4038-8621-922fa8e34800', 'Degrees Minutes Seconds', 'Deg Min Sec', '39f83e14-bff6-4cb1-af71-465cacc5bb67', '3', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('68c3b6ef-be53-44af-a4bf-fd8bb74d730b', 'Canadian Dollars', 'Can $', 'aca60c83-db19-4cfb-86ed-c458a9413878', '3', '2024-05-30 06:48:14', '2024-05-30 06:48:14');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('34ceffa3-7f4f-483d-ba06-af28bb710ed0', 'Degrees Fahrenheit', 'Degrees F', '8e827095-d519-4db8-ac18-56e502c34431', '1', '2024-09-13 11:16:18', '2024-09-13 11:16:18');
INSERT INTO units_table (id_unitsid, units_name, units_abbrev, id_unitstypeid, units_display_order, created, modified) VALUES ('38829957-31b1-4fe0-bff1-6751ae329623', 'Degrees Celsius', 'Degrees C', '8e827095-d519-4db8-ac18-56e502c34431', '2', '2024-09-13 11:16:18', '2024-09-13 11:16:18');

INSERT INTO units_type_table (id_unitstypeid, unit_type_name, units_type_display_order, created, modified) VALUES ('26a0a6c1-01a9-42f4-9601-53e9e03bde71', 'Weight', '1', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_type_table (id_unitstypeid, unit_type_name, units_type_display_order, created, modified) VALUES ('4379d5ad-1991-43b6-843b-984e3bcc82bd', 'Volume', '2', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_type_table (id_unitstypeid, unit_type_name, units_type_display_order, created, modified) VALUES ('aca60c83-db19-4cfb-86ed-c458a9413878', 'Currency', '3', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_type_table (id_unitstypeid, unit_type_name, units_type_display_order, created, modified) VALUES ('2e55da17-1bd4-4178-a83b-5c1f2034d59e', 'Time', '4', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_type_table (id_unitstypeid, unit_type_name, units_type_display_order, created, modified) VALUES ('a026b938-b873-4b04-bae7-e7dcf1100a29', 'Area', '5', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_type_table (id_unitstypeid, unit_type_name, units_type_display_order, created, modified) VALUES ('b7bc82f7-c97d-4cbc-8c9f-bd5388f3e8a5', 'Length', '6', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_type_table (id_unitstypeid, unit_type_name, units_type_display_order, created, modified) VALUES ('46101d80-0359-4c1c-a9f0-aa001fc29647', 'Count', '7', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_type_table (id_unitstypeid, unit_type_name, units_type_display_order, created, modified) VALUES ('2532d531-bea9-4315-80f0-f2ab0c088d90', 'Percent', '8', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_type_table (id_unitstypeid, unit_type_name, units_type_display_order, created, modified) VALUES ('49638d1d-b964-48bd-86ac-498930612bc8', 'Real Number', '9', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_type_table (id_unitstypeid, unit_type_name, units_type_display_order, created, modified) VALUES ('39f83e14-bff6-4cb1-af71-465cacc5bb67', 'Location Coordinates', '10', '2023-09-17 10:26:51', '2023-09-17 10:26:51');
INSERT INTO units_type_table (id_unitstypeid, unit_type_name, units_type_display_order, created, modified) VALUES ('8e827095-d519-4db8-ac18-56e502c34431', 'Temperature', '11', '2024-09-13 11:16:18', '2024-09-13 11:16:18');

```