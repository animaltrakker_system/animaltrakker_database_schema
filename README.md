# AnimalTrakker® Database
## Version 5 (2024-05-10)
### Features
- Added Mexico, United Kingdom, Australia and New Zealand to country_table pre-filled options
- Corrected NUll to be NULL for consistency
- Added genetic_codon112_table to genetic characteristics for sheep flocks. 
- Added a carcass tag id type for carcass level traceability and evaluation tracking
- Added Canadian Federal id type to handle old visual official IDs from Canada
- Added tissue test types for specific FEC counts (Strongyles, Nematodirus, Trichuris spp, Calillarid, Moniezia, Elmeria, and Strongyloides).
- Added Tissue Test types of GGP Ovine 50k SNP for DNA analysis and NAGP Storage for cloneable samples.
- Added Blue, Yellow and White top tubes as tissue sample container types. 
### Bug Fixes
- Corrected sheep_id to be id_animalid in the sheep_ebv_table
- Fixed REAL in stored_semen_table
### Breaking Changes
- Foreign key constraints added across the entire database as required. 
- Corrected typo in cluster_calculation_type_table in the display order field.
- Added columns for either a contact ID or a company ID across the entire database as required.
- Renamed sheep_ebv_table to sheep_nsip_ebv_table 
- Renamed animal_ebv_id_table to animal_ebv_table 
- Added table names to the ebv_calculation_method_table so we know where to go to get the data for each calculation type
- Renamed sheep_ebv_nsip_cross_reference_table to sheep_ebv_nsip_lambplan_cross_reference_table
- Corrected sheep_ebv_nsip_lambplan_cross_reference_table Insert statements
- Removed  the animal_birthing_history table and incorporated the data into either the animal_table records or the female_breeding_table as appropriate. 
- Renamed tag_color_table to be id_color_table since we use colors for id types other than just tags
- Changed tags in all references to be ID since more than tags can be an identification. 
- Deleted animal_horn_type_table and made horn type part of the genetic_characteristics system
- Made coat color part of the genetic_characteristics system
- Added predefined items for coat color of red, black, chocolate and white.
- Removed registry_id from the coat color table. 
- Completely restructured all the contact and company tables and the premise table for proper many to many relationships.
- Added tables for companies and contacts for email, phone and website records
- Re-did all references to a contacts_id to handle the 2 types of contacts, people and companies
- Renamed all the contacts tables to remove the plural so contacts becomes contact
- Corrected the animal_location_history_table to be by premise ID not by contact or company name.
- Removed contact_address_table because it's replaced by the premise_table 
- Removed all feed tracking tables including
	- feed_external_file_table
	- feed_ingredient_table
- In both stored_embryo_table  and stored_semen_table 
	- Removed id_animalbreedid to allow for percentage of a breed. 
	- Added a link to the owner of the sire or at time of collection. 
	- Added a link to the current owner
	- Added id_locationcollectedid link to the premise where collected
	- Removed id_locationid
- Added created and modified text field for all records for a date record created and date modified for future use. 
- Added animal name to the animal_registration_table record to handle different names for the same animal in different registries. The name in the animal table is now the call or farm name or one of the registered names. It's up to the user to decide how to use that animal name field. 
- Added fields in the animaltrakker_default_settings table to handle the contact and company changes, a user serial number and other fields as necessary.
- Added saved_evaluations_table to store sets of evaluations for use later. 
- Added animaltrakker_metadata_table to store a semantic version of the database schema.
## Version 4 (2024-04-27)

### Features
- Added GeneCheck and Flock 54 as labs in the Contacts for sheep genetic testing.
- Added predefined contacts for current Vets and AnimalTrakker support
- Added Optimal Ag Ram Breeding Soundness Exam as an option for the main display.
- Added Optimal Ag evaluation trait Scrotal Palpation as scored 1-5 trait for future ram semen testing use. 
- Added Custom Evaluation of Simple Sort to collect Keep vs Ship data quickly.
- Added Subcutaneous Neck as an option for drug locations for when which side the vaccine is given on is not tracked. 
- Updated standard name for the Western Slope Lab to include CSU in the name
- Added Predefined notes for OptimalAg evaluations
- Added predefined coat colors of Red and Black with a contact of Unknown
- Added lab_test_external_submission_form_table to link each lab with their custom form for each type of test. Added a dummy record into this table for testing purposes. 
- Added Black Face Cross as a sheep breed
### Bug Fixes
- Remove AUTOINCREMENT from primary keys in most tables
- Remove UNIQUE from primary key, redundant
- Added some foreign key definitions
### Breaking Changes
- Changed primary key for the animal_drug_table to comply with our naming conventions.


## Version 3 (2022-09-04)
### Features
### Bug Fixes
- Changed the abbreviation for Beefalo to be "BE" from "be"
- Correct Florida Cracker Sheep to be display order 59 and Coopworth Sheep to be display order 60 to solve crash on breed display.

## Version 2 (2022-05-22)
### Features
- Added many of the cattle, sheep, goat, pig, donkey and horse breeds that are currently in MIMS to the breeds table.
- Added the cattle, sheep, goat, pig, donkey and horse breeds that are on the Livestock Conservancy Conservation Priority List to the breeds table.
- Adjusted the abbreviations of breeds to match MIMS
- Added Bangs tag as a possible id type
### Bug Fixes
### Breaking Changes
- Added a county table and pre-filled with all the Colorado counties
- Changed contacts table structure to include both mailing and physical county in addresses.
- Changed animal_id_info table structure to include time tag on and time tag off data.

## Version 1 (2022-01-25)
### Features
### Bug Fixes
- Pre-Loaded reference tables with some of the data that is standard like states, tag colors, some breeds, etc.
- Reference tables created with clean create statements
### Breaking Changes

## Version 0 (2021-08-16)
### Features
- Initial conversion of LambTracker tables into new schema with consistent table and field names.
### Bug Fixes
### Breaking Changes
- Cannot be used with any LambTracker code. Requires AnimalTrakker® software of one form or another. 